<?php
   /**
	* Feeder for spreads comparison 
	* given two arrays - one of providers and one of currency pairs,
	* return the HTML table
	*
	* made by Bernacchia Simone 
	*
	* 
	*
	* @Package:		
	* @Date:		08-25-2011 17:30:00 -0800 
	* @Author:		Simone Bernacchia < sbernacchia@gmail.com >
	*
	*
	*/

//include core functions	
include('/home/fbro001/includes/spreads_quotes/spreads_and_quotes.php');	

$providers = getSpreadStatsProviders();

$symbols = getSpreadStatsSymbols();

$symbolString = implode(',',$symbols);

//initialize the middlemen arrays

ensureSpreadUpdated(3);
$tableSpreadsTempArray = getLastSpreadTable();
$cmpSpreadArr = getPreviousSpreadTable();





//initialize the output

$spreadsTable = '';

$isWorkingDay = Date("w",time()) >= 1 && Date("w",time()) <= 5;

if($isWorkingDay){

	$spreadsTable.='<table cellspacing="1" cellpadding="0" style="">
				<tbody>';
	
	$tableheadline ='';
	$tableheadline.='<tr>';
	$tableheadline.='<td valign="top"><img src="../images/pixelino.gif" width="70" height="1" alt=""/></td>';
	
	$tableimageline ='';
	$tableimageline .='<tr>';
	$tableimageline .='<td valign="top" class="listHeaderImportant"><img src="../images/pixelino.gif" alt=""  title=""/></td>';
	
	$tableSpreadsArea = '';
	
	//provider stats at the bottom
	
	$tableStatsLine = '';
	$tableStatsLine .= '<tr class="swapsTableHeadline">';
	$tableStatsLine .= '<td valign="top" class="listDefRowOdd" align="center">&nbsp;</td>';
	
	foreach($providers as $k=>$value3){			
		$tableheadline.='<td valign="top"><img src="../images/pixelino.gif" width="80" height="1" alt=""/></td>';

	
		$tableimageline.='<td valign="top" class="linetwo commCentered lightgreenCommNobBg commtableArrowBg" align="center"><span class="commHeadline strongright">'.$providers[$k]['provname'].'</span></td>';


	
		$tableStatsLine.='<td valign="top" class="listDefRowOdd" align="center"><a href="'.$providers[$k]['statsurl'].'" title="'.$providers[$k]['statstitle'].'" class="comparisonStats"></a></td>';
		
		} //end foreach providers
		
		//prepare the table quotes
		
		$line = 0;
		
		
		
		foreach($symbols as $l=>$value4){
		
		if($line == 0){
			$liststyle = 'listDefRowEven';
			$tblstyle = 'listFetRowEven';
		} 
		
		if($line == 1){
			$liststyle = 'listDefRowOdd';
			$tblstyle = 'listFetRowOdd';
		}
	
	
		
				$tableSpreadsArea .='<tr>';
				$tableSpreadsArea.='<td valign="top" class="'.$liststyle.'" align="center">'.$symbols[$l].'</td>';
		
		$origtblstyle = $tblstyle;
		
		$flowstyle =''; 
			
			foreach($providers as $n=>$value6){
				foreach($tableSpreadsTempArray as $m=>$value5){
					
					$tblstyle = $origtblstyle;
					
					if($tableSpreadsTempArray[$m]['symbol']==$symbols[$l] && $tableSpreadsTempArray[$m]['provider'] == $providers[$n]['provider'] ) {
						
						if(floatval($tableSpreadsTempArray[$m]['values']) > floatval($cmpSpreadArr[$m]['values'])){
						$flowstyle.= ' growing';
						}
						
						if(floatval($tableSpreadsTempArray[$m]['values']) < floatval($cmpSpreadArr[$m]['values'])){
						$flowstyle.= ' falling';
						}
						
											if(floatval($tableSpreadsTempArray[$m]['values']) == floatval($cmpSpreadArr[$m]['values'])){
						$flowstyle.= ' samething';
						}
					
						if($providers[$n]['provider'] == 'ecn2'){
						

							$tableSpreadsArea.='<td valign="top" class="'.$tblstyle.$flowstyle.' rightpad20" >'. $tableSpreadsTempArray[$m]['values'].'</td>';

						
						} else {
					

							$tableSpreadsArea.='<td valign="top" class="'.$tblstyle.$flowstyle.'" align="center">'.$tableSpreadsTempArray[$m]['values'].'</td>';
						} //end if providers
					
					} //end if tableSpreadsTempArray
					
					$flowstyle ='';
				} //end foreach tableSpreadsTempArray
				
			} //end foreach providers
				
				$tableSpreadsArea.='</tr>';
				
		if ($line == 0){
			
		$line++;
			} else {
		$line = 0;
			} //end if line	
				
			
			} //end foreach symbols
		
					
					
		//close table rows
	
		$tableheadline.='</tr>';
		$tableimageline.='</tr>';
		$tableStatsLine.='</tr>';
					
		//put all pieces together			
					
	
	$spreadsTable.=	$tableheadline.$tableimageline.$tableSpreadsArea.$tableStatsLine;
					
					
	$spreadsTable.='</table>';
	$spreadsTable.='<div class="pfHomeHighlighter"></div>';	
	


} else {

$spreadsTable.='vacation time, show sth better';


} //end if isWorkingDay



print $spreadsTable;