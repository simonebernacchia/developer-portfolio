   /**	* Retrieve and send lost password	*	* done by Bernacchia Simone	*	*	*	*	* @Date:		02-24-2011 16:49:00 -0800	* @Author:		Simone Bernacchia < simonebernacchia@gmail.com >	*	*	*/
	
  $(document).ready(function(){

	$('#forgot_pw_form #forgot_submit').click(function(){	var retrieveEmail = $("form#forgot_pw_form #email").val(); 	var redata = 'email='+retrieveEmail;	var reregExp = /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i;
		if(reregExp.test(retrieveEmail)){			$('img#retrloader').show();			$.ajax({				 type: "POST",		url: "Scripts/ajax_forgot_password_form.php",		data: redata, 		success: function(msg){		var remsg = msg;		if(remsg =='ok'){			$("div #forgot_error_container").hide().html('<img src="../images/warning_alert_16.png"/>Your password has been sent to your email address.').fadeIn('slow');										setTimeout("$('div #forgot_error_container').fadeOut('slow')",5000;				setTimeout("$('#forgot_pw_btn[rel]').data('overlay').close();",7000);	
		} //end if
		$('img#retrloader').hide();
		} //end success
			}); //end ajax
		} else {	

		return false;
		} //end if
		return false;
	}); //end function
  }); //end doc ready	