<?php
   /**
	* Parse OEC margins page, extract filter and show margins
	* from third party source using the simple_html_dom external script
	* done by Bernacchia Simone
	* September 20,2010 
	*
	*/

include_once('../includes/simple_html_dom.php');	
$page = "http://74.201.3.42/ibportal/pages/margins.cfm?WLID=51";

// remove HTML comments

function html_no_comment($url) {
	$ret = array();    
	// create HTML DOM    
	$html = file_get_html($url);    
	// remove all comment elements    
	foreach($html->find('table.tableinnercontent tr td') as $e)       
	$ret[] = $e->innertext;    
	// clean up memory    
	$html->clear();    
	unset($html);    
	return $ret;
} //end function

$content = html_no_comment($page);

$arrayfilter = array(	'E-Mini S&P',
						'E-Mini NASDAQ',
						'Mini DJIA 5',
						'Mini Russell 2000',
						'DAX',
						'DJ Euro STOXX 50',
						'EuroFX',
						'British Pound',
						'30 Yr US T-Bonds',
						'EuroBund',
						'Crude Oil',
						'COMEX Gold',
						'Corn',
						'COMEX Silver'
						);


						
$cleanedcontent = array();

//clear the array content
foreach($content as $i=>$value){
array_push($cleanedcontent,strip_tags($content[$i]));
}




						
$filteredcontent = array();
$doublefilteredcontent = array();

						
$maxcnt= count($cleanedcontent);






foreach($arrayfilter as $i=>$value){
$cnt = 0;
	while($cnt < $maxcnt){	

	
		if( stristr($cleanedcontent[$cnt],$arrayfilter[$i])!== false){

			array_push($filteredcontent, array(	'name'=>$cleanedcontent[$cnt],
													'symbol' => $cleanedcontent[$cnt+1],

													'exchange'=>$cleanedcontent[$cnt+2],
													'initialmargin'=>str_replace('€','&euro;',$cleanedcontent[$cnt+3]),

													'day'=>str_replace('€','&euro;',$cleanedcontent[$cnt+4]),
												));
		} 
		$cnt++;
		
		
	} //end while
} //end foreach


//further filter contents
foreach($arrayfilter as $i=>$value){
	foreach($filteredcontent as $j=>$value){
	if($filteredcontent[$j]['name'] == $arrayfilter[$i]){
		array_push($doublefilteredcontent,$filteredcontent[$j]);
		} //end if
	} //end foreach filteredcontent
} //end foreach arrayfilter


	
//reset table
$table="";
				
				$table .='<table width="500" cellspacing="0" cellpadding="0">';				
				$table.='<thead>	<tr class="onepixelrow">
				<td valign="top" class=""><img src="../images/pixelino.gif" width="70" height="1" alt=""/></td>
				<td valign="top"><img src="../images/pixelino.gif" width="60" height="1" alt=""/></td>
				<td valign="top"><img src="../images/pixelino.gif" width="60" height="1" alt=""/></td>
				<td valign="top"><img src="../images/pixelino.gif" width="60" height="1" alt=""/></td>
				<td valign="top"><img src="../images/pixelino.gif" width="60" height="1" alt=""/></td>

				</tr>';	
				$table.='<tr class="table_trading_header linetwo">
					<td class="linetwo"><span class="daconditions">Name</span></td>
					<td class="linetwo strongright"><span class="daconditions ">Symbol</span></td>
					<td class="linetwo strongright"><span class="daconditions strongright">Exchange</span></td>
					<td class="linetwo strongright"><span class="daconditions strongright">Init.Margin</span></td>
					<td class="linetwo strongright"><span class="daconditions strongright" >Day Trading Margin*</span></td>
					</tr></thead><tbody>';
				
				$cnt = 0;				
				$maxcnt= count($content);
				$color='1';

					foreach($doublefilteredcontent as $i=>$value){
		
		
				if($color=='1'){
					$table.='<tr class="lineone">';
					$table.='<td class="lineone"><span class="daconditions">'.htmlentities($doublefilteredcontent[$i]['name']).'</span></td>';
					$table.='<td class="lineone strongright"><span class="daconditions ">'.$doublefilteredcontent[$i]['symbol'].'</span></td>';
					$table.='<td class="lineone strongright"><span class="daconditions">'.$doublefilteredcontent[$i]['exchange'].'</span></td>';
					$table.='<td class="lineone strongright"><span class="daconditions">'.$doublefilteredcontent[$i]['initialmargin'].'</span></td>';
					$table.='<td class="lineone strongright"><span class="daconditions">'.$doublefilteredcontent[$i]['day'].'</span></td>';
					$table.="</tr>";
				
					$color="2";
				} else {
					$table.='<tr class="linetwo">';
					$table.='<td class="linetwo"><span class="daconditions">'.$doublefilteredcontent[$i]['name'].'</span></td>';
					$table.='<td class="linetwo strongright"><span class="daconditions">'.$doublefilteredcontent[$i]['symbol'].'</span></td>';
					$table.='<td class="linetwo strongright"><span class="daconditions">'.$doublefilteredcontent[$i]['exchange'].'</span></td>';
					$table.='<td class="linetwo strongright"><span class="daconditions">'.$doublefilteredcontent[$i]['initialmargin'].'</span></td>';
					$table.='<td class="linetwo strongright"><span class="daconditions">'.$doublefilteredcontent[$i]['day'].'</span></td>';
					$table.="</tr>";
				
					$color="1";
				}//end if
				} // end while
				$table.="</tbody></table>";
				print $table;												
		
				





?>