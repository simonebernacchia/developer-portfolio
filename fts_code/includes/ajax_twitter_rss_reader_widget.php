<?php
   /**
	* Twitter RSS reader for the media center page
	* made by Simone Bernacchia for broker.com
	*
	* based on another RSS reader made for FAQs on the old site
	*
	* @Date:		12-20-2010 17:00:00 -0800 (modified)
	* @Author:		Simone Bernacchia < simonebernacchia@gmail.com >
	*
	*/
	
$twitterfeed='http://twitter.com/statuses/user_timeline/12345678.rss';

$twitsNum = 4; //number of twits to show

$twitterWidget='';

	$doc = new DOMDocument();
	$doc->load($twitterfeed);
	$arrFeeds = array();

	$sectitle = $doc->getElementsByTagName('title')->item(0)->nodeValue;
	$secsubtitle = $doc->getElementsByTagName('description')->item(0)->nodeValue;
	foreach ($doc->getElementsByTagName('item') as $node) {
		$itemRSS = array ( 
			'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
			'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
			'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
			'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue
			);
		array_push($arrFeeds, $itemRSS);
	}


	
	
	
	
$twitterWidget.='<ul class="tweets">';


for($i=0; $i<$twitsNum; $i++){

	$twitterWidget.='<li>';
	
		$twitterWidget.='<a href="'.$arrFeeds[$i]['link'].'">'.$arrFeeds[$i]['title'].'</a><br/>';
		$twitterWidget.='<span class="tweetDate">'.date('m/d/Y h:i:s',strtotime($arrFeeds[$i]['date'])).'</span>';

	$twitterWidget.='</li>';


} //end for

$twitterWidget.='</ul>';


	
print $twitterWidget;
	
	
	

?>