<?php 
// widget for the economic calendar
// made by Bernacchia Simone - march 19 2010
// based on the work of

require('../includes/functions_simone.php');
connect_calendar();


//parse parameters - timezone

if ($_GET['timezone']) $timezone=sanitizer($_GET['timezone']); else $timezone='-08:00';

if ($timezone{0}!='-') $timezone="+".substr($timezone,-5);


//parse parameters - 
	
	if(!$_GET['lang']){
	$language = 'en';
	} else{
	$language = sanitizer($_GET['lang']);
	}
	
	//if possible set a cookie for the language and for the timezone


$idate=gmdate("Y-m-d", time() +$_GET['d']*24*3600+ 3600*($timezone+date("I"))); 


$sql = "SELECT *  , CONVERT_TZ( published, OFFSET , '".$timezone."' ) AS gmt FROM `leadex_calendar_data` LEFT JOIN leadex_calendar ON leadex_calendar.cid = reportid WHERE TO_DAYS( '".$idate."' ) - TO_DAYS( date( CONVERT_TZ( published,OFFSET , '".$timezone."' ) ) ) =0 ORDER BY gmt";


$q = mysql_query($sql);

$max=mysql_fetch_array($q);

if (!$_GET['d'] ) {
	$next=1;
	$previous=-1;
	$daytrail = "";
} else {
	$next=$_GET['d']+1; 
	$previous=$_GET['d']-1; 
	$current = $_GET['d'];
	$daytrail = "d=".$current;
}  

//language terms
$lpreviuos['sp']='Dia anterior';

$lnext['sp']='Siguiente dia';

$lcalendar['sp']='Calendario Economico para ';

$ltime['sp']='Todos los tiempos en';



$lpreviuos['ita']='Giorno Anteriore';

$lnext['ita']='Giorno Seguente';

$lcalendar['ita']='Calendario Economico per';

$ltime['ita']='Tutte le Ore';



$lpreviuos['en']='Previous Day';

$lnext['en']='Next Day';

$lcalendar['en']='Economic Calendar for';

$ltime['en']='All times in ';



$lpreviuos['ch']='Previous Day';

$lnext['ch']='Next Day';

$lcalendar['ch']='Economic Calendar for';

$ltime['ch']='All times in ';



$lparam="&lang=".$language;

$tparam="&timezone=".$timezone;

$impact = Array ("rating_none","rating_lowest","rating_low","rating_middle","rating_high","rating_highest");
?>




<!-- a table formatted in the good old way in some places -->
<div id="FBCalendarscroll">
<table cellspacing="0" cellpadding="0">
<thead>
<tr class="tablehead">
<td valign="top"><img src="../images/pixelino.gif" width="16" height="1"/></td>
<td valign="top"><img src="../images/pixelino.gif" width="16" height="1"/></td>
<td valign="top"><img src="../images/pixelino.gif" width="16" height="1"/></td>
<td valign="top"><img src="../images/pixelino.gif" width="85" height="1"/></td>
<td valign="top"><img src="../images/pixelino.gif" width="16" height="1"/></td>
<td valign="top"><img src="../images/pixelino.gif" width="60" height="1"/></td>
<td valign="top"><img src="../images/pixelino.gif" width="60" height="1"/></td>
<td valign="top"><img src="../images/pixelino.gif" width="60" height="1"/></td>
</tr>
<tr class="tablecaption">
<td valign="top"><img src="../images/pixelino.gif" width="16" height="1"/></td>
<td valign="top">DATE</td>
<td valign="top">REPORT</td>
<td valign="top">IMPACT</td>
<td valign="top">SOURCE</td>
<td valign="top">PREVIOUS</td>
<td valign="top">FORECAST</td>
<td valign="top">ACTUAL</td>
</tr>
</thead>
<tbody>
<?php
$Result = mysql_query( $sql );
		
		if(mysql_num_rows($Result) == 0 || !mysql_num_rows($Result)) {
		
		//redraw this

				echo '<tr class="line1"><td colspan="8" align="center" >There are no Economic indicators released today.</td></tr>';

				} else {
		$ct=0;
	//coloured strips variable
	$color="1";
		
	while($row = mysql_fetch_object($Result)){
	
//	print $row->published;
	$time=substr($row->gmt,0,16); 
			
//prepare the news list according to the language
			switch($language){
		
				case 'ita':
					$source=$row->source_italy;
					$report= $row->report_italy;
					$description= str_replace( chr(13).chr(10),' ',$row->description_italy);
					$description=str_replace("'","`",$description);
					break;
					
				case 'sp':
					$source=$row->source_spain;
					$report= $row->report_spain;
					$description= str_replace( chr(13).chr(10),' ',$row->description_spain);
					$description=str_replace("'","`",$description);
					
					break;

				case 'ch':
					$source=$row->source_chinese;
					$report= $row->report_chinese;
					$description= str_replace( chr(13).chr(10),' ',$row->description_chinese);
					$description=str_replace("'","`",$description);
					
					break;

				case 'en':
					$source=$row->source;
					$report= $row->report;
					$description= str_replace( chr(13).chr(10),' ',$row->description);
					$description=str_replace("'","`",$description);
					
					break;
				
				default:
					$source = $row->source;
					$report = $row->report;
					$description = str_replace( chr(13).chr(10),' ',$row->description);
					$description = str_replace("'","`",$description);
				
					break;

				} //end switch

	

	
		if($color=="1"){
			echo'<tr class="line1">';
	echo'<td><img src="flags/'.$row->currency.'.gif" alt="'.$row->currency.'"/></td>';
	echo'<td>'.$time.'</td>';
	echo'<td><span class="longdesc" title="'.$description.'">'.$report. '</span></td>';
	echo'<td><img src="../images/'.$impact[$row->impact].'.gif" alt="'.str_replace("_"," ",$impact[$row->impact]).'"/></td>';
		echo'<td><a target="_blank" href="'.$row->webpage.'">'.$source.'</a></td>';
			echo'<td>'.$row->previous .'&nbsp;</td>';
			echo'<td>'.$row->forecast .'&nbsp;</td>';
			echo'<td>'.$row->actual .'&nbsp;</td>';
	echo"</tr>";
		
		
		
		$color="2";
		} else {	
		echo'<tr class="line2">';
	echo'<td><img src="flags/'.$row->currency.'.gif" alt="'.$row->currency.'"/></td>';
echo'<td>'.$time.'</td>';
	echo'<td><span class="longdesc" title="'.$description.'">'.$report. '</span></td>';
	echo'<td><img src="../images/'.$impact[$row->impact].'.gif" alt="'.str_replace("_"," ",$impact[$row->impact]).'"/></td>';
		echo'<td><a target="_blank" href="'.$row->webpage.'">'.$source.'</a></td>';
			echo'<td>'.$row->previous .'&nbsp;</td>';
			echo'<td>'.$row->forecast .'&nbsp;</td>';
			echo'<td>'.$row->actual .'&nbsp;</td>';
	echo"</tr>";
		
		$color="1";
		}
	
	
	
	
	}
	
	}
?>
</tbody></table></div>
