<?php
/**** Commentary JSON code widget
	* code by Simone Bernacchia
	*/



include('../includes/functions_simone.php');

connect_db();

$fbwwidth = sanitizer($_GET['ww']); /*widget width*/
$fbwheight = sanitizer($_GET['wh']); /*widget height*/
$fbwbg = sanitizer(substr($_GET['bg'],0,6)); //main background
$fbwl1 = sanitizer(substr($_GET['l1'],0,6)); //line 1 color
$fbwl2 = sanitizer(substr($_GET['l2'],0,6)); // line 2 color
$fbwlc = sanitizer(substr($_GET['lc'],0,6)); //line character color
$fbwfont = sanitizer($_GET['font']); //font
$fbwfs = sanitizer(substr($_GET['fs'],0,6)); //font size in pixel
$fbwth = sanitizer(substr($_GET['th'],0,6)); //headline color
$fbwtc = sanitizer(substr($_GET['tc'],0,6)); //headline character color
$fbwtz = sanitizer(substr($_GET['tz'],0,6)); //timezone offset
$fbwln = sanitizer(substr($_GET['ln'],0,6)); //language
$fbscroll = sanitizer($_GET['s']); //scroll
$fbcid= sanitizer($_GET['pcode']); //campaign

//defaults if no choice is made

if(!$fbwtz || $fbwtz == ""){$fbwtz = "-08:00";}
if(!$fbwln || $fbwln == ""){$fbwln = "en";}

if(!$fbwwidth || $fbwwidth == ""){$fbwwidth = "400";}
if(!$fbwheight || $fbwheight == ""){$fbwheight = "400";}

?>

var FBwWidth = <?php print $fbwwidth; ?>;
var FBwHeight = <?php print $fbwheight; ?>;
var FBwBG0 = '<?php print $fbwbg; ?>';
var FBwl1 = '<?php print $fbwl1; ?>';
var FBwl2 = '<?php print $fbwl2; ?>';
var FBwlc = '<?php print $fbwlc; ?>';
var FBwfont = '<?php print $fbwfont; ?>';
var FBwfs = '<?php print $fbwfs; ?>';
var FBwth = '<?php print $fbwth; ?>';
var FBwtc = '<?php print $fbwtc; ?>'; 
var FBwtz = '<?php print $fbwtz; ?>';
var FBwln = '<?php print $fbwln; ?>';
var FBscroll = '<?php print $fbscroll; ?>';
var FBcid = '<?php print $fbcid; ?>';

var refreshrate = 60000*30; //page refresh rate - every 30 minutes
//the params from the host site
var widgetURL ="http://<?php print $serverName; ?>/widgets/json-commentary-2.php";
var flagURL = "http://<?php print $serverName; ?>/widgets/flags";
var priURL = "http://<?php print $serverName; ?>/widgets/img";
var iconsURL = "http://<?php print $serverName; ?>/widgets/icons";
var bannerURL = "http://<?php print $serverName; ?>/widgets/banner_feeder.php?id=3";
//var bannerpicURLb = "http://<?php print $serverName; ?>/widgets/banners/";
var bannerpicURLb = "http://<?php print $serverName; ?>/banners/images/widgets/";
var d = new Date();
var session_id = d.getTime();
var url = window.location.href;
var day = 0;
var FBwdpar;
FBwFontList = ["Verdana","Arial","Tahoma","Trebuchet MS","Calibri"];
var todaysdate = '<?php echo date('l, F j Y'); ?>';
var FBWversion1 = 'v1.0.53 - 12/29/2010';


// takes a hex string and returns the r, g, b values 
	function parseColor(text){
		var ot = text;
		if(ot.length == 3){
		t1 = ot.substr(0,1);
		t2 = ot.substr(1,1);
		t3 = ot.substr(2,1);
		ot = t1+t1+t2+t2+t3+t3;

		text = ot;
		}
	    // this regular expression checks for a hex color in proper format 
		//	return {r:255,g:0,b:0};
	    if(/^\#?[0-9A-F]{6}$/i.test(text)){ 
	        return { 
	            r: eval('0x'+text.substr(text.length==6?0:1, 2)), 
	            g: eval('0x'+text.substr(text.length==6?2:3, 2)), 
	            b: eval('0x'+text.substr(text.length==6?4:5, 2)) 
	        } 
	    } 
		
	} 
	
	function hex(c){ 
    c=parseInt(c).toString(16); 
    return c.length<2?"0"+c:c 
	} 
	
function colorAverageOpposite(daColor){
	var colorcomponent = parseColor(daColor);
	daaverage = Math.round((colorcomponent.r+colorcomponent.g+colorcomponent.b)/3);
	if (daaverage<=128){
	return "fff";
	} else {
		return "000";
	}	
}

/*defines the styleSheets*/

var head = document.getElementsByTagName('head')[0],
    style = document.createElement('style'),
    rules = document.createTextNode('#FastBrokersCommentary {display:block;}'+'\n' 
	+			'#FBcommentary{display:block; width:'+ FBwWidth +'px; height:'+ FBwHeight +'px; background: #'+FBwBG0+'; position:relative;}'+'\n'
				+	'#FBcommentaryArea{display:block; width:'+ (FBwWidth-4) +'px; height:'+ (FBwHeight-90) +'px; overflow:hidden; font-family:'+FBwFontList[FBwfont]+',sans-serif; position:relative;}'+'\n'
				+	'#FBcommentarytop{display:block; width:'+FBwFontList[FBwfont]+'px; height:63px; background-color:#'+FBwBG0+';}'+'\n'
				+	'#FBcommentaryscroll{display:block; position:absolute; width:'+ (FBwWidth-6) +'px; height:'+ (FBwHeight-90) +'px; left:3px;}'+'\n'		
				+	'#FBcommentarybottom {display:block;  width:'+FBwWidth+'px; height:20px; top:'+(FBwHeight -20)+'px; font-family:'+FBwFontList[FBwfont]+',sans-serif; font-size:80%; background:#'+FBwBG0+'; color:#'+colorAverageOpposite(FBwBG0)+';}'+'\n'
				+	'#timestamp2{float:left; font-family:calibri,sans-serif; font-size:12px;  color: #'+colorAverageOpposite(FBwBG0)+'; margin-left:3px; padding-top:3px; padding-bottom:3px; width:90%; }'+'\n'
				+	'#FBbannerArea1 {display:block; float:left; /*width:'+(FBwWidth-6)+'px;*/ height:35px; background-color:#transparent; margin-left:3px;}'+'\n'
				+	'#FBcommentary #FBdisclaimer1{display:none; width:'+(FBwWidth-10)+'px; height:120px; position:absolute; top:'+(FBwHeight-150)+'px; margin-left:3px; padding:3px; overflow:none; z-index:3000; background-color: #'+ FBwl2 +'; font-family: calibri,sans-serif; font-size:11px; color:#'+colorAverageOpposite(FBwl2)+'; /*letter-spacing:-1px;*/ filter:alpha(opacity=80); opacity: 0.8; -moz-opacity:0.8;}'+'\n'
				+	'#commentarytable {width:100%; font-size:'+FBwfs+'px;}'+'\n'
				+	'#commentarytable tr{border-bottom: 1px #'+FBwl1+' solid; padding:0 4px 0 4px;}'+'\n'
				+	'#commentarytable td{  padding:4px; }'+'\n'
				+	'#commentarytable .line1{background: #'+FBwl1+'; padding:0!important;}'+'\n'
				+	'#commentarytable .line2 {background: #'+FBwl2+'; color:#'+FBwlc+';  }'+'\n'
				+	'#commentarytable .line1 td{padding:0!important;}'+'\n'
				+	'#commentarytable .line3 {padding:0 !important;}'+'\n'
				+	'.format {width:400px; padding:4px; background:#ffc;}'+'\n'
				+	'#commentarytable tr.tablecaption td { background-color:#'+FBwth+'; color:#'+FBwtc+'; border: 1px outset;}'+'\n'
				+	'#tooltip {position: absolute; z-index: 3000; border: 1px solid #111; background-color: #fffacd; padding: 5px; }'+'\n'
				+	'.fbctitle{font-size:120%; padding-bottom:8px; color:'+FBwtc+'; font-weight:bold; margin-bottom:0 !important;}'+'\n'
				+	'.fbctitle a {color:'+FBwtc+'; text-decoration:none; }'+'\n'
				+	'#tooltip h3, #tooltip div { margin: 0; font-weight:normal;}'+'\n'
				+	'#p_status{float:left; margin-top:20px; color:#f00;}'+'\n'
				+	'.readmore {padding:4px;}'+'\n'
			
				+	'.moretitle{font-size:120%; font-weight:bold; clear:both; padding-bottom:4px;}'+'\n'
				+	'.FBbottomleftside{float:left;margin-left:3px; color:#'+colorAverageOpposite(FBwBG0)+'; font-family:calibri,sans-serif; font-size:12px; padding-top:3px;}'+'\n'
				+	'.FBbottomrightside{float:right; margin-right:3px; color:#'+colorAverageOpposite(FBwBG0)+'; font-family:calibri,sans-serif; font-size:12px; padding-top:3px;}'+'\n'
				+	'a.FBbottomlink:active,a.FBbottomlink:link,a.FBbottomlink:visited{text-decoration:none; color:#'+colorAverageOpposite(FBwBG0)+';}'+'\n'
				+	'a.FBbottomlink:hover{text-decoration:underline; color:#'+colorAverageOpposite(FBwBG0)+';}'+'\n'
				+	'a.alwaysul,a.alwaysul:active,a.alwaysul:link,a:alwaysul:visited,a:alwaysul:hover{text-decoration:underline;}'+'\n'
				+	'.paddingleft2{padding-left: '+((FBwWidth-306)/2)+'px;}'+'\n'
				+	'.leftside{display:block;width:100px; float:left;}'+'\n');		
	


	style.type = 'text/css';
if(style.styleSheet)
    style.styleSheet.cssText = rules.nodeValue;
else style.appendChild(rules);
head.appendChild(style);



/*defines the div for the ajax to come out - redone with injectors*/
if(!cmaindoc) {
var cmaindoc = document.createElement('div');
cmaindoc.setAttribute('id','FBcommentary');
document.getElementById('FastBrokersCommentary').appendChild(cmaindoc); 
var commtop = document.createElement('div');
commtop.setAttribute('id','FBcommentarytop');
commtop.innerHTML = '<div id="timestamp2"><img src="'+priURL+'/22.png" border="0" alt="More" style="vertical-align:middle;" title="'+FBWversion1+'"/>&nbsp;Commentary for '+todaysdate+'</div><div id="FBbannerArea1" class="paddingleft2"></div>';

document.getElementById('FBcommentary').appendChild(commtop);
var commarea = document.createElement('div');
commarea.setAttribute('id','FBcommentaryArea');
document.getElementById('FBcommentary').appendChild(commarea);
var comdisclaimer = document.createElement('div');
comdisclaimer.setAttribute('id','FBdisclaimer1');
comdisclaimer.setAttribute('style','display:none;');
comdisclaimer.innerHTML =	'<b>Disclaimer:</b><br/>'
					+	'FastBrokers&#39; widgets are provided for information purposes only and under no circumstances should be regarded neither as an investment advice nor as a solicitation or an offer to sell/buy any financial product. FastBrokers assumes no responsibility or liability from gains or losses incurred by the information herein contained. Live quotes are delayed. <br/>'
					+	'<b>Risk Disclosure:</b><br/>'
					+	'There is a substantial risk of loss in trading futures and foreign exchange. Please carefully review all risk disclosure documents before opening an account as these financial instruments are not appropriate for all investors.';
					document.getElementById('FBcommentary').appendChild(comdisclaimer);

var commscroll = document.createElement('div');
commscroll.setAttribute('id','FBcommentaryscroll');
document.getElementById('FBcommentaryArea').appendChild(commscroll);
var commbottom = document.createElement('div');
commbottom.setAttribute('id','FBcommentarybottom');
commbottom.innerHTML = '<div class="FBbottomleftside">Powered by <a href="http://<?php print $serverName; ?>/index.php?'+FBcid+'" target="_blank" class="FBbottomlink alwaysul"><b>FastBrokers.com</b></a></div>'
						+	'<div class="FBbottomrightside"><a href="javascript:;" onMouseOver="toggleDisclaimer(\'FBdisclaimer1\');" onMouseOut="toggleDisclaimer(\'FBdisclaimer1\');" class="FBbottomlink">Disclaimer</a> | <a href="http://<?php print $serverName; ?>/solutions/widgets.php?'+FBcid+'" class="FBbottomlink" target="_blank">Free Widgets</a></div>';
document.getElementById('FBcommentary').appendChild(commbottom);
}


var params = "";


var myurl = widgetURL;

var lastMessage = 0;
var mTimer;
var scrme2;

//initialize the widget now
addScript(myurl);

//initialize banner
addBannerScript3(bannerURL);




function addScript(myurl) {

		clearInterval(scrme2);
	var script = document.createElement('script');
	script.setAttribute('id','Aparameter');
    script.src = myurl;

	document.getElementsByTagName('head')[0].appendChild(script);
	document.getElementById('FBcommentaryscroll').style.top = 0+'px';

}

function refreshScript(myurl){
	clearInterval(scrme2);
	var script = document.getElementById('Aparameter');
	if (script) {
			document.getElementsByTagName('head')[0].removeChild(script);

	}
	
	addScript(myurl);
}

function tellStatus(){
	alert('loaded');
}

//banner load script
function addBannerScript3(myurl) {

	var bannerscript2 = '';
	var bannerscript2 = document.createElement('script');
	bannerscript2.setAttribute('id','bannerdata2');
    bannerscript2.src = myurl;
	
	document.getElementsByTagName('head')[0].appendChild(bannerscript2);

}

//banner feeded script
function jsonbanner3(myresponse){
	var bannerres = myresponse;
	var banner_div = document.getElementById('FBbannerArea1');
	
	if(!bannerres.banner.data[0].campaign || bannerres.banner.data[0].campaign == ''){
	var FBbannercampaigncode = FBcid;
	} else {
		var FBbannercampaigncode = bannerres.banner.data[0].campaign;
	
	}
	
	
		if(bannerres.banner.data[0].isflash=='1'){

	var blink1 = document.createElement('div');

	
	blink1.innerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="'+bannerres.banner.data[0].width+'" height="'+bannerres.banner.data[0].height+'">'
 + '<param name="movie" value="'+bannerpicURLb+bannerres.banner.data[0].image+'" />'
  + '<param name="quality" value="high" />'
  + '<param name="flashvars" value="'+bannerres.banner.data[0].flashcode+'='+bannerres.banner.data[0].link+'?'+FBbannercampaigncode+'" />'
 + '<embed src="'+bannerpicURLb+bannerres.banner.data[0].image+'?'+bannerres.banner.data[0].flashcode+'='+bannerres.banner.data[0].link+'?'+FBbannercampaigncode+'" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="'+bannerres.banner.data[0].width+'" height="'+bannerres.banner.data[0].height+'"></embed></object>';
 	banner_div.appendChild(blink1);
	} else {
	
	
	var blink1 = document.createElement('a');
	blink1.setAttribute('href',bannerres.banner.data[0].link+'?'+FBbannercampaigncode);
	blink1.setAttribute('target','_blank');

	blink1.innerHTML = '<img src="'+bannerpicURLb+bannerres.banner.data[0].image+'" alt="'+bannerres.banner.data[0].alttext+'" border="0" />';
	banner_div.appendChild(blink1);
	
	} //end if

}


function jsoncommentary(daresponse){
	clearInterval(scrme2);

	var alticon;
	var response = daresponse;


	var commentary_div = document.getElementById('FBcommentaryscroll'); //indicates where to write the table
	var headline_div = document.getElementById('FBcommentarytop'); //indicates where to write the table
	var myinnerHTML = "";
		myinnerHTML +='<thead><tr class="tablehead">';
		//the single pixel row adjuster
		
			myinnerHTML += '<td><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="16" height="1"/></td>';
			myinnerHTML += '<td><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="16" height="1"/></td>';
		myinnerHTML +='</tr></thead>';

		
		//the table body and datas

		
		myinnerHTML +='<tbody>';
		for(i=0;i < response.commentary.data.length; i++) {

			alticon = response.commentary.data[i].caption.slice(0,-4);
						myinnerHTML +='<tr class="line2" >';
			myinnerHTML +='<td rowspan="2" valign="top"><img src="'+iconsURL+'/'+response.commentary.data[i].caption+'" alt="'+alticon+'"/></td>';
			myinnerHTML +='<td valign="top"><p class="fbctitle"><a href="http://<?php print $serverName;?>/commentary/view.php?'+FBcid+'&id='+response.commentary.data[i].id+'" target="_blank">'+response.commentary.data[i].title+'</a></p><div class="author" style="float:left;">By '+response.commentary.data[i].author+'</div><div style="float:right;"><span class="datetime">'+response.commentary.data[i].date+'</span></div></td>';

			myinnerHTML +='</tr>';
					myinnerHTML +='<tr class="line2" ><td  class="line3" ><div id="hid'+i+'"class="readmore" >'+response.commentary.data[i].content+'&nbsp;&nbsp;<a href="http://<?php print $serverName;?>/commentary/view.php?'+FBcid+'&id='+response.commentary.data[i].id+'" target="_blank"><img src="'+priURL+'/29.png" border="0" alt="More"/></a></div></td></tr>';
					myinnerHTML += '<tr class="line1">';
					myinnerHTML += '<td><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="16" height="2"/></td>';
			myinnerHTML += '<td><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="16" height="2"/></td>';
					myinnerHTML +='</tr>';
			
			
			

		} //end for
		
		
		
		
		myinnerHTML +='</tbody>';
			
			commentary_div.innerHTML = '<table id="commentarytable" width="100%" cellspacing="0" cellpadding="0" >'+myinnerHTML+'</table>';
		mTimer = setTimeout('refreshScript(myurl);',refreshrate); //Refresh content every given interval
		
	if(FBscroll =='1'){	
		clearInterval(scrme2);
		LoadNewsScrollers();
		scrme2 = setInterval('AutoScrollNewsDown()',150);
		}

}

function toggleDisclaimer(id){
	 var e = document.getElementById(id);

       if(e.style.display == 'block') {
          e.style.display = 'none';

       } else {
          e.style.display = 'block';

       } //end if

}

var gpy;
var gStopNews;

//getElementById absstractor from the old common.js

var null_element;
function spsGetElementById(id) {
	if (document.getElementById(id)) {
		return document.getElementById(id);
	} else if (document.all) {
		return document.all[id];
	} else if (document.layers && document.layers[id]) {
		return (document.layers[id]);
	} else {
		return null_element;
	}
}




//an addEventLinsnet abstractor from the old common.js

function xAddEventListener(es,eventType,eventListener,useCapture) {
	var e;
	if(!(e=spsGetElementById(es))) return;
	eventType=eventType.toLowerCase();
	var eh1="e.on"+eventType+"=eventListener";
	if(e.addEventListener) e.addEventListener(eventType,eventListener,useCapture);
	else if(e.attachEvent) e.attachEvent("on"+eventType,eventListener);
	else if(e.captureEvents) {
		if(useCapture||(eventType.indexOf('mousemove')!=-1)) { e.captureEvents(eval("Event."+eventType.toUpperCase())); }
		eval(eh1);
	}
	else eval(eh1);
}



function AutoScrollNewsDown() {
	var t,u, eh;

	if(gStopNews) {
	return;
	}
	t= document.getElementById('FBcommentaryscroll');
	u = document.getElementById('commentarytable');
	eh = u.offsetHeight;
	
	ewh = -(eh -  (parseInt(FBwHeight)-100));
	
	if(eh+gpy<ewh) return;
	if(eh+gpy<=0) gpy=0;
	gpy-=2;
	t.style.top=gpy+'px';
	
}

function AutoScrollNewsStop() {
	gStopNews=true;
}

function AutoScrollNewsStart() {
	
	gStopNews=false;
}

function LoadNewsScrollers(){
	
	var t,u, eh;

	gpy=0;
	t= document.getElementById('FBcommentaryscroll');
	u = document.getElementById('commentarytable');
	eh = u.offsetHeight;

	ewh = -(eh -  (parseInt(FBwHeight)-100));

	if(eh<ewh) return;
	gStopNews=false;
	xAddEventListener('FBcommentaryArea','mouseover',AutoScrollNewsStop,false);
	xAddEventListener('FBcommentaryArea','mouseout',AutoScrollNewsStart,false);



}





<?php


$ref= (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '';
$url = stripslashes(strip_tags($ref));
$start = date('Y/m/d g:i a');
$end = date('Y/m/d g:i a');
$widget = 'commentary_widget';
$session_id = time();


mysql_query("INSERT INTO tbl_widgets_tracker (session_id,widget_name,url,start) VALUES ('".$session_id."','".$widget."','".$url."', '".$start."')");	


return;



?>