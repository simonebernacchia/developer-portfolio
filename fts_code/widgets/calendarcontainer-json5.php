<?php
/**** calendar JSON code widget
	* hoping this will work
	* code by Simone Bernacchia
	*/



include('../includes/functions_simone.php');

connect_db();

$fbwwidth = sanitizer($_GET['ww']); /*widget width*/
$fbwheight = sanitizer($_GET['wh']); /*widget height*/
$fbwbg = sanitizer(substr($_GET['bg'],0,6)); //main background
$fbwl1 = sanitizer(substr($_GET['l1'],0,6)); //line 1 color
$fbwl2 = sanitizer(substr($_GET['l2'],0,6)); // line 2 color
$fbwlc = sanitizer(substr($_GET['lc'],0,6)); //line character color
$fbwfont = sanitizer($_GET['font']); //font
$fbwfs = sanitizer(substr($_GET['fs'],0,6)); //font size in pixel
$fbwth = sanitizer(substr($_GET['th'],0,6)); //headline color
$fbwtc = sanitizer(substr($_GET['tc'],0,6)); //headline character color
$fbwtz = sanitizer(substr($_GET['tz'],0,6)); //timezone offset
$fbwln = sanitizer(substr($_GET['ln'],0,6)); //language
$fbscroll = sanitizer($_GET['s']); //scroll
$fbcid= sanitizer($_GET['pcode']); //campaign

//defaults if no choice is made

if(!$fbwtz || $fbwtz == ""){$fbwtz = "-08:00";}
if(!$fbwln || $fbwln == ""){$fbwln = "en";}

if(!$fbwwidth || $fbwwidth == ""){$fbwwidth = "400";}
if(!$fbwheight || $fbwheight == ""){$fbwheight = "400";}

?>

var FBwWidth = <?php print $fbwwidth; ?>;
var FBwHeight = <?php print $fbwheight; ?>;
var FBwBG1 = '<?php print $fbwbg; ?>';
var FBwl1 = '<?php print $fbwl1; ?>';
var FBwl2 = '<?php print $fbwl2; ?>'; 
var FBwlc = '<?php print $fbwlc; ?>';
var FBwfont = '<?php print $fbwfont; ?>';
var FBwfs = '<?php print $fbwfs; ?>';
var FBwth = '<?php print $fbwth; ?>';
var FBwtc = '<?php print $fbwtc; ?>';
var FBwtz = '<?php print $fbwtz; ?>';
var FBwln = '<?php print $fbwln; ?>';
var FBscroll = '<?php print $fbscroll; ?>';
var FBcid = '<?php print $fbcid; ?>';


var refreshrate = 60000*60; //page refresh rate - every hour
//the params from the host site
var widgetURL1 ="http://<?php print $serverName; ?>/widgets/json-calendar.php";
var flagURL = "http://<?php print $serverName; ?>/widgets/flags";
var priURL1 = "http://<?php print $serverName; ?>/widgets/img";
var bannerURL = "http://<?php print $serverName; ?>/widgets/banner_feeder.php?id=2";
var bannerpicURLc = "http://<?php print $serverName; ?>/banners/images/widgets/";
var d = new Date();
var session_id = d.getTime();
var url = window.location.href;
var day = 0;
var FBwdpar ="";
FBwFontList = ["Verdana","Arial","Tahoma","Trebuchet MS","Calibri"];
var FBWversion2 = 'v1.0.44 - 12/29/2010';

// takes a hex string and returns the r, g, b values 
	function parseColor(text){
		var ot = text;
		if(ot.length == 3){
		t1 = ot.substr(0,1);
		t2 = ot.substr(1,1);
		t3 = ot.substr(2,1);
		ot = t1+t1+t2+t2+t3+t3;

		text = ot;
		}
	    // this regular expression checks for a hex color in proper format 
		// return {r:255,g:0,b:0};
	    if(/^\#?[0-9A-F]{6}$/i.test(text)){ 
	        return { 
	            r: eval('0x'+text.substr(text.length==6?0:1, 2)), 
	            g: eval('0x'+text.substr(text.length==6?2:3, 2)), 
	            b: eval('0x'+text.substr(text.length==6?4:5, 2)) 
	        } 
	    } 
		
	} 
	
	function hex(c){ 
    c=parseInt(c).toString(16); 
    return c.length<2?"0"+c:c 
	} 
	
function colorAverageOpposite(daColor){
	var colorcomponent = parseColor(daColor);
	daaverage = Math.round((colorcomponent.r+colorcomponent.g+colorcomponent.b)/3);
	if (daaverage<=128){
	return "fff";
	} else {
		return "000";
	}
	
}

//found in a forum gotta see if it works

function FindPos(ctrl) { 
var pos = {x:0, y:0};

if (ctrl.offsetParent){
    while(ctrl) {
        pos.x += ctrl.offsetLeft;
        pos.y += ctrl.offsetTop;
        ctrl = ctrl.offsetParent;
    } //end while
} else if (ctrl.x && ctrl.y) {
    pos.x += ctrl.x;
    pos.y += ctrl.y;
} //end if

return pos;

}


var myleft = FindPos("BrokerFrame");

/*defines the styleSheets*/
//var mainstyle = 
var head = document.getElementsByTagName('head')[0],
    style = document.createElement('style'),
    rules = document.createTextNode('#FBcalendar {display:block; width:'+ FBwWidth +'px; height:'+ FBwHeight +'px; background: #'+FBwBG1+'; position:relative; text-transform: normal!important; letter-spacing:none!important;}'+'\n'
			+	'#FBcalendarArea {display:block; width:'+ (FBwWidth-4) +'px; height:'+ (FBwHeight-90) +'px; overflow:hidden; font-family:'+FBwFontList[FBwfont]+',sans-serif; position:relative; }'+'\n'
				+	'#FBcalendartop {display:block; width:width:'+FBwWidth+'px; height:63px; background-color:#'+FBwBG1+';}'+'\n'
				+	'#FBcalendarscroll {display:block; position:absolute; width:'+ (FBwWidth-6) +'px; height:'+ (FBwHeight-90) +'px; left:3px;}'+'\n'
				+	'#FBcalendarbottom {display:block;  width:'+FBwWidth+'px; height:20px; top:'+(FBwHeight -20)+'px; font-family:'+FBwFontList[FBwfont]+',sans-serif; font-size:80%; background:#'+FBwBG1+'; color:#'+colorAverageOpposite(FBwBG1)+'; z-index:10000;}'+'\n'
				+	'.daycontrols {display:block; float:left; padding: 3px 0 3px 3px; font-family:'+FBwFontList[FBwfont]+';  width:'+ (FBwWidth-6) +'px; height:18px;  }'+'\n'
				+	'#ctimestamp{float:left; font-family:calibri,sans-serif; font-size:12px;  color: #'+colorAverageOpposite(FBwBG1)+';  }'+'\n'
				+	'#language{float:left; font-family:calibri,sans-serif; font-size:12px;  color: #'+colorAverageOpposite(FBwBG1)+'; padding-left:20px; }'+'\n'
				+	'#FBcalendar #FBdisclaimer{display:none; width:'+(FBwWidth-10)+'px; height:120px; position:absolute; top:'+(FBwHeight-150)+'px; left:0 px; margin-left:3px; padding:3px; overflow:none; z-index:3000; background-color: #'+ FBwl2 +'; font-family: calibri,sans-serif; font-size:11px; color:#'+colorAverageOpposite(FBwl2)+'; /*letter-spacing:-1px;*/ filter:alpha(opacity=80); opacity: 0.8; -moz-opacity:0.8;}'+'\n'
			
				+	'#calendartable, #headercalendartable {width:100%; font-size:'+FBwfs+'px;}'+'\n'
				+	'#headercalendartable  tr.tablehead td{border:none; padding:0 4px 0 4px;}'+'\n'
				+	'#calendartable  tr.tablehead td{border:none; padding:0 4px 0 4px;}'+'\n'
				+	'#calendartable td{ padding:4px; }'+'\n'
				+	'#headercalendartable td{ padding:4px; }'+'\n'
				+	'#calendartable .line1, #headercalendartable .line1 {background: #'+ FBwl1 +'; color:#'+FBwlc+'; }'+'\n' 
				+	'#calendartable .line2, #headercalendartable .line2 {background: #'+FBwl2+'; color:#'+FBwlc+'; }'+'\n'
				+	'#calendartable .line3, #headercalendartable .line3 {margin:0 !important; padding:0 !important; line-height:0px!important;}'+'\n'
				+	'#calendartable .line4, #headercalendartable .line4 {margin:0 !important; padding:0 !important; line-height:14px!important;}'+'\n'
				+	'.format {width:400px; padding:4px; background:#ffc;}'+'\n'

				+	'#calendartable tr.tablecaption td { background-color:#'+FBwth+'; color:#'+FBwtc+'; border-left:none !important;}'+'\n'
				+	'#headercalendartable tr.tablecaption td { background-color:#'+FBwth+'; color:#'+FBwtc+'; border-left:none !important;}'+'\n'
				+	'#tooltip {position: absolute; z-index: 3000; border: 1px solid #111; background-color: #fffacd; padding: 5px; font-size:14px;}'+'\n'
				+	'#tooltip h3, #tooltip div { margin: 0; font-weight:normal;}'+'\n'
				+	'#p_status{float:left; margin-top:20px; color:#f00;}'+'\n'
				+	'.readmore {padding:8px;}'+'\n'
				+	'#daycont {float:right;}'+'\n'
				+	'.moretitle{font-size:120%; font-weight:bold; clear:both; padding-bottom:4px;}'+'\n'
				+	'#bannerarea {display:block; float:left; /*width:'+(FBwWidth-6)+'px;*/ height:35px; background-color:transparent; margin-left:3px;}'+'\n'
				+	'a#prbut:active, a#prbut:link,a#prbut:visited{display:block; float:left; width:16px; height:16px; background:url(http://<?php print $serverName;?>/images/widget_arrow_left.png) 0 0 no-repeat;text-decoration:none; margin-right:4px;}'+'\n'
				+	'a#prbut:hover{display:block; width:16px; height:16px; background:url(http://<?php print $serverName;?>/images/widget_arrow_left.png) 0 -16px no-repeat;text-decoration:none; margin-right:4px;}'+'\n'
				+	'a#nextbut:active, a#nextbut:link,a#nextbut:visited{display:block; float:left; width:16px; height:16px; background:url(http://<?php print $serverName;?>/images/widget_arrow_right.png) 0 0 no-repeat;text-decoration:none; }'+'\n'
				+	'a#nextbut:hover{display:block; width:16px; height:16px; background:url(http://<?php print $serverName;?>/images/widget_arrow_right.png) 0 -16px no-repeat;text-decoration:none;}'+'\n'
				+	'.FBbottomleftside1{float:left;margin-left:3px; color:#'+colorAverageOpposite(FBwBG1)+'; font-family:calibri,sans-serif; font-size:12px; padding-top:3px;}'+'\n'
				+	'.FBbottomrightside1{float:right; margin-right:3px; color:#'+colorAverageOpposite(FBwBG1)+'; font-family:calibri,sans-serif; font-size:12px; padding-top:3px;}'+'\n'
				+	'a.FBbottomlink1:active,a.FBbottomlink1:link,a.FBbottomlink1:visited{text-decoration:none; color:#'+colorAverageOpposite(FBwBG1)+';}'+'\n'
				+	'a.FBbottomlink1:hover{text-decoration:underline; color:#'+colorAverageOpposite(FBwBG1)+';}'+'\n'
				+	'a.alwaysul,a.alwaysul:active,a.alwaysul:link,a.alwaysul:visited,a.alwaysul:hover{text-decoration:underline;}'+'\n'
				+	'.paddingleft1{padding-left: '+((FBwWidth-306)/2)+'px;}'+'\n'
				+	'.leftside{display:block;width:100px; float:left;}');		
	

	style.type = 'text/css';
if(style.styleSheet)
    style.styleSheet.cssText = rules.nodeValue;
else style.appendChild(rules);
head.appendChild(style);

/*defines the div for the ajax to come out - redone with injectors*/
if(!maindoc){ 


var maindoc = document.createElement('div');
maindoc.setAttribute('id','FBcalendar');

document.getElementById('brokerCalendar').appendChild(maindoc); 
var calendartop = document.createElement('div');
calendartop.setAttribute('id','FBcalendartop');
calendartop.innerHTML = '<div id="daycontrols" class="daycontrols">'
					+	'<div id="ctimestamp" ></div>'
				/**/	+	'<div id="language">Language:&nbsp;<a href="javascript:changelanguage(\'en\');"><img src="'+flagURL+'/USD.gif" alt="en" border="0"/></a>'
					+	'&nbsp;<a href="javascript:changelanguage(\'ita\');"><img src="'+flagURL+'/it.gif" alt="ita" border="0"/></a>'
					+ 	'&nbsp;<a href="javascript:changelanguage(\'sp\');"><img src="'+flagURL+'/es.gif" alt="sp" border="0"/></a>'
					+	'&nbsp;<a href="javascript:changelanguage(\'ch\');"><img src="'+flagURL+'/cn.gif" alt="ch" border="0"/></a>'
					+	'</div>'
					+	'<div id="daycont" style="float:right; padding-left:10px; font-size:11px;"><a id="prbut" href="javascript:parsePrevDay();"></a><a id="nextbut" href="javascript:parseNextDay();"></a><br/></div></div>'
					+	'<div id="bannerarea" class="paddingleft1"></div>';

document.getElementById('FBcalendar').appendChild(calendartop);
var calendararea = document.createElement('div');
calendararea.setAttribute('id','FBcalendarArea');
document.getElementById('FBcalendar').appendChild(calendararea);

var calendarscroll = document.createElement('div');
calendarscroll.setAttribute('id','FBcalendarscroll');
document.getElementById('FBcalendarArea').appendChild(calendarscroll);
var calendarbottom = document.createElement('div');
calendarbottom.setAttribute('id','FBcalendarbottom');
calendarbottom.innerHTML = '<div class="FBbottomleftside1">Powered by <a href="http://<?php print $serverName; ?>/index.php?'+FBcid+'" target="_blank" class="FBbottomlink1 alwaysul"><b>broker.com</b></a></div>'
						+	'<div class="FBbottomrightside1"><a href="javascript:;" onMouseOver="toggleDisclaimer(\'FBdisclaimer\');" onMouseOut="toggleDisclaimer(\'FBdisclaimer\');" class="FBbottomlink1">Disclaimer</a> | <a href="http://<?php print $serverName; ?>/solutions/index.php?'+FBcid+'" class="FBbottomlink1" target="_blank">Free Widgets</a></div>';
document.getElementById('FBcalendar').appendChild(calendarbottom);

var disclaimer = document.createElement('div');
disclaimer.setAttribute('id','FBdisclaimer');
disclaimer.setAttribute('style','display:none;');
disclaimer.innerHTML =	'<b>Disclaimer:</b><br/>'
					+	'broker&#39; widgets are provided for information purposes only and under no circumstances should be regarded neither as an investment advice nor as a solicitation or an offer to sell/buy any financial product. broker assumes no responsibility or liability from gains or losses incurred by the information herein contained. Live quotes are delayed. <br/>'
					+	'<b>Risk Disclosure:</b><br/>'
					+	'There is a substantial risk of loss in trading futures and foreign exchange. Please carefully review all risk disclosure documents before opening an account as these financial instruments are not appropriate for all investors.';
					document.getElementById('FBcalendarbottom').appendChild(disclaimer);






//lets try to include the cufon and font


}

var params = "?timezone="+FBwtz+"&lang="+FBwln+FBwdpar;

var myurl = widgetURL1+params;

var lastMessage = 0;
var mTimer;
var scrme1;

//initialize the widget now
addScript(myurl);

//initialize banner
addBannerScript(bannerURL);

function parsePrevDay(){
	clearInterval(scrme1);
	day -=1;
	FBwdpar = "&d="+day;
	params = "?timezone="+FBwtz+"&lang="+FBwln+FBwdpar;
	myurl1 = widgetURL1+params;
	refreshScript(myurl1);
}

function parseNextDay(){
	clearInterval(scrme1);
	day +=1;
	FBwdpar = "&d="+day;
	params = "?timezone="+FBwtz+"&lang="+FBwln+FBwdpar;
	myurl1 = widgetURL1+params;
	refreshScript(myurl1);
}

function changelanguage(dalang){
	FBwln = dalang;
	FBwdpar = "&d="+day;
	params = "?timezone="+FBwtz+"&lang="+FBwln+FBwdpar;
	myurl1 = widgetURL1+params;
	refreshScript(myurl1);
}


function addScript(myurl) {
//	alert("script invoked");
		clearInterval(scrme1);
	var script = document.createElement('script');
	script.setAttribute('id','Bparameter');
    script.src = myurl;
	
	document.getElementsByTagName('head')[0].appendChild(script);
	document.getElementById('FBcalendarscroll').style.top = 0+'px';

}

function refreshScript(myurl){
	clearInterval(scrme1);
	var script = document.getElementById('Bparameter');
	if (script) {
			document.getElementsByTagName('head')[0].removeChild(script);

	}
	
	addScript(myurl);
}

//banner load script
function addBannerScript(myurl) {

	var bannerscript ='';
	var bannerscript = document.createElement('script');
	bannerscript.setAttribute('id','bannerdata');
    bannerscript.src = myurl;
	
	document.getElementsByTagName('head')[0].appendChild(bannerscript);

}

//banner feeded script
function jsonbanner2(myresponse){
	var bannerres = myresponse;

	var banner_div = document.getElementById('bannerarea');
	
	if(!bannerres.banner.data[0].campaign || bannerres.banner.data[0].campaign == ''){
	var FBbannercampaigncode = FBcid;
	} else {
		var FBbannercampaigncode = bannerres.banner.data[0].campaign;
	
	}
	
	if(bannerres.banner.data[0].isflash=='1'){

	var blink2 = document.createElement('div');

	
	blink2.innerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="'+bannerres.banner.data[0].width+'" height="'+bannerres.banner.data[0].height+'">'
 + '<param name="movie" value="'+bannerpicURLc+bannerres.banner.data[0].image+'" />'
  + '<param name="quality" value="high" />'
  + '<param name="flashvars" value="'+bannerres.banner.data[0].flashcode+'='+bannerres.banner.data[0].link+'?'+FBbannercampaigncode+'" />'
 + '<embed src="'+bannerpicURLc+bannerres.banner.data[0].image+'?'+bannerres.banner.data[0].flashcode+'='+bannerres.banner.data[0].link+'?'+FBbannercampaigncode+'" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="'+bannerres.banner.data[0].width+'" height="'+bannerres.banner.data[0].height+'"></embed></object>';
 	banner_div.appendChild(blink2);

	} else {
	
	
	var blink2 = document.createElement('a');
	blink2.setAttribute('href',bannerres.banner.data[0].link+'?'+FBbannercampaigncode);
	blink2.setAttribute('target','_blank');

	blink2.innerHTML = '<img src="'+bannerpicURLc+bannerres.banner.data[0].image+'" alt="'+bannerres.banner.data[0].alttext+'" border="0" />';
	banner_div.appendChild(blink2);

	}

}



function tellStatus(msg){
	var t = document.getElementById("p_status");
	t.innerHTML = msg;
}



function jsoncalendar(daresponse){
	clearInterval(scrme1);
	var response = daresponse;
	var calendar_div = document.getElementById('FBcalendarscroll'); //indicates where to write the table
	var headline_div = document.getElementById('FBcalendartop'); //indicates where to write the table
	var titleline_div = document.getElementById('ctimestamp'); //indicates where to write the table
	var prevbutid = document.getElementById('prbut');
	var nextbutid = document.getElementById('nextbut');
	//set values for vars before to populate it
	titleline_div.innerHTML ="&nbsp;";
	prevbutid.innerHTML ="&nbsp;";
	nextbutid.innerHTML ="&nbsp;";
	//set headers and buttons in language
	
		titleline_div.innerHTML = '<img src="'+priURL1+'/30b.png" border="0" alt="More" style="vertical-align:middle;" title="'+FBWversion2+'"/>&nbsp;'+response.calendar.terms[0].calendar+" "+response.calendar.timestamp[0];
		prevbutid.setAttribute ('title',response.calendar.terms[0].previous);
		nextbutid.setAttribute ('title',response.calendar.terms[0].next);
	
	if(response.calendar.data[0] == "There are no Economic indicators released today."){ //if no data for the day

	var myinnerHTML = "";
	myinnerHTML +='<tbody>';
		myinnerHTML +='<tr class="line1"><td height="100%">'+response.calendar.data[0]+'</td></tr>';
	myinnerHTML +='</tbody>';
			
			calendar_div.innerHTML = '<table id="headercalendartable" width="'+FBwWidth+'" height="100%" cellspacing="0" cellpadding="0">'+myinnerHTML+'</table>';
		mTimer = setTimeout('refreshScript(myurl);',refreshrate); //Refresh content every given interval

	
	
	} else {


	var myinnerHTML = "";
		myinnerHTML +='<thead><tr class="tablehead">';
		//the single pixel row adjuster
		for(i=0; i<response.calendar.header.length;i++){
			myinnerHTML += '<td><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="16" height="1"/></td>';
		}
		
		myinnerHTML +='</tr><tr class="tablecaption">';
		//the table header with text
	
			
				myinnerHTML += '<td width="16"><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="16" height="1"/></td>';
				myinnerHTML += '<td>'+response.calendar.header[1]+'</td>';
				myinnerHTML += '<td>'+response.calendar.header[2]+'</td>';
				myinnerHTML += '<td width="16"><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="16" height="1"/></td>';
				myinnerHTML += '<td>'+response.calendar.header[4]+'</td>';
				myinnerHTML += '<td>'+response.calendar.header[5]+'</td>';
				myinnerHTML += '<td>'+response.calendar.header[6]+'</td>';
				myinnerHTML += '<td>'+response.calendar.header[7]+'</td>';
	
		
		myinnerHTML +='</tr></thead>';

		
		//the table body and datas

		
		myinnerHTML +='<tbody>';
	
		for(i=0;i < response.calendar.data.length; i++) {
		

			
			myinnerHTML +='<tr class="line1">';
			myinnerHTML +='<td><img src='+flagURL+'/'+response.calendar.data[i].currency+'.gif alt="'+response.calendar.data[i].currency+'"/></td>';
			myinnerHTML +='<td>'+response.calendar.data[i].time.substr()+'</td>';
			myinnerHTML +='<td><span class="longdesc" title="'+response.calendar.data[i].description+'">'+response.calendar.data[i].report+'</span><br/><img src='+priURL1+'/'+response.calendar.data[i].impact+'.gif alt="'+response.calendar.data[i].impact.replace("_"," ")+'"/></td>';

			myinnerHTML +='<td><a id="ln'+i+'" href="javascript:;" onClick="toggleMe(\'hid'+i+'\',\'ln'+i+'\');"><img src="'+priURL1+'/29.png" border="0" alt="More"/></a></td>';
			myinnerHTML +='<td><a href="'+response.calendar.data[i].webpage+'" target="_blank">'+response.calendar.data[i].source+'</a></td>';
			myinnerHTML +='<td>'+response.calendar.data[i].previous+'&nbsp;</td>';
			myinnerHTML +='<td>'+response.calendar.data[i].forecast+'&nbsp;</td>';
			myinnerHTML +='<td>'+response.calendar.data[i].actual+'&nbsp;</td>';
			myinnerHTML +='</tr>';
			myinnerHTML +=	'<tr class="line1 line4" ><td colspan="8"  class="line4" ><div id="hid'+i+'"class="readmore" style="display:none;"><div class="moretitle">'+response.calendar.data[i].report+'</div>'
						+	'<div class="leftside"><img src='+flagURL+'/'+response.calendar.data[i].currency+'.gif alt="'+response.calendar.data[i].currency+'"/>&nbsp;<b>'+response.calendar.data[i].currency+'</b><br/>'
						+	'<div>Actual:'+response.calendar.data[i].actual+'<br/>Forecast:'+response.calendar.data[i].forecast+'<br/>Previous:'+response.calendar.data[i].previous+'</div></div>'
						+	'<div>'+response.calendar.data[i].description+'</div></div></td></tr>';
			
			
						myinnerHTML +='<tr class="line2 line3"><td colspan="8" class="line3"><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="16" height="2"/></td></tr>';
			

			
			

		} //end for
		
		myinnerHTML +='<tr class="tablecaption">';
		//the table header with text
						myinnerHTML += '<td width="16"><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="16" height="1"/></td>';
				myinnerHTML += '<td>'+response.calendar.header[1]+'</td>';
				myinnerHTML += '<td>'+response.calendar.header[2]+'</td>';
				myinnerHTML += '<td width="16"><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="16" height="1"/></td>';
				myinnerHTML += '<td>'+response.calendar.header[4]+'</td>';
				myinnerHTML += '<td>'+response.calendar.header[5]+'</td>';
				myinnerHTML += '<td>'+response.calendar.header[6]+'</td>';
				myinnerHTML += '<td>'+response.calendar.header[7]+'</td>';
		
		
		myinnerHTML +='</tr>';
		
		
		myinnerHTML +='</tbody>';

			calendar_div.innerHTML = '<table id="calendartable" width="'+FBwWidth+'" cellspacing="0" cellpadding="0">'+myinnerHTML+'</table>';
		
		mTimer = setTimeout('refreshScript(myurl);',refreshrate); //Refresh content every given interval
	if(FBscroll =='1'){
		clearInterval(scrme1);
		LoadNewsScrollers1();

		scrme1 = setInterval('AutoScrollNewsDown1()',150);
	}
		} //end if
}

function toggleMe(id,aid){
	 var e = document.getElementById(id);
	 var a = document.getElementById(aid);
       if(e.style.display == 'block') {
          e.style.display = 'none';
		  a.innerHTML = '<img src="'+priURL1+'/29.png" border="0" alt="More"/>';
       } else {
          e.style.display = 'block';
		    a.innerHTML = '<img src="'+priURL1+'/30.png" border="0" alt="More"/>';
       } //end if
}

function toggleDisclaimer(id){
	 var e = document.getElementById(id);

       if(e.style.display == 'block') {
          e.style.display = 'none';
 
       } else {
          e.style.display = 'block';

       } //end if

}

var gpy;
var gStopNews1;

//getElementById absstractor from the old common.js

var null_element;
function spsGetElementById(id) {
	if (document.getElementById(id)) {
		return document.getElementById(id);
	} else if (document.all) {
		return document.all[id];
	} else if (document.layers && document.layers[id]) {
		return (document.layers[id]);
	} else {
		return null_element;
	}
}




//an addEventLinsnet abstractor from the old common.js

function yAddEventListener(es,eventType,eventListener,useCapture) {
	var e1;
	if(!(e1=spsGetElementById(es))) return;
	eventType=eventType.toLowerCase();
	var eh="e1.on"+eventType+"=eventListener";
	if(e1.addEventListener) e1.addEventListener(eventType,eventListener,useCapture);
	else if(e1.attachEvent) e1.attachEvent("on"+eventType,eventListener);
	else if(e.captureEvents) {
		if(useCapture||(eventType.indexOf('mousemove')!=-1)) { e1.captureEvents(eval("Event."+eventType.toUpperCase())); }
		eval(eh);
	}
	else eval(eh);
}



function AutoScrollNewsDown1() {
	var t,u, eh;

	if(gStopNews1) {
	return;
	}
	t= document.getElementById('FBcalendarscroll');
	u = document.getElementById('calendartable');
	eh = u.offsetHeight;
	
	ewh = -(eh -  (parseInt(FBwHeight)-130));
	

	if(eh+gpy<0) gpy=0;
	gpy-=2;
	t.style.top=gpy+'px';
	
}

function AutoScrollNewsStop1() {
	gStopNews1=true;
}

function AutoScrollNewsStart1() {
	
	gStopNews1=false;
}

function LoadNewsScrollers1(){
	
	var t,u, eh;

	gpy=0;
	t= document.getElementById('FBcalendarscroll');
	u = document.getElementById('calendartable');
	eh = u.offsetHeight;

	ewh = -(eh -  (parseInt(FBwHeight)-130));

	if(eh<ewh) return;
	gStopNews1=false;
	yAddEventListener('FBcalendarArea','mouseover',AutoScrollNewsStop1,false);
	yAddEventListener('FBcalendarArea','mouseout',AutoScrollNewsStart1,false);


//	
}

<?php


$ref= (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '';
$url = stripslashes(strip_tags($ref));
$start = date('Y/m/d g:i a');
$end = date('Y/m/d g:i a');
$widget = 'eco_cal_widget';
$session_id = time();


mysql_query("INSERT INTO tbl_widgets_tracker (session_id,widget_name,url,start) VALUES ('".$session_id."','".$widget."','".$url."', '".$start."')");	


return;



?>
