<?php
/**** Forex Quotes JSON code widget
	* code by Simone Bernacchia
	*/



include('../includes/functions_simone.php');

connect_db();

$fbwwidth = sanitizer($_GET['ww']); /*widget width*/
$fbwheight = sanitizer($_GET['wh']); /*widget height*/
$fbwbg = sanitizer(substr($_GET['bg'],0,6)); //main background
$fbwl1 = sanitizer(substr($_GET['l1'],0,6)); //line 1 color
$fbwl2 = sanitizer(substr($_GET['l2'],0,6)); // line 2 color
$fbwlc = sanitizer(substr($_GET['lc'],0,6)); //line character color
$fbwfont = sanitizer($_GET['font']); //font
$fbwfs = sanitizer($_GET['fs']); //font size in pixel
$fbwth = sanitizer(substr($_GET['th'],0,6)); //headline color
$fbwtc = sanitizer(substr($_GET['tc'],0,6)); //headline character color
$fbwtz = sanitizer(substr($_GET['tz'],0,6)); //timezone offset
$fbwln = sanitizer(substr($_GET['ln'],0,6)); //language
$fbcid= sanitizer($_GET['pcode']); //campaign
$fbpair= sanitizer($_GET['pair']); // for quotes - values

//defaults if no choice is made

if(!$fbwtz || $fbwtz == ""){$fbwtz = "-08:00";}
if(!$fbwln || $fbwln == ""){$fbwln = "en";}

if(!$fbwwidth || $fbwwidth == ""){$fbwwidth = "400";}
if(!$fbwheight || $fbwheight == ""){$fbwheight = "400";}
if (!$fbpair || $fbpair=="") {$pair="01011100001100000001";}

?>

var FBwWidth = <?php print $fbwwidth; ?>;
var FBwHeight = <?php print $fbwheight; ?>;
var FBwBG = '<?php print $fbwbg; ?>';
var FBwl1 = '<?php print $fbwl1; ?>';
var FBwl2 = '<?php print $fbwl2; ?>';
var FBwlc = '<?php print $fbwlc; ?>';
var FBwfont = '<?php print $fbwfont; ?>';
var FBwfs = '<?php print $fbwfs; ?>';
var FBwth = '<?php print $fbwth; ?>';
var FBwtc = '<?php print $fbwtc; ?>';
var FBwtz = '<?php print $fbwtz; ?>';
var FBwln = '<?php print $fbwln; ?>';
var FBcid = '<?php print $fbcid; ?>';
var FBpair = '<?php print $fbpair; ?>';



var refreshrate = 15000; //page refresh rate - every 15 seconds
//the params from the host site
var widgetURL ="http://<?php print $serverName; ?>/widgets/quotes-json_all.php";
var flagURL = "http://<?php print $serverName; ?>/widgets/flags";
var priURL = "http://<?php print $serverName; ?>/widgets/img";
var iconsURL = "http://<?php print $serverName; ?>/widgets/icons";
var bannerURL = "http://<?php print $serverName; ?>/widgets/banner_feeder.php";
var bannerpicURL = "http://<?php print $serverName; ?>/widgets/banners/";
var d = new Date();
var session_id = d.getTime();
var url = window.location.href;
var day = 0;
var FBwdpar;
FBwFontList = ["Verdana","Arial","Tahoma","Trebuchet MS","Calibri"];
var todaysdate = '<?php echo date('l, F j Y'); ?>';

// takes a hex string and returns the r, g, b values 
	function parseColor(text){
		var ot = text;
		if(ot.length == 3){
		t1 = ot.substr(0,1);
		t2 = ot.substr(1,1);
		t3 = ot.substr(2,1);
		ot = t1+t1+t2+t2+t3+t3;
	//	alert("ot = "+ot);
		text = ot;
		}
	    // this regular expression checks for a hex color in proper format 
	    if(/^\#?[0-9A-F]{6}$/i.test(text)){ 
	        return { 
	            r: eval('0x'+text.substr(text.length==6?0:1, 2)), 
	            g: eval('0x'+text.substr(text.length==6?2:3, 2)), 
	            b: eval('0x'+text.substr(text.length==6?4:5, 2)) 
	        } 
	    } 
		
//		return {r:255,g:0,b:0};
	} 
	
	function hex(c){ 
    c=parseInt(c).toString(16); 
    return c.length<2?"0"+c:c 
	} 
	
function colorAverageOpposite(daColor){
	var colorcomponent = parseColor(daColor);
	daaverage = Math.round((colorcomponent.r+colorcomponent.g+colorcomponent.b)/3);
	if (daaverage<=128){
	return "fff";
	} else {
		return "000";
	}
}

/*defines the styleSheets*/

var head = document.getElementsByTagName('head')[0],
    style = document.createElement('style'),
    rules = document.createTextNode('#FBquotes{display:block; width:'+ FBwWidth +'px; height:'+ FBwHeight +'px; background: #'+FBwBG+';}'+'\n'
				+	'#FBquotesArea{display:block; width:'+ (FBwWidth-4) +'px; height:'+ (FBwHeight-90) +'px; overflow:hidden; font-family:'+FBwFontList[FBwfont]+',sans-serif; position:absolute;}'+'\n'
				+	'#FBquotestop{display:block; width:'+FBwWidth+'px; height:63px; background-color:#'+FBwBG+';}'+'\n'
				+	'#FBquotesscroll{display:block; position:absolute; width:'+ (FBwWidth-6) +'px; height:'+ (FBwHeight-90) +'px; left:3px;}'+'\n'
+	'#FBquotesbottom {display:block; position:relative; width:'+FBwWidth+'px; height:20px; top:'+(FBwHeight -20)+'px; font-family:'+FBwFontList[FBwfont]+',sans-serif; font-size:80%; background:#'+FBwBG+'; color:#'+colorAverageOpposite(FBwBG)+';}'+'\n'				
				+	'#timestamp{float:left; font-family:calibri,sans-serif; font-size:12px;  color: #'+colorAverageOpposite(FBwBG)+'; margin-left:3px; padding-top:3px; padding-bottom:3px; }'+'\n'
				+	'#FBbannerArea {display:block; float:left; /*width:'+(FBwWidth-6)+'px;*/ height:35px; background-color:transparent; margin-left:3px;}'+'\n'
				+	'#FBdisclaimer{display:none; width:'+(FBwWidth-10)+'px; height:120px; position:absolute; top:'+(FBwHeight -155)+'px; margin-left:3px; padding:3px; overflow:none; z-index:3000; background-color: #'+ FBwl2 +'; font-family: calibri,sans-serif; font-size:11px; color:#'+colorAverageOpposite(FBwl2)+'; /*letter-spacing:-1px;*/ filter:alpha(opacity=80); opacity: 0.8; -moz-opacity:0.8;}'+'\n'
				+	'table{width:'+FBwWidth+'; font-size:'+FBwfs+'px;}'+'\n'
				+	'tr{border-bottom: 1px #'+FBwl1+' solid; padding:0 4px 0 4px;}'+'\n'
				+	'tr .tablehead td{padding:0px !important;}'+'\n'
				+	'tr .tablecaption td{height:12px !important;}'+'\n'
				+	'td{  padding:4px;}'+'\n'
				+	'.line1{background: #'+FBwl1+'; color:#'+FBwlc+'; }'+'\n'
				+	'tr .line1 td{border-bottom: 1px #'+FBwth+' solid;}'+'\n'
				+	'tr .line2 td{border-bottom: 1px #'+FBwth+' solid;}'+'\n'
				+	'.line2 {background: #'+FBwl2+'; color:#'+FBwlc+';  }'+'\n'
				+	'.currency{font-weight:bold; font-size:105%;}'+'\n'
				+	'.format {width:400px; padding:4px; background:#ffc;}'+'\n'
				+	'tr.tablecaption td { background-color:#'+FBwth+'; color:#'+FBwtc+'; border-bottom: 1px solid;}'+'\n'
				+	'.fbctitle{font-size:120%; padding-bottom:8px; color:'+FBwtc+'; font-weight:bold;}'+'\n'	
				+	'.fbctitle a {color:'+FBwtc+'; text-decoration:none;}'+'\n'
				+	'#tooltip h3, #tooltip div { margin: 0; font-weight:normal;}'+'\n'
				+	'.moretitle{font-size:120%; font-weight:bold; clear:both; padding-bottom:4px;}'+'\n'
				+	'.FBbottomleftside{float:left;margin-left:3px; color:#'+colorAverageOpposite(FBwBG)+'; font-family:calibri,sans-serif; font-size:12px; padding-top:3px;}'+'\n'
				+	'.FBbottomrightside{float:right; margin-right:3px; color:#'+colorAverageOpposite(FBwBG)+'; font-family:calibri,sans-serif; font-size:12px; padding-top:3px;}'+'\n'
				+	'a.FBbottomlink:active,a.FBbottomlink:link,a.FBbottomlink:visited{text-decoration:none; color:#'+colorAverageOpposite(FBwBG)+';}'+'\n'
				+	'a.FBbottomlink:hover{text-decoration:underline; color:#'+colorAverageOpposite(FBwBG)+';}'+'\n'
				+	'a.alwaysul,a.alwaysul:active,a.alwaysul:link,a.alwaysul:visited,a.alwaysul:hover{text-decoration:underline;}'+'\n'
				+	'.paddingleft{padding-left: '+((FBwWidth-306)/2)+'px;}'+'\n'
				+	'.leftside{display:block;width:100px; float:left;}'+'\n');		
	

	
	style.type = 'text/css';
if(style.styleSheet)
    style.styleSheet.cssText = rules.nodeValue;
else style.appendChild(rules);
head.appendChild(style);



/*defines the div for the ajax to come out - redone with injectors*/
if(!qmaindoc) {
var qmaindoc = document.createElement('div');
qmaindoc.setAttribute('id','FBquotes');
document.getElementsByTagName('body')[0].appendChild(qmaindoc); 

var qcalendartop = document.createElement('div');
qcalendartop.setAttribute('id','FBquotestop'); 
qcalendartop.innerHTML = '<div id="timestamp"><img src="'+priURL+'/31.png" border="0" alt="More" style="vertical-align:middle;"/>&nbsp;Forex Live Quotes</div><div id="FBbannerArea" class="paddingleft"></div>';

document.getElementById('FBquotes').appendChild(qcalendartop);
var qcalendararea = document.createElement('div');
qcalendararea.setAttribute('id','FBquotesArea');
document.getElementById('FBquotes').appendChild(qcalendararea);
var qdisclaimer = document.createElement('div');
qdisclaimer.setAttribute('id','FBdisclaimer');
qdisclaimer.setAttribute('style','display:none;');
qdisclaimer.innerHTML =	'<b>Disclaimer:</b><br/>'
					+	'broker&#39; widgets are provided for information purposes only and under no circumstances should be regarded neither as an investment advice nor as a solicitation or an offer to sell/buy any financial product. broker assumes no responsibility or liability from gains or losses incurred by the information herein contained. Live quotes are delayed. <br/>'
					+	'<b>Risk Disclosure:</b><br/>'
					+	'There is a substantial risk of loss in trading futures and foreign exchange. Please carefully review all risk disclosure documents before opening an account as these financial instruments are not appropriate for all investors.';
					document.getElementById('FBquotes').appendChild(qdisclaimer);
var qcalendarscroll = document.createElement('div');
qcalendarscroll.setAttribute('id','FBquotesscroll');
document.getElementById('FBquotesArea').appendChild(qcalendarscroll);
var qbottom = document.createElement('div');
qbottom.setAttribute('id','FBquotesbottom');
qbottom.innerHTML = '<div class="FBbottomleftside">Powered by <a href="http://<?php print $serverName; ?>/index.php?'+FBcid+'" target="_blank" class="FBbottomlink alwaysul"><b>broker.com</b></a></div>'
						+	'<div class="FBbottomrightside"><a href="javascript:;" onMouseOver="toggleDisclaimer(\'FBdisclaimer\');" onMouseOut="toggleDisclaimer(\'FBdisclaimer\');" class="FBbottomlink">Disclaimer</a> | <a href="http://<?php print $serverName; ?>/solutions/index.php?'+FBcid+'" class="FBbottomlink" target="_blank">Free Widgets</a></div>';
document.getElementById('FBquotes').appendChild(qbottom);
}

var params = "?pair="+FBpair;

var myurl = widgetURL+params;

var lastMessage = 0;
var mTimer;
var scrme;

//initialize the widget now
addScript(myurl);

//initialize banner
addBannerScript(bannerURL);


function addScript(myurl) {

		clearInterval(scrme);
	var script = document.createElement('script');
	script.setAttribute('id','Cparameter');
    script.src = myurl;

	document.getElementsByTagName('head')[0].appendChild(script);

}

function refreshScript(myurl){
	clearInterval(scrme);
	var script = document.getElementById('Cparameter');
	if (script) {
			document.getElementsByTagName('head')[0].removeChild(script);

	}
	
	addScript(myurl);
}

//banner load script
function addBannerScript(myurl) {

	var bannerscript = document.createElement('script');
	bannerscript.setAttribute('id','bannerdata');
    bannerscript.src = myurl;
	
	document.getElementsByTagName('head')[0].appendChild(bannerscript);

}

//banner feeded script
function jsonbanner(myresponse){
	var bannerres = myresponse;
	var banner_div = document.getElementById('FBbannerArea');
		if(bannerres.banner.data[0].isflash=='1'){

	var blink = document.createElement('div');

	
	blink.innerHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="'+bannerres.banner.data[0].width+'" height="'+bannerres.banner.data[0].height+'">'
 + '<param name="movie" value="'+bannerpicURL+bannerres.banner.data[0].image+'" />'
  + '<param name="quality" value="high" />'
  + '<param name="flashvars" value="'+bannerres.banner.data[0].flashcode+'='+bannerres.banner.data[0].link+'?'+FBcid+'" />'
 + '<embed src="'+bannerpicURL+bannerres.banner.data[0].image+'?'+bannerres.banner.data[0].flashcode+'='+bannerres.banner.data[0].link+'?'+FBcid+'" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="'+bannerres.banner.data[0].width+'" height="'+bannerres.banner.data[0].height+'"></embed></object>';
 	banner_div.appendChild(blink);
	} else {
	
	
	var blink = document.createElement('a');
	blink.setAttribute('href',bannerres.banner.data[0].link+'?'+FBcid);
	blink.setAttribute('target','_blank');

	blink.innerHTML = '<img src="'+bannerpicURL+'/'+bannerres.banner.data[0].image+'" alt="'+bannerres.banner.data[0].alttext+'" border="0" />';
	banner_div.appendChild(blink);
	
	} //end if
}



function tellStatus(){
	alert('loaded');
}

function processFlags(dasymbol){

	var symbol = dasymbol;
	var symbol1 = symbol.substr(0,3).toUpperCase();
	var symbol2 = symbol.substr(4,3).toUpperCase();
	var output = 	'<div style="float:right;"><img src="'+flagURL+'/'+symbol1+'.gif" alt="'+symbol1+'"/>&nbsp;<img src="'+flagURL+'/'+symbol2+'.gif" alt="'+symbol2+'"/></div>';

	return output;
}

function jsonquotes(daresponse){
	clearInterval(scrme);

	var alticon;
	var response = daresponse;
	var color;

	var quotes_div = document.getElementById('FBquotesscroll'); //indicates where to write the table
	var headline_div = document.getElementById('FBquotestop'); //indicates where to write the table
	var myinnerHTML = "";
		myinnerHTML +='<thead><tr class="tablehead" style="padding:0;">';
		//the single pixel row adjuster
		
			myinnerHTML += '<td style="padding:0;"><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="64" height="1"/></td>';
			myinnerHTML += '<td style="padding:0;"><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="48" height="1"/></td>';
			myinnerHTML += '<td style="padding:0;"><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="64" height="1"/></td>';
			myinnerHTML += '<td style="padding:0;"><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="64" height="1"/></td>';
			myinnerHTML += '<td style="padding:0;"><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="64" height="1"/></td>';
			myinnerHTML += '<td style="padding:0;"><img src="http://<?php print $serverName; ?>/images/pixelino.gif" width="48" height="1"/></td>';
			myinnerHTML +='</tr>';
			myinnerHTML +='<tr class="tablecaption">';
			myinnerHTML += '<td>Currency</td>';
			myinnerHTML += '<td>&nbsp;</td>';
			myinnerHTML += '<td>Date</td>';
			myinnerHTML += '<td>Ask</td>';
			myinnerHTML += '<td>Bid</td>';
			myinnerHTML += '<td>Spread</td>';
			// the headlines go here
		myinnerHTML +='</tr></thead>';
			

		//the table body and datas
		
		
		myinnerHTML +='<tbody>';
		color = '1';
		for(i=0;i < response.quotes.data.length; i++) {
			var tradedate = response.quotes.data[i].tradedate[0];
		if(color=='1'){

			
			myinnerHTML +='<tr class="line1" >';
			myinnerHTML +='<td valign="top" class="currency">'+response.quotes.data[i].symbol[0].toUpperCase()+'</td>';
			myinnerHTML +='<td valign="top" class="currency">'+processFlags(response.quotes.data[i].symbol[0])+'</td>';
			myinnerHTML +='<td valign="top">'+response.quotes.data[i].tradedate[0]+'</td>';
			myinnerHTML +='<td valign="top">'+response.quotes.data[i].ask[0]+'</td>';
			myinnerHTML +='<td valign="top">'+response.quotes.data[i].bid[0]+'</td>';
			myinnerHTML +='<td valign="top">'+response.quotes.data[i].spread[0]+'</td>';
			myinnerHTML +='</tr>';
			color='2';
		} else {
			myinnerHTML +='<tr class="line2" >';
			myinnerHTML +='<td valign="top" class="currency">'+response.quotes.data[i].symbol[0].toUpperCase()+'</td>';
			myinnerHTML +='<td valign="top" class="currency">'+processFlags(response.quotes.data[i].symbol[0])+'</td>';
			myinnerHTML +='<td valign="top">'+response.quotes.data[i].tradedate+'</td>';
			myinnerHTML +='<td valign="top">'+response.quotes.data[i].ask+'</td>';
			myinnerHTML +='<td valign="top">'+response.quotes.data[i].bid+'</td>';
			myinnerHTML +='<td valign="top">'+response.quotes.data[i].spread+'</td>';
			myinnerHTML +='</tr>';
			color='1';
		}
		
		} //end for
		
		
		
		
		myinnerHTML +='</tbody>';
		quotes_div.innerHTML = '<table id="quotestable" width="'+FBwWidth+'" height="100%" cellspacing="0" cellpadding="0">'+myinnerHTML+'</table>';
		mTimer = setTimeout('refreshScript(myurl);',refreshrate); //Refresh content every given interval
		clearInterval(scrme);

}

function toggleMe(id,aid){
	 var e = document.getElementById(id);
	 var a = document.getElementById(aid);
       if(e.style.display == 'block') {
          e.style.display = 'none';
		  a.innerHTML = "More";
       } else {
          e.style.display = 'block';
		   a.innerHTML = "Less";
       } //end if

}

function toggleDisclaimer(id){
	 var e = document.getElementById(id);

       if(e.style.display == 'block') {
          e.style.display = 'none';

       } else {
          e.style.display = 'block';

       } //end if

}

var gpy;
var gStopNews;

//getElementById absstractor from the old common.js

var null_element;
function spsGetElementById(id) {
	if (document.getElementById(id)) {
		return document.getElementById(id);
	} else if (document.all) {
		return document.all[id];
	} else if (document.layers && document.layers[id]) {
		return (document.layers[id]);
	} else {
		return null_element;
	}
}




//an addEventLinsnet abstractor from the old common.js

function xAddEventListener(es,eventType,eventListener,useCapture) {
	var e;
	if(!(e=spsGetElementById(es))) return;
	eventType=eventType.toLowerCase();
	var eh="e.on"+eventType+"=eventListener";
	if(e.addEventListener) e.addEventListener(eventType,eventListener,useCapture);
	else if(e.attachEvent) e.attachEvent("on"+eventType,eventListener);
	else if(e.captureEvents) {
		if(useCapture||(eventType.indexOf('mousemove')!=-1)) { e.captureEvents(eval("Event."+eventType.toUpperCase())); }
		eval(eh);
	}
	else eval(eh);
}









<?php


$ref= (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '';
$url = stripslashes(strip_tags($ref));
$start = date('Y/m/d g:i a');
$end = date('Y/m/d g:i a');
$widget = 'quotes_widget';
$session_id = time();


mysql_query("INSERT INTO tbl_widgets_tracker (session_id,widget_name,url,start) VALUES ('".$session_id."','".$widget."','".$url."', '".$start."')");	


return;



?>