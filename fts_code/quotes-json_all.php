<?php 
   /**
	* Quotetable JSONP feed for widget
	* Simone Bernacchia 
	* 
	*
	* @package: Widgets
	*
	*
	*/
	
	//modified: 12-29-2010 - modified JSON
	
	
	header("Cache-Control: no-cache");
//sleep(2);


	include('/home/fbro001/includes/spreads_quotes/spreads_and_quotes.php');	
	
	connect_db();
	
	$provider = 'alparius';


$json ="";
//==============================================================
//blacklist check			
			
	include('../includes/domainblacklist.php');

$allowed = 1;
	
foreach($blacklistDomains as $j=>$value){
			
	if(stristr($_SERVER['HTTP_REFERER'] , $blacklistDomains[$j] )!= false ) {			
		$allowed = 0;
		$json.='/*bingo*/ ';
	} //end if stristr
//	$json.='compare result: '
} //end foreach
//==============================================================
			
			
if($allowed == 1){
			
$pair=sanitizer($_GET["pair"]);//lookup all hints from array if length of q>0
if (!$pair){$pair="01011100001100000001";}
	
	//reset table value
    $table="";
		  
			
			$query = "";
			
	//		$symbols = array("eur/usd", "usd/jpy", "gbp/usd", "aud/usd", "usd/chf", "usd/cad", "eur/gbp", "eur/jpy", "gbp/jpy", "gbp/chf");
			$symbols = array("usd/jpy", "eur/usd", "gbp/usd", "usd/chf", "usd/cad", "aud/usd", "eur/gbp", "eur/jpy", "gbp/jpy", "eur/chf", "usd/mxn", "chf/jpy", "gbp/chf", "eur/aud", "eur/cad", "aud/cad", "aud/jpy", "nzd/usd", "aud/nzd", "cad/jpy");


//for composing the array
$tempSymbolsArr = array();

			
			foreach ($symbols as $i => $value) {
	    
			

	    	
			if(substr($pair,$i,1) =="1"){
			
			
				array_push($tempSymbolsArr,$symbols[$i]);
				
			
			}	//end if
		
		} //end for

//debug output 

$currentSymbols = implode(',',$tempSymbolsArr);			
		
		

	//start JSONP feed
	

	
	
	$json.='/*host: '.$_SERVER['HTTP_HOST'].' - http_referrer:'.$_SERVER['HTTP_REFERER'].' - allowed: '.$allowed.' - symbols: '.$currentSymbols.'*/ ';
	
	
	$json.='jsonquotes({"quotes": ';
	
	//start data part
$json .= '{ "data":[';


	
	
	$result = preg_replace("/[\n\r]/","|",getTicksScreenShot($tempSymbolsArr, $provider, 1, 30));
	
	$quotesArray = 	explode("|", $result);	
	
	
	
	$quotesArrCleaner = array_pop($quotesArray);

	$numrecords = (count($quotesArray)-1);
	
	if(count($quotesArray) == 0 || !count($quotesArray)) {
	
	

		
		//send if no data available


		$json .="[\"There are no quotes today.\"]";

	
		$json.="]}";
		

				} else {
			$idx = 0;
			$currentcurrency ="";

			
		
			for($j=1; $j<count($quotesArray); $j++){

			$tempArrayDataVal = explode(',',$quotesArray[$j]);
			
			/*
			[0] => SYMBOL 
			[1] => TRADEDATE 
			[2] => PRICE 
			[3] => LVOLUME 
			[4] => TVOLUME 
			[5] => ASK 
			[6] => ASKSIZE 
			[7] => BID 
			[8] => BIDSIZE 
			[9] => SPREAD 
			*/
			
			$currSymbol = $tempArrayDataVal[0];
			$currDate = $tempArrayDataVal[1];
			$currPrice = $tempArrayDataVal[2];
			$currVolume = $tempArrayDataVal[3];
			$currBid = $tempArrayDataVal[7];
			$currAsk = $tempArrayDataVal[5];
	
	

			
			
			
			
			$symbol = $currSymbol;

			$tradedateTempArr = explode(' ',$currDate,2);
			$tradedate = $tradedateTempArr[1];			
			$bid = $currBid;
			$ask = $currAsk;
			
			$spread = computeSpread($bid,$ask,$symbol);
			$json.="{";
			$json.='"symbol": ["'.$symbol.'"],';
			$json.='"tradedate": ["'.$tradedate.'"],';

			$json.='"bid": ["'.formatCurrency($bid,$symbol).'"],';
			$json.='"ask": ["'.formatCurrency($ask,$symbol).'"],';

			$json.='"spread": ["'.$spread.'"]';
			$json.='}';
				
				if($ct<($numrecords-1)){
					$ct++;
					$json.=",";
				}//end if

			
			} //end while
			
			//end data block
			$json.="]}";
			
			} //end if
	//end JSONP feed
	$json.="});";

	echo($json);

	
//	freeresult($result);
} //end if allowed


function formatCurrency($value,$currency){

	$stringvalue = strval($value);
	$integer = substr($stringvalue,0,strpos($stringvalue,"."));

	$point = strstr($stringvalue,".");
	
	
	
	if(preg_match("/jpy/i",$currency)){
		$goodpoint = substr($point,1,2);
		$finalpoint = substr($point,3,1);
		$pointstring = ".".$goodpoint."<sup>".$finalpoint."</sup>";
	
	} else {
		$goodpoint = substr($point,1,4);
		$finalpoint = substr($point,5,1);
		$pointstring = ".".$goodpoint."<sup>".$finalpoint."</sup>";
	} //end if
	
		$pricestring = $integer.$pointstring;
		return $pricestring;
} //end function




function computeSpread($daBid,$daAsk,$currency){

	
	
	
	if(preg_match("/jpy/i",$currency)){
		$unit = 0.010;
		$spreadval = round((($daAsk - $daBid) / $unit),2);

		} else {
		$unit = 0.00010;
		$spreadval = round((($daAsk - $daBid) / $unit),2); 

	return $spreadval;
}
?>