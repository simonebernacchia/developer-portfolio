   /**
    * Spread Comparisons live table script
	* Using datas from the web service retrieve and display spreads 
	* for a given list of currency pairs
	*
	* made by Bernacchia Simone 
	*
	* based on old frame and quotes scripts
	*
	* hope this works ^^
	*
	*
	*

	* @Date:		08-25-2011 16:16:00 -0800 (creation)
	* @Author:		Simone Bernacchia < simonebernacchia@gmail.com >
	*
	*/



	
	
$(document).ready(function(){
	
		
//patch to ajax for jQuery 1.4.3 - from atomicObject.com

var myAjax = function(settings) {
  $.ajax($.extend({}, settings, {
    complete: function (xhr, status) {
      if (settings.complete) {
        settings.complete(xhr, status);
      }

      // Avoid IE memory leak
      xhr.onreadystatechange = null;
      xhr.abort = null;
    }
  }));
};
	
	
	
	
	
	
	var updateRate = 5000; //every 5 seconds web service get called
	var currentDate = new Date();
	var offset = currentDate.getTimezoneOffset();
	var dummyval = currentDate.getTime();	
	var weekday = currentDate.getDay();
	var dayhour = currentDate.getHours();
	
	var isWeekend;
	
	
	console.log(dummyval);
	console.log ('today is:'+weekday);
	console.log ('offset from GMT is:'+offset);
	
	
//=============================================================================	
//make the spread work only on monday to friday
	
if((weekday >0 && weekday <6) &&(dayhour >8 && dayhour <18 ) ){
	//set interval for refreshing table
	spreadRefresher = setInterval(updateSpreadsTable,updateRate);
	
	isWeekend = false;
	
} else {
	if(spreadRefresher){
		clearInterval(spreadRefresher);
	} //end if
	
	isWeekend = true;
	
} //end if 

//functions to handle the spreads comparison; those will work only on weekends

if(isWeekend){

	//spreads comparison
	updateFlashChart('#averageDisplayform','averageDisplaycontainer','averageDisplayChart',{charttype:'showaverage',timeframe:'<?php print $chosentf; ?>',numvalues:'100',myprovider:'',parameter:'<?php print $chosenMainParamList; ?>', width:470,height:100});/**/	
	
	$('#averageDisplayform button').click(function(){
	 //retrieve the parameters to pass at the function up
		var charttype = $('#averageDisplayform  input[name="charttype"]').val();
		var timeframe = $('#averageDisplayform select[name="timeframe"]').val();
		var numvalues = $('#averageDisplayform select[name="numvals"]').val();
		var provider1 = $('#averageDisplayform  select[name="provider"]').val();
		var provider2 = $('#averageDisplayform  select[name="provider2"]').val();
		var parameter = $('#averageDisplayform  select[name="symbol"]').val();
		
		var fullParam = provider1+'|'+provider2+'|'+parameter;
		
		var chartWidth = 470;
		var chartHeight = 100;
	 //create a singleton object to pass parameters
	 var myparams = { charttype: charttype, timeframe: timeframe, numvalues: numvalues, myprovider:'' , parameter: fullParam, width: chartWidth, height: chartHeight };
	 

	 
	 updateFlashChart('#averageDisplayform','averageDisplaycontainer','averageDisplay',myparams);

	 
	 return false;
	});

} //end if
	
//=============================================================================	
	/**
	 * reload datas from the Ajax Spread feeder and display it on the homepage
	 *
	 */
	
	function updateSpreadsTable(){
	
	//	$.ajax({
		myAjax({
					url:'feeds/ajax_home_spread_comparison_feeder.php',
					data:'',
					success:function(html){

							myhtml = html;
							$('#comparisonTable').html(myhtml);
		
						//inject table on the layout	
					}
		}); //end ajax

	
	} //end function
	
	
	
	
	
	

}); //end doc ready

//=============================================================================	
//functions from the stats page to feed the spreads window


  /**
	*	Update chart dynamically on call
	*
	*	@param: formID - the ID of the form that send the message
	*	@param: containerID - the ID of container where the flash div resides
	*	@param: flashdivID - the ID of the target layer where to load flash
	*	@param: paramsobj = a singleton object with properties that might be hard
	*						to parse individually due to different fields;
	*
	*/

	function updateFlashChart(formID,containerID,flashdivID,paramsobj){
		var myformID = formID;
		var myParams = paramsobj;
		var myContainerID = containerID;
		var charttype = myParams.charttype;
		var timeframe = myParams.timeframe;
		var numvalues = myParams.numvalues;
		var myprovider = myParams.myprovider;
		var parameter = myParams.parameter;
		var mydivID = flashdivID;
		var myWidth = myParams.width;
		var myHeight = myParams.height;

	
	//variables defined by CASE statements
	
		var myFeedName;
		var settingFileName;
		var charttypefile;

		
		
		
		
		switch(charttype){
			case 'piespread':
				charttypefile = 'ampie.swf';
				settingFileName = '3d-piechart.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
			
			case 'piecount':
				charttypefile = 'ampie.swf';
				settingFileName = '3d-piechart.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
			
			case 'donutcount':
				charttypefile = 'ampie.swf';
				settingFileName = '3d-donut-chart.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
			
			case 'donutspread':
				charttypefile = 'ampie.swf';
				settingFileName = '3d-donut-chart.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';	
			break;
			
			case 'radarspreads':
				charttypefile = 'amradar.swf';
				settingFileName = 'radar-chart.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
			
			case 'radarcount':
				charttypefile = 'amradar.swf';
				settingFileName = 'radar-chart_custom.php?params='+parameter+'&amp;numvals='+numvalues;
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
			
			case 'columnspreads':
				charttypefile = 'amcolumn.swf';
				settingFileName = '3d-column.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
			
			case 'columncount':
				charttypefile = 'amcolumn.swf';
				settingFileName = '3d_column_custom.php?numvals='+numvalues;
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
			
			case 'xy':
			break;
			
			case 'line':
				charttypefile = 'amline.swf';
				settingFileName = 'stats_settings.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
		
			case 'linestack':
			break;
		
			case 'stock':
			break;
			
			//cases for volatility and execution times
			
			case 'volatility':
				charttypefile = 'amcolumn.swf';
				settingFileName = 'volatility_chart_settings.php?params='+parameter;
				myFeedName = 'spread_get_day_hours_volatility.php';
			break;
			
			case 'belowsec':
				charttypefile = 'amcolumn.swf';
				settingFileName = 'exectime_chart_settings.php';
				myFeedName = 'spread_chart_time_execution_average_range.php';
			break;
			
			case 'exectimes':
				charttypefile = 'amcolumn.swf';
				settingFileName = 'exec-column.xml';
				myFeedName = 'spread_avg_execution_times.php';
			break;
			
			//the gauge
			case 'tradegauge':
				charttypefile = 'gauge_filler.swf';
				settingFileName = '';
				myFeedName = 'stats_trades_below_sec_feed.php';
			break;
		
			//the gauge new
			case 'tradegaugenew':
				charttypefile = 'gauge_filler.swf';
				settingFileName = '';
				myFeedName = 'stats_trades_below_sec_feed_multiple.php';
			break;

			//for main: multiple spreads chart

			case 'multipleline':
				charttypefile = 'amline.swf';
				settingFileName = 'comparison_settings.php?param='+parameter;
				myFeedName = 'ajax_spread_multiple_chart_stats_render.php';
			break;			
			
			
			
			//for main: dual spread comparison
			
			case 'showaverage':
				charttypefile = 'two_spreads_compare.swf';
				settingFileName = '';
				myFeedName = 'ajax_spread_multiple_chart_stats_render.php';
			break;		
			
			
			
			default: //donutspread
				charttypefile = 'ampie.swf';
				settingFileName = '3d-donut-chart.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
		} //end charttype

	
	//the flash generating script
	

	$('#'+myContainerID).html('');
	//reset the flashlayer
	$('<div id="'+mydivID+'"><'+'/div>').appendTo('#'+myContainerID);


	var flashvars = {	path: '../feeds/amline/', 	
						data_file: encodeURIComponent('../feeds/'+myFeedName+'?tf='+timeframe+'&provider='+myprovider+'&numvals='+numvalues+'&chart='+charttype+'&params='+parameter),		settings_file: encodeURIComponent('../feeds/amline/'+settingFileName)};		
	var params = {allowscriptaccess:'always',allowfullscreen:true,wmode:'opaque'};		
	var attributes = {id:mydivID};		
	swfobject.embedSWF('../feeds/amline/'+charttypefile, mydivID , myWidth, myHeight, '9.0.0', 'expressInstall.swf',flashvars,params,attributes);

	return false; //to prevent the button to launch a submit procedure
	} //end updateChart


