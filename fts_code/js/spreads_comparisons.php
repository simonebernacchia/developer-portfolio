<?php 

//-----------------------------------------------------------------------------
// parameters for spreads_comparisons

	date_default_timezone_set('UTC');	  


$chosenMainParamList = 'ecn2|alpariuk|EUR/USD';

	$chosentf = 'ticks';

	$providers = array(	
	
							array(	'name'=>'Statistics Main',
									'feedName'=>'main',
									'provider'=>'currenex',
									'providerData'=>'fxdd,currenex,cnxa',
									),
							
	
							array(	'name'=>'CNXa',
									'feedName'=>'cnxa',
									'provider'=>'currenex',
									'providerData'=>'fxdd,currenex,cnxa',
									),
	
							array(	'name'=>'CNXb',
									'feedName'=>'cnxb',
									'provider'=>'alparius',
									'providerData'=>'alparius,cnxb',
									),
									
							array(	'name'=>'CNXc',
									'feedName'=>'cnxc',
									'provider'=>'alpariuk',
									'providerData'=>'alpariuk,cnxc',
									),
									
							array(	'name'=>'CNXd',
									'feedName'=>'cnxd',
									'provider'=>'fcstone',
									'providerData'=>'fcstone,cnxd',
									),
									
							array(	'name'=>'ECN<sup>2</sup>',
									'feedName'=>'ecn2',
									'provider'=>'ecn2',
									'providerData'=>'ecn2',
									),


							array(	'name'=>'INT-A',
									'feedName'=>'inta',
									'provider'=>'integral',
									'providerData'=>'integral,inta',
									),
									
							array(	'name'=>'LMax',
									'feedName'=>'lmax',
									'provider'=>'lmax',
									'providerData'=>'lmax',
									),
	
	);			
	
	
	$providersMain = array_slice($providers,1);

	$symbols = array("USD/JPY", "EUR/USD", "GBP/USD", "USD/CHF", "USD/CAD", "AUD/USD", "EUR/GBP", "EUR/JPY", 
									 "GBP/JPY", "EUR/CHF", /*"USD/MXN",*/ "CHF/JPY", "GBP/CHF", "EUR/AUD", "EUR/CAD", "AUD/CAD", 
									 "AUD/JPY", "NZD/USD", "AUD/NZD", "CAD/JPY" );

	$timeframes = array('ticks','hours','days');					


			//unpack and feed the selected params
					$chosenParamArr = explode('|',$chosenMainParamList);
					$chosenprov1 = $chosenParamArr[0];
					$chosenprov2 = $chosenParamArr[1];
					$chosensymbol9 = $chosenParamArr[2];	

?> 


 /**
    * Spread Comparisons live table script
	* Using datas from the web service retrieve and display spreads 
	* for a given list of currency pairs
	*
	* made by Bernacchia Simone 
	*
	* based on old frame and quotes scripts
	*

	*
	*
	*
	* @Date:		08-25-2011 16:16:00 -0800 (creation)
	* @Author:		Simone Bernacchia < simonebernacchia@gmail.com >
	*
	*/



	
$(document).ready(function(){
	
//console.log('spread_comparisons.php initiated');
		
//patch to ajax for jQuery 1.4.3 - from atomicObject.com

var myAjax = function(settings) {
  $.ajax($.extend({}, settings, {
    complete: function (xhr, status) {
      if (settings.complete) {
        settings.complete(xhr, status);
      }

      // Avoid IE memory leak
      xhr.onreadystatechange = null;
      xhr.abort = null;
    }
  }));
};
	

<?php $now = time() - 28800; ?>
	
	
	
	var updateRate = 5000; //every 5 seconds web service get called



	

	var weekdayUTC = <?php print date('w',$now); ?>; //taken from php date
	var hourUTC = <?php print date('G',$now); ?>; //taken from php date

	
	
	var isWeekend;
	
	

	
	
//=============================================================================	
//make the spread work only on monday to friday 

if((weekdayUTC >=5 && hourUTC >=14 || weekdayUTC >=6 || weekdayUTC ==0  && hourUTC <17) ){

		isWeekend = true;

	} else {
		updateSpreadsTable();
	

		spreadRefresher = setInterval(updateSpreadsTable,updateRate);
	
		isWeekend = false;
	}

//functions to handle the spreads comparison; those will work only on weekends

if(isWeekend){

	createSpreadsComparisonDiv();
	//spreads comparison
	updateFlashChart('#averageDisplayform','averageDisplaycontainer','averageDisplayChart',{charttype:'showaverage',timeframe:'<?php print $chosentf; ?>',numvalues:'100',myprovider:'',parameter:'<?php print $chosenMainParamList; ?>', width:470,height:100});/**/	
	
	$('#averageDisplayform button').click(function(){
	 //retrieve the parameters to pass at the function up
		var charttype = $('#averageDisplayform  input[name="charttype"]').val();
		var timeframe = $('#averageDisplayform select[name="timeframe"]').val();
		var numvalues = $('#averageDisplayform select[name="numvals"]').val();
		var provider1 = $('#averageDisplayform  select[name="provider"]').val();
		var provider2 = $('#averageDisplayform  select[name="provider2"]').val();
		var parameter = $('#averageDisplayform  select[name="symbol"]').val();
		
		var fullParam = provider1+'|'+provider2+'|'+parameter;
		
		var chartWidth = 470;
		var chartHeight = 100;
	 //create a singleton object to pass parameters
	 var myparams = { charttype: charttype, timeframe: timeframe, numvalues: numvalues, myprovider:'' , parameter: fullParam, width: chartWidth, height: chartHeight };
	 

	 
	 updateFlashChart('#averageDisplayform','averageDisplaycontainer','averageDisplay',myparams);

	 
	 return false;
	});

} //end if
	
//=============================================================================	
	/**
	 * reload datas from the Ajax Spread feeder and display it on the homepage
	 *
	 */
	
	function updateSpreadsTable(){
	

		myAjax({
					url:'feeds/ajax_home_spread_comparison_feeder.php',
					data:'',
					success:function(html){
							$('#comparisonTable').html('');

							myhtml = html;
							$('#loadingspreads').css('background','none');
							$('#comparisonTable').html(myhtml);
	
						//inject table on the layout	
					}
		}); //end ajax

	
	} //end function
	
	
	
	
	
	

}); //end doc ready

//=============================================================================	
//functions from the stats page to feed the spreads window


  /**
	*	Update chart dynamically on call
	*
	*	@param: formID - the ID of the form that send the message
	*	@param: containerID - the ID of container where the flash div resides
	*	@param: flashdivID - the ID of the target layer where to load flash
	*	@param: paramsobj = a singleton object with properties that might be hard
	*						to parse individually due to different fields;
	*
	*/

	function updateFlashChart(formID,containerID,flashdivID,paramsobj){
		var myformID = formID;
		var myParams = paramsobj;
		var myContainerID = containerID;
		var charttype = myParams.charttype;
		var timeframe = myParams.timeframe;
		var numvalues = myParams.numvalues;
		var myprovider = myParams.myprovider;
		var parameter = myParams.parameter;
		var mydivID = flashdivID;
		var myWidth = myParams.width;
		var myHeight = myParams.height;

	
	//variables defined by CASE statements
	
		var myFeedName;
		var settingFileName;
		var charttypefile;
		

		
		
		
		
		switch(charttype){
			case 'piespread':
				charttypefile = 'ampie.swf';
				settingFileName = '3d-piechart.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
			
			case 'piecount':
				charttypefile = 'ampie.swf';
				settingFileName = '3d-piechart.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
			
			case 'donutcount':
				charttypefile = 'ampie.swf';
				settingFileName = '3d-donut-chart.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
			
			case 'donutspread':
				charttypefile = 'ampie.swf';
				settingFileName = '3d-donut-chart.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';	
			break;
			
			case 'radarspreads':
				charttypefile = 'amradar.swf';
				settingFileName = 'radar-chart.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
			
			case 'radarcount':
				charttypefile = 'amradar.swf';
				settingFileName = 'radar-chart_custom.php?params='+parameter+'&amp;numvals='+numvalues;
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
			
			case 'columnspreads':
				charttypefile = 'amcolumn.swf';
				settingFileName = '3d-column.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
			
			case 'columncount':
				charttypefile = 'amcolumn.swf';
				settingFileName = '3d_column_custom.php?numvals='+numvalues;
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
			
			case 'xy':
			break;
			
			case 'line':
				charttypefile = 'amline.swf';
				settingFileName = 'stats_settings.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
		
			case 'linestack':
			break;
		
			case 'stock':
			break;
			
			//cases for volatility and execution times
			
			case 'volatility':
				charttypefile = 'amcolumn.swf';
				settingFileName = 'volatility_chart_settings.php?params='+parameter;
				myFeedName = 'spread_get_day_hours_volatility.php';
			break;
			
			case 'belowsec':
				charttypefile = 'amcolumn.swf';
				settingFileName = 'exectime_chart_settings.php';
				myFeedName = 'spread_chart_time_execution_average_range.php';
			break;
			
			case 'exectimes':
				charttypefile = 'amcolumn.swf';
				settingFileName = 'exec-column.xml';
				myFeedName = 'spread_avg_execution_times.php';
			break;
			
			//the gauge
			case 'tradegauge':
				charttypefile = 'gauge_filler.swf';
				settingFileName = '';
				myFeedName = 'stats_trades_below_sec_feed.php';
			break;
		
			//the gauge new
			case 'tradegaugenew':
				charttypefile = 'gauge_filler.swf';
				settingFileName = '';
				myFeedName = 'stats_trades_below_sec_feed_multiple.php';
			break;

			//for main: multiple spreads chart

			case 'multipleline':
				charttypefile = 'amline.swf';
				settingFileName = 'comparison_settings.php?param='+parameter;
				myFeedName = 'ajax_spread_multiple_chart_stats_render.php';
			break;			
			
			
			
			//for main: dual spread comparison
			
			case 'showaverage':
				charttypefile = 'two_spreads_compare.swf';
				settingFileName = '';
				myFeedName = 'ajax_spread_multiple_chart_stats_render.php';
			break;		
			
			
			
			default: //donutspread
				charttypefile = 'ampie.swf';
				settingFileName = '3d-donut-chart.xml';
				myFeedName = 'spread_stat_pages_chart_feeders.php';
			break;
		} //end charttype

	
	//the flash generating script
	

	$('#'+myContainerID).html('');
	//reset the flashlayer
	$('<div id="'+mydivID+'"><'+'/div>').appendTo('#'+myContainerID);


	var flashvars = {	path: '../feeds/amline/', 	
						data_file: encodeURIComponent('feeds/'+myFeedName+'?tf='+timeframe+'&provider='+myprovider+'&numvals='+numvalues+'&chart='+charttype+'&params='+parameter),		settings_file: encodeURIComponent('feeds/amline/'+settingFileName)};		
	var params = {allowscriptaccess:'always',allowfullscreen:true,wmode:'transparent'};		
	var attributes = {id:mydivID};		
	swfobject.embedSWF('feeds/amline/'+charttypefile, mydivID , myWidth, myHeight, '9.0.0', 'expressInstall.swf',flashvars,params,attributes);

	return false; //to prevent the button to launch a submit procedure
	} //end updateChart
	
	
//generate the spreads comparison form on the space for the spreads if weekend	

function createSpreadsComparisonDiv(){
		var comparisonOutputString ="<h3 style=\"padding-top:10px;\">Compare Our Spreads</h3>"
							+		"<p style=\"padding:0; line-height:16px;\">Chart displays the overall weighted average Execution Time for this provider. Calculation starting Jan, 1st 2011. </p>"
							+		"<div class=\"averageDisplay\">"
							+		"<div id=\"averageDisplaycontainer\">"
							+		"<div id=\"averageDisplayChart\"></div>"
							+		"</div><!-- end chartDataContainer5-->"
							+		"<div class=\"averageDisplayformcontrols\" style=\"position:relative;\">"
							+		"<form id=\"averageDisplayform\" action=\"\">"
							+		"<fieldset class=\"noborder\">"
							+		"<table cellspacing=\"0\" cellpadding=\"0\" >"
							+		"<tr height=\"1\">"
							+		"<td valign=\"top\" style=\"line-height:0px;\" height=\"1\"><img src=\"../images/pixelino.gif\" width=\"100\" height=\"1\" alt=\"\"/></td>"
							+		"<td valign=\"top\" style=\"line-height:0px;\" height=\"1\"><img src=\"../images/pixelino.gif\" width=\"100\" height=\"1\" alt=\"\"/></td>"
							+		"<td valign=\"top\" style=\"line-height:0px;\" height=\"1\"><img src=\"../images/pixelino.gif\" width=\"100\" height=\"1\" alt=\"\"/></td>"
							+		"</tr>"
							+		"<tr>"
							+		"<td align=\"center\">"
							+		"<select name=\"provider\" class=\"width76\">"
							+		"<?php 
									foreach($providersMain as $j=>$value){
									$isselected4 = ($providersMain[$j]['provider']== $chosenprov1) ? 'selected=\"selected\"' : '';
													print'<option value=\"'.$providersMain[$j]['provider'].'\" '.$isselected4.' >'.$providersMain[$j]['name'].'</option>';
									} //end foreach
									?>"
							+		"</select></td>"
							+		"<td align=\"center\">"
							+		"<select name=\"provider2\" class=\"width76\">"
							+		"<?php 
									foreach($providersMain as $j=>$value){
									$isselected5 = ($providersMain[$j]['provider']== $chosenprov2) ? 'selected=\"selected\"' : '';
													print'<option value=\"'.$providersMain[$j]['provider'].'\" '.$isselected5.' >'.$providersMain[$j]['name'].'</option>';
									} //end foreach
									?>"
							+		"</select></td>"
							+		"<td align=\"center\">"
							+		"<select name=\"timeframe\" class=\"width76\">"
							+		"<?php 
										foreach($timeframes as $j=>$value){
										$isselected3 = ($timeframes[$j]== $chosentf) ? 'selected=\"selected\"' : '';
											print'<option value=\"'.$timeframes[$j].'\" '.$isselected3.' >'.$timeframes[$j].'</option>';
										}//end foreach
										?>"
							+		"</select>"
							+		"</td>"
							+		"</tr>"
							+		"<tr>"
							+		"<td align=\"center\">"
							+		"<label >Feed 1:</label>"
							+		"	</td>"
							+		"	<td align=\"center\">"
							+		"	<label >Feed 2:</label>"
							+		"	</td>"
							+		"	<td align=\"center\">"
							+		"	<label >Period:</label>"
							+		"	</td>"
							+		"</tr>"
							+		"<tr>"		
							+		"<td align=\"center\">"
							+		"<select name=\"numvals\" class=\"width76\">"
							+		"<option value=\"25\" >25</option>"
							+		"<option value=\"50\">50</option>"
							+		"<option value=\"75\">75</option>"
							+		"<option value=\"100\" selected=\"selected\">100</option>"
							+		"<option value=\"250\" >250</option>"
							+		"<option value=\"500\" >500</option>"													
							+		"</select>"
							+		"</td>"
							+		"<td align=\"center\">"
							+		"<select name=\"symbol\" class=\"width76\">"
							+		"			<?php 
												foreach($symbols as $j=>$value){
												$isselected9 = ($symbols[$j]== $chosensymbol9) ? 'selected=\"selected\"' : '';
												print'<option value=\"'.$symbols[$j].'\" '.$isselected9.'>'.$symbols[$j].'</option>';
												}//end foreach
												?>"
							+		"</select>"
							+		"<input type=\"hidden\" name=\"charttype\" value=\"showaverage\"/>"
							+		"</td>"
							+		"</tr>"
							+		"<tr>"
							+		"<td align=\"center\">"
							+		"<label >N.Values:</label>"
							+		"</td>"
							+		"<td align=\"center\">"
							+		"<label >Symbol:</label>"
							+		"</td>"
							+		"<td>&nbsp;</td>"
							+		"</tr></table>"
							+		"<button id=\"spread_average\" value=\"Refresh\" style=\"cursor:pointer;\">&nbsp;</button>"
							+		"</fieldset></form>"
							+		"</div><!-- end chart form controls -->"
							+		"</div><!-- end chartspace -->"
							+		"<div style=\"margin-top:16px;\"><span style=\"font-style:italic; line-height:18px;\">Average spreads for informational purpose only.<br/>Hourly and Daily spread value calculated as a weighted average.</span></div>";
							

	$('#comparisonTable').html(comparisonOutputString);
	
	Cufon.replace('h3', { fontFamily: 'DIN' });						
	$('#comparisonTable').css('top','10px');
	$('#comparisonTable').css('left','20px');
	$('#loadingspreads').css('background','none');	
	} // end function

