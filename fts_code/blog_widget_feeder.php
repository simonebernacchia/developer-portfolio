<?php
   /* Blog Widget JSONP encoder starting from RSS feeds
	* made by Bernacchia Simone
	* Aug 19,2010

	*
	* @Package:		Widgets
	* @Date:		11-17-2010 17:09:00 -0800
	* @Author:		Simone Bernacchia < sbernacchia@gmail.com >
	*
	*/

//parse RSS blogs (blogtitle, article ) from a list

include('../includes/functions_simone.php');

$blogaddresses = array("http://blogs.broker.com/autotrade/",
"http://blogs.broker.com/brokers/",
"http://blogs.broker.com/management/",
);

$feedaddresses = array("feed/",
"feed/",
"feed/");	

$bloggerspics = array('http://www.broker.com/widgets/bloggers/salvatore.jpg','http://www.broker.com/widgets/bloggers/alexander.jpg',
'http://www.broker.com/widgets/bloggers/giuseppe_3.jpg',
);


//==============================================================
//blacklist check			
			
	include('../includes/domainblacklist.php');

$allowed = 1;
	
foreach($blacklistDomains as $j=>$value){
			
	if(stristr($_SERVER['HTTP_REFERER'] , $blacklistDomains[$j] )!= false ) {			
		$allowed = 0;
		$json.='/*bingo*/ ';
	} //end if stristr

} //end foreach
//==============================================================
			
			
if($allowed == 1){



//prepare all the part for json
	$json = "";
	
	//start the json render
	
	$json.='/*host: '.$_SERVER['HTTP_HOST'].' - http_referrer:'.$_SERVER['HTTP_REFERER'].' - allowed: '.$allowed.'*/ ';
	

$json.='jsonblogs({"blogs":';

//start data part
$json .= '{ "data":[';





foreach ($blogaddresses as $i => $value) {

	$doc = new DOMDocument();
	$doc->load($blogaddresses[$i].$feedaddresses[$i]);
	$arrFeeds = array();

	$sectitle = $doc->getElementsByTagName('title')->item(0)->nodeValue;
	$secsubtitle = $doc->getElementsByTagName('description')->item(0)->nodeValue;

	foreach ($doc->getElementsByTagName('item') as $node) {
		$itemRSS = array ( 
			'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
			'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
			'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
			'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,

			'cont' => $node->getElementsByTagName('content')->item(0)->nodeValue,
			
			);
		array_push($arrFeeds, $itemRSS);
	}
	
//var_dump($arrFeeds);
	

//process sectitle

$find[] = '“'; // left side double smart quote
$find[] = '”'; // right side double smart quote
$find[] = '‘'; // left side single smart quote
$find[] = '’'; // right side single smart quote
$find[] = '…'; // elipsis
$find[] = '—'; // em dash
$find[] = '–'; // en dash
$find[] = '&#38;'; // ampersand
$find[] = '&#8217;'; // right single quote
$find[] = '&#194;'; // semicolon

$replace[] = '"';
$replace[] = '"';
$replace[] = "'";
$replace[] = "'";
$replace[] = "...";
$replace[] = "-";
$replace[] = "-";
$replace[] = "&";
$replace[] = "'";
$replace[] = ""; 

$sectitle1 = str_replace($find, $replace, $sectitle);



$content0 = replaceHighBitCharacters($arrFeeds[0]['desc']);
$content2 = str_replace($find, $replace,$content0);



	
	$json.="{";
	$json.='"blogname": "'.$sectitle1.'",';
	$json.='"blogpic": "'.$bloggerspics[$i].'",';
	$json.='"blogaddress": "'.$blogaddresses[$i].'",'; 
	$json.='"blogpermalink": "'.$arrFeeds[0]['link'].'",';
	$json.='"blogpubdate": "'.$arrFeeds[0]['date'].'",';
	$json.='"blogtitle": "'.htmlentities($arrFeeds[0]['title']).'",';
	$json.='"blogcontent": "'.fixstr(substr(htmlentities($content2),0,100)).'...'.'"';

} //end for

$json = substr($json,0,-1); //remove last comma
	
$json.="]}"; //end data part	
$json.="});";

echo($json);	

} //end if allowed

function fixstr($str){

$str=strtr ($str,chr(13),'-'); // replace carriage return with dash
$str=strtr ($str,chr(10),chr(32)); // replace line feed with space
return $str; // take it back home
}


	
?>