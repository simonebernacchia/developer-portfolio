<?php

/**
 * Form component for SA-form-engine
 *
 * @package:	CC-Common
 * @subpackage: sa-form-engine
 * @author:		Simone Bernacchia <simonebernacchia@gmail.com>
 * @date:		2013-10-10 13:52:00 PM PDT
 *
 */



//all field definition is fed by config.php
?>

<h3 id="form_headline"><?php if($form_headline_status == 1) { echo $form_headline; } ?></h3>

            <input type="hidden" name="Station_Id" value="<?php print $_SERVER['STATION_ID'];?>"/>

<?php if($form_text_status == 1) { echo '<p>'.$form_text.'</p>'; } ?>
            
             
            <?php if($written_errors_notify	 == 1){ ?>
			<div id="errorsTextList">
			

			<ul class="errorList custom-alert-error-pill">			
	                    <?php
					if($_GET['err']){
					

                        
                        for($i=0; $i<count($errorListArr); $i++) {

                            echo "<li>Please complete the " . $errorListArr[$i] . " field.</li>";
                            } //end for
                        
					} //end if
                        
                    ?>
 				</ul>
			

			</div>
		<?php } //end if
		
//=============================================================================
// FORM FIELD CREATION
//=============================================================================
		
			foreach($required_fields_with_spaces as $i => $value) {
			 
			 
				switch($required_fields_with_spaces[$i]['field_type']){
					case 'text':
					
//=============================================================================
// TEXT FIELD
//=============================================================================					 
			 

				$textfieldHtml5Type = '';
				
				//this part to make email work with spanish names
				
				if($required_fields[$i] == 'Correo_electr_oacute_nico' || $required_fields[$i] == 'Direcci_oacute_n_de_correo_electr_oacute_nico' ){
					
					$textfieldHtml5Type = 'type="email"';
				
					$parsedFieldIdName = 'E-Mail';
				
				
				} else {
				
					$parsedFieldIdName = $required_fields[$i];
					
				
				if($required_fields_with_spaces[$i]['field_name'] == "E-Mail"){
				
				$textfieldHtml5Type = 'type="email"';
				
				} else {
				
				$textfieldHtml5Type = 'type="text"';
				
				} //end if

				if($required_fields_with_spaces[$i]['field_name'] == "Zip Code"){
				
				$textfieldHtml5Type = 'type="email"';
				
				} else {
				
				$textfieldHtml5Type = 'type="text"';
				
				} //end if
				
				} //end if
				
			 ?>
				 <div class="control-group" id="<?php echo $required_fields[$i]; ?>_container">
                 <label class="control-label"><?php echo $field_labels[$i]; ?></label>
                 
                 <div class="controls">

                 <input id="<?php echo $parsedFieldIdName; ?>" name="<?php echo $parsedFieldIdName; ?>" <?php /* type="text"*/ print $textfieldHtml5Type; ?> onFocus="if (this.value=='<?php echo $required_fields_with_spaces[$i]['field_default_value']; ?>') this.value = ''" onBlur="if (this.value=='') this.value = '<?php echo $required_fields_with_spaces[$i]['field_default_value']; ?>'" value="<?php if($oldvalues){print $oldvalues[$required_fields[$i]];} else {echo $required_fields_with_spaces[$i]['field_default_value'];} ?>" <?php ($required_fields[$i] =='Zip_Code') ? print 'size="5" maxlength="5"' : print ''; ?>/>
                 </div>
                 
                 </div>
                 
                 <div class="clear"></div>
			 
					
             <?php
//=============================================================================				 
					break;
					
					case 'textarea':
					
//=============================================================================
// TEXTAREA
//=============================================================================						
					

					?>
                    
                    	<div class="control-group" id="<?php echo $required_fields[$i]; ?>_container">
                        <label class="control-label"><?php echo $field_labels[$i]; ?></label>
                    	
                        <div class="controls">
                    	<textarea name="<?php echo $required_fields[$i]; ?>" id="<?php echo $required_fields[$i]; ?>"><?php if($oldvalues){print $oldvalues[$required_fields[$i]];} else {echo $required_fields_with_spaces[$i]['field_default_value'];} ?></textarea>
                        </div>
                        
                        <div id="<?php echo $required_fields[$i].'Error'; ?>" class="error <?php if($_GET['err']){(formErrorCheck($errorListArr,$required_fields[$i]) == true) ? print' fieldNo' : print '';}?>">

                        </div>
                         
                        </div>
                        
                        <div class="clear"></div>

                        
                    <?php
//=============================================================================		
					break;
					
					case 'textareacounter':
//=============================================================================
// TEXTAREA with Counter
//=============================================================================						
					

					?>
					<div class="textAreaCounterWrapper">
                    	
                        <div class="control-group" id="<?php echo $required_fields[$i]; ?>_container">
                        <label class="control-label"><?php echo $field_labels[$i]; ?></label>
                    	
                        <div class="controls">
                    	<textarea name="<?php echo $required_fields[$i]; ?>" id="<?php echo $required_fields[$i]; ?>"><?php if($oldvalues){print $oldvalues[$required_fields[$i]];} else {echo $required_fields_with_spaces[$i]['field_default_value'];} ?></textarea>
                        </div>
                        
                        <div id="<?php echo $required_fields[$i].'Error'; ?>" class="error <?php if($_GET['err']){(formErrorCheck($errorListArr,$required_fields[$i]) == true) ? print' fieldNo' : print '';}?>">

                         </div>
                         
                         </div>
                                                 
					</div>
                    
                    <div class="clear"></div>
                        
                    <?php
//=============================================================================		
					break;					
					
					case 'date_of_birth':
//=============================================================================
// DATE OF BIRTH
//=============================================================================						

					
					
					
					

					
					if($_GET['err']){
						echo date_dropdown(0,$oldvalues['Date_of_Birth'],$required_fields[$i],$errorListArr,'en',$field_labels[$i]);
					} else {

						echo date_dropdown(0,$required_fields_with_spaces[$i]['field_default_value'],$required_fields[$i],$errorListArr,'en',$field_labels[$i]);
					
					}
					
							
							//or limit by age requirement
							//echo date_dropdown(18);

					?>
                    
                    	<input type="hidden" value="" name="Date_of_Birth" />
                        
                        </div>
                        <div class="clear"></div>
                            
                        
                    <?php		
//=============================================================================						
					break;

					case 'date_of_birth_es':
//=============================================================================
// DATE OF BIRTH ESPANOL
//=============================================================================						

					
					
					
					

					
					if($_GET['err']){
						echo date_dropdown(0,$oldvalues['Date_of_Birth'],$required_fields[$i],$errorListArr,'es',$field_labels[$i]);
					} else {

						echo date_dropdown(0,$required_fields_with_spaces[$i]['field_default_value'],$required_fields[$i],$errorListArr,'es',$field_labels[$i]);
					
					}
					
							
							//or limit by age requirement
							//echo date_dropdown(18);

					?>
                    
                    	<input type="hidden" value="" name="Date_of_Birth" />
                        
                        </div>
                        <div class="clear"></div>
                            
                        
                    <?php		
//=============================================================================						
					break;
					
					
					case 'phone_number':
					case 'Phone_Number':
//=============================================================================
// PHONE NUMBER
//=============================================================================
					

					?>
                            
                            		<div class="control-group" id="<?php echo $required_fields[$i]; ?>_container" style="margin-bottom: 35px !important;">
                                	
                                    <label class="control-label"><?php echo $field_labels[$i]; ?></label>
                                    
                                    <div class="controls">
                                    
                                    <div id="phone">
                                        <input id="phone1" name="phone1" type="tel" maxlength="3" value="<?php if($oldvalues ){print  $oldvalues['phone1']; } ?>"/>
                                       	<div class="phone-dash">-</div>
                                        <input id="phone2" name="phone2" type="tel" maxlength="3" value="<?php if($oldvalues ){print  $oldvalues['phone2']; } ?>"/>
                                        <div class="phone-dash">-</div>
                                        <input id="phone3" name="phone3" type="tel" maxlength="4" value="<?php if( $oldvalues){print  $oldvalues['phone3']; } ?>"/>
                                    </div>
                                    </div>
                                    
                                        <div id="Phone_NumberError" class="error <?php if($_GET['err']){(formErrorCheck($errorListArr,$required_fields[$i]) == true) ? print' fieldNo' : print '';}?>">

                 						</div>
                                        
                                        
                                    <input type="hidden" value="" name="Phone_Number" />
                                    
                                    </div>
                                    <div class="clear"></div>

					
                    <?php
//=============================================================================						
					break;
					
					case 'phone_number_add':
//=============================================================================
// ADDITIONAL PHONE NUMBER 
//=============================================================================
					

					?>
                            
                            		<div class="control-group" id="<?php echo $required_fields[$i]; ?>_container">
                                	
                                    <label class="control-label"><?php echo $field_labels[$i]; ?></label>
                                    
                                    <div class="controls">
                                    
                                    <div id="<?php echo $required_fields[$i]; ?>" class="phone">
                                        <input class="phone1" id="phone1_<?php echo $required_fields[$i]; ?>" name="phone1_<?php echo $required_fields[$i]; ?>" type="tel" maxlength="3" value="<?php if($oldvalues ){print  $oldvalues['phone1_'.$required_fields[$i]]; } ?>"/>
                                       	<div  class="phone-dash">-</div>
                                        <input class="phone2" id="phone2_<?php echo $required_fields[$i]; ?>" name="phone2_<?php echo $required_fields[$i]; ?>" type="tel" maxlength="3" value="<?php if($oldvalues ){print  $oldvalues['phone2'.$required_fields[$i]]; } ?>"/>
                                        <div class="phone-dash">-</div>
                                        <input class="phone3" id="phone3_<?php echo $required_fields[$i]; ?>" name="phone3_<?php echo $required_fields[$i]; ?>" type="tel" maxlength="4" value="<?php if( $oldvalues){print  $oldvalues['phone3'.$required_fields[$i]]; } ?>"/>
                                    </div>
                                    </div>
                                    
                                        <div id="<?php echo $required_fields[$i].'Error'; ?>" class="error <?php if($_GET['err']){(formErrorCheck($errorListArr,$required_fields[$i]) == true) ? print' fieldNo' : print '';}?>">

                 						</div>
                                        
                                        
                                    <input type="hidden" value="" name="<?php echo $required_fields[$i]; ?>" />
                                    
                                    </div>
                                    <div class="clear"></div>

					
                    <?php
//=============================================================================						
					break;


					
					case 'state':
//=============================================================================
// STATE
//=============================================================================
					

						
						//states array
						$statesSelect = array(	
						array(	'state_abbreviation'	=>'SelectState',	'state'	=>'Select State'),
						array(	'state_abbreviation'	=>'CA',	'state'	=>'California'),
						array(	'state_abbreviation'	=>'AL',	'state'	=>'Alabama'),
						array(	'state_abbreviation'	=>'AK',	'state'	=>'Alaska'),		
						array(	'state_abbreviation'	=>'AR',	'state'	=>'Arkansas'),
						array(	'state_abbreviation'	=>'AZ',	'state'	=>'Arizona'),
						array(	'state_abbreviation'	=>'CO',	'state'	=>'Colorado'),
						array(	'state_abbreviation'	=>'CT',	'state'	=>'Connecticut'),
						array(	'state_abbreviation'	=>'DC',	'state'	=>'Washington DC'),
						array(	'state_abbreviation'	=>'DE',	'state'	=>'Delaware'),
						array(	'state_abbreviation'	=>'FL',	'state'	=>'Florida'),
						array(	'state_abbreviation'	=>'GA',	'state'	=>'Georgia'),
						array(	'state_abbreviation'	=>'HI',	'state'	=>'Hawaii'),
						array(	'state_abbreviation'	=>'ID',	'state'	=>'Idaho'),
						array(	'state_abbreviation'	=>'IL',	'state'	=>'Illinois'),
						array(	'state_abbreviation'	=>'IN',	'state'	=>'Indiana'),
						array(	'state_abbreviation'	=>'IA',	'state'	=>'Iowa'),
						array(	'state_abbreviation'	=>'KS',	'state'	=>'Kansas'),
						array(	'state_abbreviation'	=>'KY',	'state'	=>'Kentucky'),
						array(	'state_abbreviation'	=>'LA',	'state'	=>'Louisiana'),
						array(	'state_abbreviation'	=>'ME',	'state'	=>'Maine'),
						array(	'state_abbreviation'	=>'MD',	'state'	=>'Maryland'),
						array(	'state_abbreviation'	=>'MA',	'state'	=>'Massachusetts'),
						array(	'state_abbreviation'	=>'MI',	'state'	=>'Michigan'),
						array(	'state_abbreviation'	=>'MN',	'state'	=>'Minnesota'),
						array(	'state_abbreviation'	=>'MS',	'state'	=>'Mississippi'),
						array(	'state_abbreviation'	=>'MO',	'state'	=>'Missouri'),
						array(	'state_abbreviation'	=>'MT',	'state'	=>'Montana'),
						array(	'state_abbreviation'	=>'NE',	'state'	=>'Nebraska'),
						array(	'state_abbreviation'	=>'NV',	'state'	=>'Nevada'),
						array(	'state_abbreviation'	=>'NH',	'state'	=>'New Hampshire'),
						array(	'state_abbreviation'	=>'NJ',	'state'	=>'New Jersey'),
						array(	'state_abbreviation'	=>'NM',	'state'	=>'New Mexico'),								
						array(	'state_abbreviation'	=>'NY',	'state'	=>'New York'),								
						array(	'state_abbreviation'	=>'NC',	'state'	=>'North Carolina'),								
						array(	'state_abbreviation'	=>'ND',	'state'	=>'North Dakota'),								
						array(	'state_abbreviation'	=>'OH',	'state'	=>'Ohio'),								
						array(	'state_abbreviation'	=>'OK',	'state'	=>'Oklahoma'),								
						array(	'state_abbreviation'	=>'OR',	'state'	=>'Oregon'),								
						array(	'state_abbreviation'	=>'PA',	'state'	=>'Pennsylvania'),								
						array(	'state_abbreviation'	=>'RI',	'state'	=>'Rhode Island'),								
						array(	'state_abbreviation'	=>'SC',	'state'	=>'South Carolina'),								
						array(	'state_abbreviation'	=>'SD',	'state'	=>'South Dakota'),								
						array(	'state_abbreviation'	=>'TN',	'state'	=>'Tennessee'),								
						array(	'state_abbreviation'	=>'TX',	'state'	=>'Texas'),								
						array(	'state_abbreviation'	=>'UT',	'state'	=>'Utah'),								
						array(	'state_abbreviation'	=>'VT',	'state'	=>'Vermont'),								
						array(	'state_abbreviation'	=>'VA',	'state'	=>'Virginia'),								
						array(	'state_abbreviation'	=>'WA',	'state'	=>'Washington'),								
						array(	'state_abbreviation'	=>'WV',	'state'	=>'West Virginia'),								
						array(	'state_abbreviation'	=>'WI',	'state'	=>'Wisconsin'),								
						array(	'state_abbreviation'	=>'WY',	'state'	=>'Wyoming'),						
						);

					?>
                           	<div class="control-group" id="<?php echo $required_fields[$i]; ?>_container">
                            
                            <label class="control-label">State</label>
                            
                             <div class="controls">
                             
                             <div class="state">
                             <select id="State" name="State">
                            <?php 
                            
                                $optionList = '';
                                foreach($statesSelect as $j=>$value2){
                                    $optionList.='<option value="'.$statesSelect[$j]['state_abbreviation'].'"';
                                    if($oldvalues && $oldvalues['State'] == $statesSelect[$j]['state_abbreviation']){
                                        $optionList.=' selected="selected"';
                                    } //end if
                                    $optionList.='>'.$statesSelect[$j]['state'].'</option>';
                                } //end foreach
                                
                                print $optionList;
                                ?>
                            </select>
                            
                          </div>
                          
                          </div>
                          
                            <div id="StateError" class="error <?php print $required_fields[$i]; ?><?php if($_GET['err']){(formErrorCheck($errorListArr,$required_fields[$i]) == true) ? print' fieldNo' : print 'boh';}?>">
             

                 			</div>
                        
						 	</div>
                            <div class="clear"></div>

                    <?php
					break;
					
					case 'radio':
//=============================================================================
// RADIO BUTTONS
//=============================================================================				

					
					//prepare the values
					
					$radioLabelAndValuesArr = explode('~',$required_fields_with_spaces[$i]['field_default_value']);
					$radioLabel	= $radioLabelAndValuesArr[0];
					$radioValuesArr = explode(chr(13),$radioLabelAndValuesArr[1]);
					
					$radioButtonArr = array();
					
					foreach($radioValuesArr as $k=>$value1){
						$subRadioValuesArr = explode('|',$radioValuesArr[$k]);
						array_push($radioButtonArr, array(	'name'	=>	$subRadioValuesArr[0],
																'value'	=>	$subRadioValuesArr[1],
															));
					
					} //end foreach
					
					
					?>
                    <div id="above_radio" style="width:86%; margin:0 auto 10px;">
					<label class="radio_title"><?php print $radioLabel; ?></label>
                                        
                    </div>
                                        
					<div class="control-group" style="*width:86%;" id="<?php echo $required_fields[$i]; ?>_container">
					<?php
					$buttonHtml = '';
					
					foreach($radioButtonArr as $j=>$value2){
						$buttonHtml.='<div class="radio_controls">'.chr(13);
						$buttonHtml.='';
						$buttonHtml.='<input type="radio"  name="'.$required_fields[$i].'" value="'.$radioButtonArr[$j]['value'].'"';
						
						if($oldvalues && $oldvalues[$required_fields[$i]] == $radioButtonArr[$j]['name']){
							$optionList.=' checked="checked" ';
						} //end if

						$buttonHtml.=' style="width:24px !important;"/>'.chr(13);
						$buttonHtml.='<label class="radio">'.$radioButtonArr[$j]['name'].'</label>'.chr(13);	
						$buttonHtml.='</div>'.chr(13).'<div class="clear"></div>'.chr(13);
					
					} //end foreach
					
					print $buttonHtml;
					
					?>
                    
                    <div id="below_radio" style="max-width:417px;">
                    <div id="<?php print $required_fields[$i]; ?>Error" class="error radioButtonError <?php if($_GET['err']){(formErrorCheck($errorListArr,$required_fields[$i]) == true) ? print' fieldNo' : print '';}?> " ></div>  
                    </div>  
					
					</div>
                    
                    <div class="clear"></div>
					
				<?php	
				//	} //end if				 
//=============================================================================				
					break;

					case 'radiopic':
//=============================================================================
// RADIO BUTTONS WITH PICTURES
//=============================================================================				

					
					//prepare the values
					
					$radioLabelAndValuesArr = explode('~',$required_fields_with_spaces[$i]['field_default_value']);
					$radioLabel	= $radioLabelAndValuesArr[0];
					$radioValuesArr = explode(chr(13),$radioLabelAndValuesArr[1]);
					
					$radioButtonArr = array();
					
					foreach($radioValuesArr as $k=>$value1){
						$subRadioValuesArr = explode('|',$radioValuesArr[$k]);
						array_push($radioButtonArr, array(	'name'	=>	$subRadioValuesArr[0],
																'value'	=>	$subRadioValuesArr[1],
																'pic'	=>	$subRadioValuesArr[2],
															));
					
					} //end foreach
					
					
					?>
                    <div id="above_radio" style="width:86%; margin:0 auto 10px;">
					<label class="radio_title"><?php print $radioLabel; ?></label>
                                        
                    </div>
                                        
					<div class="control-group" style="*width:86%;" id="<?php echo $required_fields[$i]; ?>_container">
					<?php
					$buttonHtml = '';
					
					foreach($radioButtonArr as $j=>$value2){
						$buttonHtml.='<div class="radio_controls">'.chr(13);
						$buttonHtml.='<!--<div class=""><img src=""/></div>-->';
						$buttonHtml.='<input type="radio"  name="'.$required_fields[$i].'" value="'.$radioButtonArr[$j]['value'].'"';
						
						if($oldvalues && $oldvalues[$required_fields[$i]] == $radioButtonArr[$j]['name']){
							$optionList.=' checked="checked" ';
						} //end if

						$buttonHtml.=' style="width:24px !important;"/>'.chr(13);
						$buttonHtml.='<label class="radio">'.$radioButtonArr[$j]['name'].'</label>'.chr(13);	
						$buttonHtml.='</div>'.chr(13).'<div class="clear"></div>'.chr(13);
					
					} //end foreach
					
					print $buttonHtml;
					
					?>
                    
                    <div id="below_radio" style="max-width:417px;">
                    <div id="<?php print $required_fields[$i]; ?>Error" class="error radioButtonError <?php if($_GET['err']){(formErrorCheck($errorListArr,$required_fields[$i]) == true) ? print' fieldNo' : print '';}?> " ></div>  
                    </div>  
					
					</div>
                    
                    <div class="clear"></div>
					
				<?php	
				//	} //end if				 
//=============================================================================				
					break;
					
					
					
					case 'checkbox':
//=============================================================================
// CHECKBOX
//=============================================================================	
				$checkboxValArr = explode('|',$required_fields_with_spaces[$i]['field_default_value']);
				?>	
            
            	<div class="control-group" id="<?php echo $required_fields[$i]; ?>_container">
                <div class="controls">
                
                <input type="checkbox" value="<?php print $checkboxValArr[1]; ?>" name="<?php echo $required_fields[$i]; ?>" id="<?php echo $required_fields[$i]; ?>" <?php if($oldvalues && $oldvalues[$required_fields[$i]] == $checkboxValArr[1]){ print'checked="checked"';}?>/>
				<label class="checkbox" for="<?php echo $required_fields[$i]; ?>"  ><?php echo $required_fields_with_spaces[$i]['field_name']; ?></label>
            	</div>
				
                <div id="below_chkbox" style="max-width:417px;">
				<div id="<?php echo $required_fields[$i].'Error'; ?>" class="error <?php if($_GET['err']){(formErrorCheck($errorListArr,$required_fields[$i]) == true) ? print' fieldNo' : print '';}?>" style="float:right !important;"></div>
                </div>
                 
                 </div>
            
            <div class="clear"></div>
            
			
					
				<?php	
//=============================================================================				
					break;
					
					
					
					case 'dropdown':
//=============================================================================
// CUSTOM DROP-DOWN LIST
//=============================================================================						
					
						//prepare the values
					
					$dropDownLabelAndValuesArr = explode('~',$required_fields_with_spaces[$i]['field_default_value']);
					$dropDownLabel	= $dropDownLabelAndValuesArr[0];
					$dropDownValuesArr = explode(chr(13),$dropDownLabelAndValuesArr[1]);
					
					$dropDownArr = array();
					
					foreach($dropDownValuesArr as $k=>$value1){
						$subdropDownValuesArr = explode('|',$dropDownValuesArr[$k]);
						array_push($dropDownArr, array(	'name'	=>	$subdropDownValuesArr[0],
																'value'	=>	$subdropDownValuesArr[1],
															));
					
					} //end foreach
					
					?>		
                    
                    		<div class="control-group" id="<?php echo $required_fields[$i]; ?>_container">
                            
                            <label class="control-label"><?php print $dropDownLabel; ?></label>
                            
                             <div class="controls">
                             <div class="state">
                             <select id="<?php print $required_fields[$i]; ?>" name="<?php print $required_fields[$i]; ?>" class="customDropDown">
                            <?php 
                            
                                $optionList = '';
                                foreach($dropDownArr as $j=>$value2){
                                    $optionList.='<option value="'.$dropDownArr[$j]['value'].'"';
                                    if($oldvalues && $oldvalues[$required_fields[$i]] == $dropDownArr[$j]['value']){
                                        $optionList.=' selected="selected"';
                                    } //end if
                                    $optionList.='>'.$dropDownArr[$j]['name'].'</option>';
                                } //end foreach
                                
                                print $optionList;
                                ?>
                            </select>
                            
                          </div>
                          
                          </div>
                          
                            <div id="<?php echo $required_fields[$i].'Error'; ?>" class="error <?php if($_GET['err']){(formErrorCheck($errorListArr,$required_fields[$i]) == true) ? print' fieldNo' : print '';}?>">

                 			</div>
                            
                          </div>
                          <div class="clear"></div>			
					
					
					
                 <?php 
//=============================================================================				
					break;
					
					case 'file':
//=============================================================================
// FILE REQUESTER
//=============================================================================					
					?>
                    
                    <div class="control-group" id="<?php echo $required_fields[$i]; ?>_container">
                    
					<label class="control-label"><?php echo $field_labels[$i]; ?></label>
                    
                    <div class="controls">
                    
							<input type="file" class="file" name="<?php echo $required_fields[$i]; ?>" id="<?php echo $required_fields[$i]; ?>" style=""/>
                            
                    </div>
                 
                 </div>
                 <div class="clear"></div>

					<?php
//=============================================================================				
					break;
					
					
/*		
*/			
					case 'autozip':
//=============================================================================
// AUTOZIP
//=============================================================================						
					?>
                    
                 <div class="control-group" id="<?php echo $required_fields[$i]; ?>_container">   
                 <label class="control-label"><?php echo $field_labels[$i]; ?></label>
                 
                 <div class="controls">
                 <input id="<?php echo $required_fields[$i]; ?>" name="<?php echo $required_fields[$i]; ?>" type="number" onFocus="if (this.value=='<?php echo $required_fields_with_spaces[$i]['field_default_value']; ?>') this.value = ''" onBlur="if (this.value=='') this.value = '<?php echo $required_fields_with_spaces[$i]['field_default_value']; ?>'" value="<?php if($oldvalues){print $oldvalues[$required_fields[$i]];} else {echo $required_fields_with_spaces[$i]['field_default_value'];} ?>" size="5"  maxlength="5"/>
                 </div>
                 
                 <div id="<?php echo $required_fields[$i].'Error'; ?>" class="error <?php if($_GET['err']){(formErrorCheck($errorListArr,$required_fields[$i]) == true) ? print' fieldNo' : print '';}?>">

                 </div>					
					
					<input type="hidden" name="city_r" id="city_r" value=""/>
					<input type="hidden" name="state_r" id="state_r" value=""/>
                    
                 </div>
                 <div class="clear"></div>
					
					<?php
//=============================================================================				
					break;
					
					case 'title':
//=============================================================================		
// CUSTOM TITLE		
//=============================================================================					

					print '<!-- title generated via form.php -->'.chr(13).chr(13);
							$titleAppend = '';
							
							$titleAppend .='<'.$required_fields_with_spaces[$i]['field_default_value'].' class="form-internal-title">';
							$titleAppend .= $required_fields_with_spaces[$i]['field_name'];
							$titleAppend .='</'.$required_fields_with_spaces[$i]['field_default_value'].'>';

							print $titleAppend.chr(13).chr(13);

					print '<!-- end title generated via form.php -->'.chr(13).chr(13);
					
//=============================================================================				
					break;  
					
				/*	
					case 'hidden':
//=============================================================================		
// HIDDEN FIELD (WIP)		
//=============================================================================	
					
					
					
//=============================================================================				
					break;
				*/	
					


				case 'instagram':
//=============================================================================		
// INSTAGRAM
//=============================================================================	
				
				$instagramLabelArr = explode('|',$required_fields_with_spaces[$i]['field_default_value']);
				
				?>	
                        <div class="control-group" id="<?php echo $required_fields[$i]; ?>_container">
						
<input type="hidden" name="<?php echo $required_fields[$i]; ?>_submission_Type"/>
						
						
                        <label class="control-label"><?php echo $field_labels[$i]; ?></label>
                    	

                        
<div class="controls fileSelectorContainer" >
	<input type="radio"  class="instagramRadioButton" name="<?php echo $required_fields[$i]; ?>_submission_Type" value="handle" style="display: block; float: left; border: medium none; width: 32px; line-height: 10px ! important; margin-bottom: 0pt;"/>
	<label class="control-label instagramRadio" ><?php print $instagramLabelArr[0]; ?></label>
    
    <div style="clear:both;"></div>
	<div id="inputURLContainer" style="display: block; clear: both; padding-top:10px;">
					 <input id="instagramUrl" name="<?php echo $required_fields[$i]; ?>" type="text" onFocus="if (this.value=='<?php echo $instagramLabelArr[2]; ?>') this.value = ''" onBlur="if (this.value=='') this.value = '<?php if($oldvalues){print $oldvalues[$required_fields[$i]];} else {echo $instagramLabelArr[2];} ?>'" value="social handle" />

	</div>
	
</div><!-- end fileSelectorContainer -->

                 <div class="clear"></div>

<div class="controls fileSelectorContainer">

	<input type="radio" class="instagramRadioButton" name="<?php echo $required_fields[$i]; ?>_submission_Type" value="file"  style="border: medium none; width: 32px; line-height: 10px ! important; margin-bottom: 0pt;"/>
	<label class="control-label instagramRadio" ><?php print $instagramLabelArr[1]; ?></label>
	<div id="inputFileContainer">
	

				<input type="file" class="file" name="<?php echo $required_fields[$i]; ?>" id="instagramFile" style="margin-top:10px; border: none; background: none;"/>
	</div><!-- end inputFileContainer --->  







                        <div id="<?php echo $required_fields[$i].'Error'; ?>" class="error <?php if($_GET['err']){(formErrorCheck($errorListArr,$required_fields[$i]) == true) ? print' fieldNo' : print '';}?>"></div>
  
                         </div>				
				<?PHP	
//=============================================================================			
				break;
					
					case 'html':
//=============================================================================		
// HTML INSERT		
//=============================================================================	
				
					print $required_fields_with_spaces[$i]['field_default_value'];
					
					
//=============================================================================						
					break;					
					
//=============================================================================		
// ADDITIONAL FIELDS		
//=============================================================================				

					
					
			} //end switch
			
				
					
					


             
			} //end foreach required_fields_with_spaces
//=============================================================================				
			
			 
			 ?>

            <?php if($opt_in_status == 1) { ?>
                
            <div class="checkbox-container" id="opt-in-checkbox-container">
            
                <input type="checkbox" value="Yes" name="Opt-In" id="checkbox"><label class="field-checkbox" for="checkbox" <?php if($oldvalues && $oldvalues['Opt-In'] == 'Yes'){ print'checked="checked"';}?> /><?php echo $opt_in_message; ?></label>
                        
            </div>
            
            
            <?php } //end if opt_in_enabled ?>
			
			<?php if($rules_checkbox_enabled == 1) { ?>
                
            <div class="checkbox-container" id="rules-checkbox-container">
            
                <input type="checkbox" value="Yes" name="agreeRules" id="rulescheckbox"><label class="field-checkbox" for="rulescheckbox" <?php if($oldvalues && $oldvalues['agreeRules'] == 'Yes'){ print'checked="checked"';}?> /><?php echo $rules_checkbox_message; ?></label>
                 
				 
            </div>
             
            <div class="clear"></div>
            
            <?php } //end if rules_checkbox_enabled ?>
			
            <?php if($station_opt_in_enabled == 1) { ?>
                
            <div class="checkbox-container" id="station-opt-in-checkbox-container">
            
                <input type="checkbox" value="Yes" checked="checked" name="station_Opt-In" id="stationcheckbox"><label class="field-checkbox" for="stationcheckbox" <?php if($oldvalues && $oldvalues['Opt-In'] == 'Yes'){ print'checked="checked"';}?> /><?php if($opt_in_message) { echo $opt_in_message; } ?></label>
                        
            </div>
            
            <?php } //end if station_opt_in_enabled ?>			
            
			
       <!--     <div style="margin:0 auto; width: 260px;"> -->
	   
            <div id="submit-button-container">

			<button id="ccla-submit-button" class="btn btn-primary" value="<?php echo $submit_button_message; ?>"><?php echo $submit_button_message; ?></button>
            </div> 
            
            <div class="clear"></div>

			<div id="contest_rules_link" style="margin-top:0.5em;"><a href="<?php echo $contest_rules_url.'?'. mktime(); ?>" target="_blank"><?php if($contest_rules_message) { echo $contest_rules_message; } ?></a></div>
		
			
<?php	

/**
 * Date Dropdown function
 *
 * @param: year_limit - unused for now
 * @param: default_date - the date to process as DD-MM-YYYY
 * @param: dobFname - field name for error check
 * @param: errorListArr - error list array name for error check
 *
 */
 
	function date_dropdown($year_limit = 0, $default_date,$dobFName,$errorListArr, $language,$field_form_label) {

	
		$defDateCleanup = str_replace(',','',$default_date);
		
		$defDateArr = explode(' ',$defDateCleanup);
		
	
		switch ($language) {
			case 'es':
		$months = array("Mes", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
		$days = 'D&iacute;a';
		$years = 'A&ntilde;o';
			break;
		
			case 'en':
			default:
		$months = array("Month", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		$days = 'Day';
		$years = 'Year';
			break;
		
		} //end switch
	
	
	

		$html_output .= '<div class="control-group" id="'.$dobFName.'_container">       <label class="control-label" for="date_day">'.$field_form_label.'</label>';
		
		/*months*/
		$html_output .= '<div class="controls">           <div id="date"><div class="dob-value"><select name="date_month" id="month_select" >';
			for ($month = 0; $month <= 12; $month++) {
				if($month == 0){
					$html_output .= '               <option';
					if($defDateArr[0] == $months[0]){
						$html_output.=' selected="selected" ';
					} //end if;
					$html_output.='>'.$months[0].'</option>';
				} else {
				
					$html_output .= '               <option value="' . $months[$month] . '"';
					//check default value
					if($defDateArr[0] == $months[$month]){
					$html_output.=' selected="selected" ';
					} //end if
				} //end if
				
				$html_output.='>' . $months[$month] . '</option>';
			}
		$html_output .= '           </select></div>';
		
		/*days*/
		$html_output .= '           <div class="dob-value"><select name="date_day" id="day_select">';
			for ($day = 0; $day <= 31; $day++) {
				if($day == 0){
					$html_output .= '               <option';
					if($defDateArr[1] == $days){
						$html_output.=' selected="selected" ';
					} //end if;
					$html_output.='>'.$days.'</option>';
				} else {
					//if($day < 10) { $day = "0" . $day; }
					$html_output .= '               <option';
						//check default value
					if(strval($defDateArr[1]) == $day){
					$html_output.=' selected="selected" ';
					} //end if
					
					$html_output.='>' . $day . '</option>';
				
				} //end if
			} //end for
		$html_output .= '           </select></div>';
		
		/*years*/
		$html_output .= '           <div class="dob-value"><select name="date_year" id="year_select">';
			for ($year = (date("Y") + 1); $year >= (date("Y") - 100); $year--) {
				if($year == (date("Y") + 1)){
				$html_output .= '               <option';
					if($defDateArr[2] == $years){
						$html_output.=' selected="selected" ';
					} //end if;
					$html_output.='>'.$years.'</option>';
				
				
				} else {
				
				$html_output .= '               <option';
							//check default value
					if(strval($defDateArr[2]) == $year){
					$html_output.=' selected="selected "';
					} //end if
					$html_output.='>' . $year . '</option>';
				} //end if
			} //end for
		$html_output .= '           </select></div></div></div>';


		$html_output .= '   <div id="Date_of_BirthError" class="error ';
		
		 if($_GET['err']){

			if(formErrorCheck($errorListArr,$dobFName) == true){ 
		 
			$html_output.=' fieldNo'; 
		 
			} //end if
		 } //end if
		
$html_output.='"></div>'.chr(13);


	return $html_output;

	
} //end function			
					
?>					