<? 
//Stand-Alone Landing Page Template w/HTML Bounceback Email - Config File 

//=============================================================================
// DIRECTORY SETUP
//=============================================================================

$landing_page_directory 		=		'common'; // directory name where your contest that would follow http://www.station.com/ (for example)
$unique_directory				=		'contest_dir'; // unique directory name where your contest files live
	
	//=============================================================================
	// DO NOT EDIT
	//=============================================================================
		$source_path			=		'http://' . $_SERVER['HTTP_HOST'] . '/'.$landing_page_directory; // path to main file directory
		$source_path_root		=		$source_path.'/'.$unique_directory.'/'; // path to main file directory
	
	
//=============================================================================
// METADATA INFORMATION
//=============================================================================

$page_title 					= 		'Contest Title'; // metadata page title & bounceback subject + "Contest Entry" ex: KSTN: Contest Name Contest Entry
$page_description 				= 		'Sign Up To�Join a�Hands on Banking College Preparedness Seminar'; // metadata page description
$metadata_keywords 				= 		'College Preparedness, College Preparedness, Sign Up To�Join a�Hands on Banking College Preparedness Seminar'; // metadata keywords
$metadata_image 				= 		$source_path_root.'images/200x200.jpg' . "?" . mktime(); // metadata 200x200 image


//=============================================================================
// BASE PATH FOR INCLUDES - DO NOT EDIT, SKIP TO HEADER CONFIGURATION
//=============================================================================

$market_id       				= 		$_SERVER['MARKET_ID'];
$basePath 						= 		'/export/home2/radio/'.$market_id.'/common/'; // los angeles common
$cc_common_base_path			=		'/export/home/cc/contests/group/common/'; // cc-common basepath
$commonPath						= 		$basePath.'common/'; // common/common
$formPath 						= 		'/export/home/cc/contests/group/common/sa-form-engine/';
$centralPath 					= 		'http://'.$_SERVER['HTTP_HOST'].'/go/contests/group/common/sa-landing-page/';

$working_directory 				= 		getcwd();
$csv_path 						= 		$working_directory.'/';
//$csv_path 					= 		'/dev/null/';
$attachment_path 				= 		$working_directory.'/uploads/';

$cc_contest_id					=		strtolower($_SERVER['STATION_ID']).'_'.$unique_directory;

$service_path					=		$source_path_root.'/uploads/'; //for attachments
$canonical_url					=		$source_path_root; // set absolute directory path for SEO purposes

$akamai_server_path				=		'http://a1135.g.akamai.net/f/1135/24935/1h/cchannel.download.akamai.com/24935/43kix/'; // root akamai server path

$databaseEntriesTable  			=   	'static_los_angeles_contest_entry_data'; // table where entries will be inserted and queried from
$databaseZipCode				=   	'zip_code'; // table where zip code information for autozip is retrieved

$station_id 					= 		$_SERVER['STATION_ID']; // returns station id from the server
$debug							=		1; //1=enabled,0=disabled for debugging formscript
$debug_mode						=		0; // 1 to enable, 0 to disable ->TODO

//=============================================================================
// HEADER CONFIGURATION
//=============================================================================

$header_enabled					=		1; // 1 to enable, 0 to disable
$header_image					=		'images/header.jpg'; // header image
$header_link_enabled			=		0;	// 1 to enable, 0 to disable
$header_image_link				=		'http://www.station.com'; // header image link url

//=============================================================================
// LEFT COLUMN CONFIGURATION
//=============================================================================

$headline_status				=		1; // 1 to enable, 0 to disable
$headline						=		'Sign Up To�Join Jojo at a�Hands on Banking College Preparedness Seminar';	//headline above main DL image

// ** REQUIRED - $featured_email_image is required
// if $featured_image is populated, $featured_email_image will be overwritten with that value, 
// otherwise, you will need to provide a separate $featured_email_image if there is no
// $featured_image on the page in the case of a featured video, or if $featured_video_status = 0;

$featured_media_status			=		0;
$featured_media_type			=		'image'; // image|video

$featured_image 				=		'images/main.jpg' . "?" . mktime();	// dynamic lead image + email header source image (560px x 169px)
$featured_image_link			=		'http://www.station.com'; // populate this field to link the featured image, leave blank for the link to be omitted

$featured_video_media_id		=		'';	// video media id from the D.A.M. - .mp4 format required, example: 23505205
$featured_video_autoplay_status	=		0;

$main_text_status				=		1; // 1 to enable, 0 to disable
$main_text						=

<<<EOT

<!-- begin HTML -->

<p>We are inviting you to come hang with him in the studios. Get a jump on preparing for college with savings plans. It's easier than you think we want to get you all the details. Come walk the halls while learning how to take control of your financial future. Plus, you may catch a glimpse of a celebrity or two. Space is limited.</p><p>Sign up now!</p>

<!-- end HTML -->

EOT;

//=============================================================================
// FORM CONFIGURATION
//=============================================================================

$form_status					=		1; // 1 to enable, 0 to disable the form
$form_headline_status			=		0; // 1 to enable, 0 to disable the form headline
$form_headline					= 		'Enter below for your chance to win!';
$form_text_status				=		0; // 1 to enable, 0 to disable the form text
$form_text						=		'Fill out the form to win fabulous prizes.';
$opt_in_status					=		0; // 1 to enable, 0 to disable form opt-in
$opt_in_message 				= 		'Yes, I would like to receive special offers and promotions.';
$contest_rules_status			=		1; // 1 to enable, 0 to disable the contest rules
$contest_rules_message			=		'LEGAL'; // contest rules link text
$contest_rules_url				=		'contestrules.pdf'; // contest rules file name
$submit_button_message			=		'SUBMIT'; // submit button text


//=============================================================================
// THANK YOU PAGE CONFIGURATION
//=============================================================================
$back_to_main_link_enabled		=		1; // 1 to enable, 0 to disable

	//=============================================================================
	// NOT ENABLED
	//=============================================================================
	//$thank_you_message				=		'Thank you for entering. <br /> <br />Your information has been received.';
	

//=============================================================================
// RIGHT COLUMN CONFIGURATION
//=============================================================================

$social_bar_status					=		1; // 1 to enable, 0 to disable

$ad_unit_300x250_status				=		1; // 1 to enable, 0 to disable

$right_column_video_status			= 		0; // 1 to enable, 0 to disable
$right_column_video_module_title	=		'Video'; // leave blank to omit, enter title to have it display above video module
$right_column_video_media_id		=		''; // video media id from the D.A.M. - .mp4 format required, example: 23505205
$right_column_video_autoplay_status	=		0; // 1 to enable, 0 to disable

$facebook_like_box_status			= 		0; // 1 to enable, 0 to disable
$facebook_like_box_module_title		=		'Facebook'; // 1 to enable, 0 to disable
$facebook_like_box_url				=		'https://www.facebook.com/Facebook'; // facebook profile page which will be displayed

$twitter_widget_status				= 		0; // 1 to enable, 0 to disable
$twitter_widget_module_title		=		'Twitter';
$twitter_widget_code				=		'<a class="twitter-timeline" href="https://twitter.com/1027kiisfm" data-widget-id="352470983061422080">Tweets by @onecoconut</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>'; // For help setting this up, visit: http://www.kiisfm.com/common/landing-page/docs/#right_twitter_widget

$custom_module_status				=		0; // 1 to enable, 0 to disable
$custom_module_title				=		'Custom Module';
$custom_module_code					=		

<<<EOT

<!-- begin HTML -->

<p>This is my custom HTML!</p>

<!-- end HTML -->

EOT;


//=============================================================================
//=============================================================================
//=============================================================================
//
// MOST CONTESTS WILL NOT NEED EDITS BEYOND THIS POINT
//
//=============================================================================
//=============================================================================
//=============================================================================


//=============================================================================
// ADVANCED HTML CONFIGURATION 
//=============================================================================

$ajax_enabled					=		0; // 1 to enable, 0 to disable
$written_errors_notify			=		0; // 1 to enable, 0 to disable
$pin_it_image_url				=		'images/pin-it.jpg?' . mktime(); //change this is you wan't the Pin It image to be different from the main DL image

// FOR FORMSCRIPT
$campaign_name 					= 		$page_title; 	 //change this if you want the title of the email to be different than the page title
														 // this is the email subject
														 // [space] Contest Entry will be added after this
														 // e.g. Loan Mart Cash Giveaway will  automatically appear as
														 // Loan Mart Cash Giveaway Contest Entry in the email bounceback subject	 
														 
$csv_file_on_server_override	=		1;
$csv_file_on_server_storage		=		1;
														 
$csv_name 						= 		$unique_directory . '.csv';		// change this to customize the name of the csv that is generated when users successfully submit the for

$file_types_to_allow 			= 		array('.gif','.GIF','.jpg','.JPG','.jpeg','.JPEG','.png','.PNG');
$max_file_size 					= 		"1024000";	

$reg_today = mktime();
$reg_todays_date				=		date('F j, Y', $reg_today);

$reg_timezone					=		''; //TODO
$reg_dst_on						=		''; //TODO

$form_area_size					=		3; //1=320px width, 2=600px width, 3=custom (600 or bigger)
$form_timer_status				=		0;
$form_return_path				=		$source_path_root; //the path where the form will return on submit or on error
$redirect_url_override			=		1;
$redirect_url_address			=		$form_return_path."index.php?thankyou=true"."&".rand();
$form_page_url_override			=		1; // 1 to enable, 0 to disable
$form_page_url_address			=		'index.php'.'?'; // 1 to enable, 0 to disable

//$station_thank_you_message		=		'';

$source_email					=		'cclacontests@station.com'; //e-mail where to send form data

//=============================================================================
// DO NOT EDIT
//=============================================================================

	$autoresponder_override			=		0; // 1= on, 0 = off

//=============================================================================
// BOUNCEBACK EMAIL CONFIGURATION
//=============================================================================

$bounceback_message_override	=		1;

$bounceback_message				=		'If your information is not correct please contact <a href=\"mailto:cclacontests@station.com\" target=\"_blank\" style=\"color: #7e7e7e !important; margin-bottom: 0; margin-left: 0; margin-right: 0; margin-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0\">cclacontests@station.com</a>.'; // write your custom bounceback message 

$email_html_format				=		1; // 0 = plain text, 1 = html
$email_template_override		=		1;
	
//=============================================================================

//-----------------------------------------------------------------------------
// if email_template_override = 1 then all variables below need to be set
//-----------------------------------------------------------------------------

$email_template 				= 		1;
$autoresponse_email_override	=		1;
$source_template				=		'email_template_source.php';
$email_template_filename		=		'templates/email_template_final_' . $unique_directory .'.php'; // dev note: $unique_directory to be replaced by contest ID
$autoresponse_email_template_filename		=	$email_template_filename; // dev note: $unique_directory to be replaced by contest ID
$page_title_email				=		$page_title; // page title for HTML email
$main_image_email 				=		'images/main.jpg';
$email_header					=		$source_path_root.'templates/images/email_header.jpg'; // 560x169 email header image
//$email_header					=		$source_path . 'templates/images/email_header_' . $cc_contest_id . '.jpg'; // 560x169 email header image
$back_to_contest				=		$source_path_root . '?trackbackurl'; // back to contest Link
$facebook_sharelink				=		'https://www.facebook.com/sharer/sharer.php?u=' . urlencode($source_path_root); // Facebook Share Link
$twitter_sharelink				=		'https://twitter.com/intent/tweet?original_referer=' . urlencode($source_path_root) . '&text=' .urlencode($page_title) . '&url=' . urlencode($source_path_root);  // Twitter Share Link

$ga_email_tracking				=		'<img src="http://www.pixelsite.info/track/t15820.gif" border=0>';
//$ga_email_tracking			=		$app_ga_email_tracking_pixel;

//=============================================================================
// advanced upload methods
//=============================================================================

$upload_via_curl				=		0; //1 = on, 0 = off, for Facebook only
$via_curl						=		0; //1 = on, 0 = off, for Facebook only
 
$via_db							=		1; //1 = on, 0 = off, for station only
$via_db_via_curl				=		0; //1 = on, 0 = off, for Facebook only

//=============================================================================
// EXPIRATION DATE, IF NEEDED
//=============================================================================

$active_status					=		1;
$app_timer_status				=		0;

//PHP date expiration code
$todays_date 					= 		date("Y-m-d H:i:s"); 
$today 							= 		strtotime($todays_date); 

$beg_date 						= 		"2013-07-01 06:00:00";  // YEAR-MONTH-DAY HOUR:MINUTE:SECONDS
$begin_date 					= 		strtotime($beg_date);

$exp_date 						= 		"2014-03-07 13:00:00";  // YEAR-MONTH-DAY HOUR:MINUTE:SECONDS

$expiration_date 				= 		strtotime($exp_date);

$exp_date2 						= 		"2014-03-07 13:00:00";  // YEAR-MONTH-DAY HOUR:MINUTE:SECONDS
$expiration_date2 				= 		strtotime($exp_date2); 

//=============================================================================
// Instagram settings
//=============================================================================

/*$instagram_module				=		0; // 1 to enable, 0 to disable
$instagram_rss_feed				=		'http://followgram.me/GbyGUESS/rss';
$instagram_feed_provider		=		'followgram';


$instagram_widget_width			=		976; //width in px for the widget
$instagram_show_pics_num		=		12; //num to pic to show
$instagram_thumb_size_px		=		242; //size thumb in px
$instagram_styling				=		0; //0 = margin off, 1 = margin off*/


//=============================================================================
// FACEBOOK ONLY CONFIG VARS - DISABLED OUTSIDE FACEBOOK
//=============================================================================
/*
 //facebook vars
$fbBaseDir						= 		"https://www.lastation.com/fb/";
$app_id 						= 		"249047715230237";
$app_secret						= 		"f7a394cd09ba4077a348843cca0d92de";

$siteposition 					=		$source_path;


$fangate_image 					= 		$fbBaseDir.$unique_directory."/images/fangate.jpg?" . mktime();
$enter_image 					= 		$fbBaseDir.$unique_directory."/images/enter.jpg?" . mktime();
*/


//=============================================================================
// Check for single vote function enablers - disabled if unused
//=============================================================================

/*
$csv_check_for_single_vote		=		0;
$csv_time_gap_enabled			=		0;
$csv_time_gap_type				=		0; //0 = seconds, 1 = days
$csv_time_gap_in_seconds		=		86400;
$csv_time_gap_in_days			=		0;
$csv_check_single_vote_error	=		'You are allowed only one vote per day';
$csv_file_date_field__index		=		2; //field order of the date
*/

//print 'date is:'.$reg_todays_date.'<br/>';


//=============================================================================
// FORM FIELD DEFINITIONS
//=============================================================================

//declare required fields here
//spaces in between field names are automatically converted to underscores

												
$required_fields_with_spaces	= 		array(
											// Survey 1	
											array(	'field_name'			=>		'Survey1',
													'field_type'			=> 		'radio',
													'field_default_value'	=> 		'Q. When you apply for a loan, the lender is going to ask you for a
full printout of your transcript, including your GPA.~True|True'.chr(13).'False|False',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		1,
													'field_regex'			=>		'/^[a-zA-Z0-9 \.\-\s]{1,50}$/',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),
											
											// Survey 2	
											array(	'field_name'			=>		'Survey2',
													'field_type'			=> 		'radio',
													'field_default_value'	=> 		'Q. Your credit score is important because it can prove how you\'ve
managed your credit over time.~True|True'.chr(13).'False|False',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		1,
													'field_regex'			=>		'/^[a-zA-Z0-9 \.\-\s]{1,50}$/',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),
													
											// Survey 3	
											array(	'field_name'			=>		'Survey3',
													'field_type'			=> 		'radio',
													'field_default_value'	=> 		'Q. One of the biggest advantages of a good credit rating is being able to get free stuff from your bank. After all, everyone needs a new toaster oven!~True|True'.chr(13).'False|False',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		1,
													'field_regex'			=>		'/^[a-zA-Z0-9 \.\-\s]{1,50}$/',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),
											
									  		// Full Name
									  		array(	'field_name'			=> 		'Full Name',
													'field_type'			=> 		'text',
													'field_default_value'	=> 		'e.g. John Smith',
													'field_is_required'		=>		1,
													'field_has_regex'		=>		1,
													'field_regex'			=>		'/^[a-zA-Z0-9 \.\-\s]{1,50}$/',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''
													),
											
											
											// City			
											array(	'field_name'			=> 		'City',
													'field_type'			=> 		'text',
													'field_default_value'	=> 		'e.g. Burbank',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		1,
													'field_regex'			=>		'/^[a-zA-Z0-9 \.\-\s]{1,50}$/',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),
											
											// Zip Code			
											array(	'field_name'			=> 		'Zip Code',
													'field_type'			=> 		'text',
													'field_default_value'	=> 		'e.g. 91505',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		1,
													'field_regex'			=>		'/^[0-9]{5}$/',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),
											// Address		
											array(	'field_name'			=> 		'Address',
													'field_type'			=> 		'text',
													'field_default_value'	=> 		'e.g. 123 Olive Avenue',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		1,
													'field_regex'			=>		'/^[a-zA-Z0-9 \.,#\-\s]{1,50}$/',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),

											// State	
											array(	'field_name'			=> 		'State',
													'field_type'			=> 		'state',
													'field_default_value'	=> 		'SelectState',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		0,
													'field_regex'			=>		'',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),
											

													
								
											// Zip Code	with city/state retrieval		
											array(	'field_name'			=> 		'Zip Code',
													'field_type'			=> 		'autozip',
													'field_default_value'	=> 		'e.g. 91505',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		1,
													'field_regex'			=>		'/^[0-9]{5}$/',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),		
													
													
											// E-Mail	
											array(	'field_name'			=> 		'E-Mail',
													'field_type'			=> 		'text',
													'field_default_value'	=> 		'e.g. mail@example.com',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		1,
													'field_regex'			=>		'/^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),
											
											// Phone Number	
											array(	'field_name'			=> 		'Phone Number',
													'field_type'			=> 		'phone_number',
													'field_default_value'	=> 		'NULL',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		1,
												//	'field_regex'			=>		'/^[0-9\+\-\s]{10,17}$/',
													'field_regex'			=>		'/^(?=(\(\d{3}\)\s)|\d{3}[-])\(?([0-9]{3})\)?\s*[ -]?\s*([0-9]{3})\s*[ -]?\s*([0-9]{4})((\sext|\sx)\s*\.?:?\s([0-9]+))?$/im',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),
													
											// Date of Birth	
											array(	'field_name'			=> 		'Date of Birth',
													'field_type'			=> 		'date_of_birth',
													'field_default_value'	=> 		'Month Day, Year',
										//			'field_default_value'	=> 		'NULL',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		1,
										//			'field_regex'			=>		'/^(\d{2})-(\d{2})-(\d{4})$/',
													'field_regex'			=>		'/^\b(January|February|March|April|May|June|July|August|September|October|November|December|Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|[0-9]|1[0-2])(\s|\.|-)(\d|[0-2][0-9]|3[0-1])(st|rd|nd)?,?(\s|\.|-)\b\d{1,4}\b$/',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),

											//radio button
	/*
	 * Radio Button Definition
	 * To define the radio button list, do as follows:
	 * First, the label, end it with '~' (tilde)
	 * Second, the field name/value couples. The couples are return separated; 
	 * to do the separation in a clean way, interrupt the array with '.chr(13).' i.e. 'name|value'.chr(13).'name|value'.chr(13).' etc...
	 * Names and values are separated by the pipe ('|') character.
	 * Of course, try not to use characters sed as separators. If required so, try to escape it as HTML code.
	 *
	 *
	 */
													
													
											/*array(	'field_name'			=>		'Survey',
													'field_type'			=> 		'radio',
													'field_default_value'	=> 		'If you were given enough time in a single day to do anything you wish, would you:~a. See the latest blockbuster film with your family|a'.chr(13).'b. Have a spa day to work out all of the kinks|b'.chr(13).'c. Attend a wine tasting with friends|c'.chr(13).'d. Go shopping at the hottest galleria�and splurge on yourself|d',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		1,
													'field_regex'			=>		'/^[a-zA-Z0-9 \.\-\s]{1,50}$/',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),*/	
													
													
											/*//checkbox

											array(	'field_name'			=> 		'Demo Field',
													'field_type'			=> 		'checkbox',
													'field_default_value'	=> 		'|Yes',
													'field_is_required'		=>		0,													
													'field_has_regex'		=>		0,
													'field_regex'			=>		'/^[a-zA-Z0-9 \.\-\s]{1,50}$/',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),*/											

											/*
											array(	'field_name'			=> 		'Second Demo Field',
													'field_type'			=> 		'checkbox',
													'field_default_value'	=> 		'|Yes',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		0,
													'field_regex'			=>		'/^[a-zA-Z0-9 \.\-\s]{1,50}$/',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),	
													*/
													
													
											//custom dropdown list
/*
 *  Definition
 * To define the radio button list, do as follows:
 * First, the label, end it with '~' (tilde)
 * Second, the field name/value couples. The couples are return separated; 
 * to do the separation in a clean way, interrupt the array with '.chr(13).' i.e. 'name|value'.chr(13).'name|value'.chr(13).' etc...
 * Names and values are separated by the pipe ('|') character.
 * Of course, try not to use characters sed as separators. If required so, try to escape it as HTML code.
 *
 * NOTE: the first column REQUIRES that the value is empty (like in [Select|]) to have a null value to retrieve
 *
 *
 */
											
											/*array(	'field_name'			=>		'List Element',
													'field_type'			=> 		'dropdown',
													'field_default_value'	=> 		'List Label goes Here~Select|'.chr(13).'a. First Option|a'.chr(13).'b. Second Option|b'.chr(13).'c. Attend a wine tasting with friends|c'.chr(13).'d. Go shopping at the hottest galleria�and splurge on yourself|d',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		0,
													'field_regex'			=>		'/^[a-zA-Z0-9 \.\-\s]{1,50}$/',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),*/												
												
											/*//file
											array(	'field_name'			=> 		'Upload File',
													'field_type'			=> 		'file',
													'field_default_value'	=> 		'Yes',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		0,
													'field_regex'			=>		'/^[a-zA-Z0-9 \.\-\s]{1,50}$/',
													'field_has_count'		=>		0,
													'field_text_count'		=>		''),*/
											
											/*// Text Area	
											array(	'field_name'			=> 		'Message',
													'field_type'			=> 		'textarea',
													'field_default_value'	=> 		'',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		0,
													'field_regex'			=>		'',
													'field_has_count'		=>		1,
													'field_text_count'		=>		'50'),*/
							
											/*// Text Area with Counter	
											array(	'field_name'			=> 		'Message',
													'field_type'			=> 		'textareacounter',
													'field_default_value'	=> 		'',
													'field_is_required'		=>		1,													
													'field_has_regex'		=>		0,
													'field_regex'			=>		'',
													'field_has_count'		=>		1,
													'field_text_count'		=>		'750'),
											*/		
											
											);
						

//=============================================================================
// DO NOT FURTHER EDIT BEYOND THIS POINT
//=============================================================================

											
if(!$is_Admin) {											
											
$required_fields = array();
$required_fields_with_spaces_and_asterisks = array();
$field_labels = array();
//for($i=1; $i<count($required_fields_with_spaces); $i++)
foreach($required_fields_with_spaces as $i => $value) {
	

	array_push($required_fields,cleanUpFieldName($required_fields_with_spaces[$i]['field_name']));

	array_push($required_fields_with_spaces_and_asterisks, array(	'field_name'	=> 	$required_fields[$i],
																		'field_type'	=> 	$required_fields_with_spaces[$i]['field_type'],
																		'field_default_value'	=> 	$required_fields_with_spaces[$i]['field_default_value'],
																		'field_has_regex'		=>	$required_fields_with_spaces[$i]['field_has_regex'],
																		'field_regex'			=>	$required_fields_with_spaces[$i]['field_regex'],
																		'field_has_count'		=>	$required_fields_with_spaces[$i]['field_has_count'],
																		'field_text_count'		=>	$required_fields_with_spaces[$i]['field_text_count'],
																		'field_is_required'		=>	$required_fields_with_spaces[$i]['field_is_required'])
																		);
	
	
	

	array_push($field_labels,$required_fields_with_spaces[$i]['field_name']);
} //end foreach




//=============================================================================
// check if the template file exists if not generates it
//=============================================================================

//TODO- insert time constraints



	if(!file_exists($email_template_file)){

	include('templatemaker.php');

	} //end if


} //end if is_Admin

?>