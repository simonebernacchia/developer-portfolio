<?php    

/**
 * Create CSV file from Database Entries
 * modified to create directly from cc_contest_id
 *
 * @package:	commonLA
 * @author:		Simone Bernacchia <simonebernacchia@gmail.com>
 * @date:		2013-03-18 16:00:00 PM PDT
 * @version: 	0.0.1a
 *
 */


 //error_reporting(E_ALL ^ E_NOTICE);

 
 //includes
include('../includes/function_toolbox.php');

$is_Admin			=		1;



include('../config.php');
include('config_admin.php');



$user_permission = $_COOKIE['u_binding']; //inherited from admin interface


include('localdb.php');

$databaseShares = 'ihmla_share_count';

//retrieve the id of the contest

$cc_contest_id		=	sanitize($_GET['id'],1);
$checksum			=	sanitize($_GET['cs'],1);


if(isset($_COOKIE['gls_session_id'])){


$sharesArray = array();

$sql2 = "SELECT		id,
					cc_contest_id,
					email_address,
					share_count
					FROM $databaseShares
					WHERE cc_contest_id = '$cc_contest_id' ";

$res2 = mysql_query($sql2) or die('sql2 query error:  '.mysql_error().' - debug: query: '.$sql2.' - please notify development');

while($row2 = mysql_fetch_array($res2)){
	array_push(array(	'mail'	=>	$row2['email_address'],
						'num'	=>	$row2['share_count']
	
							),$sharesArray);

} //end while					


$recordsArray = array();

//print 'cc_contest_id is:'.$cc_contest_id.'<br/>';

$sql1 = "SELECT 	cc_contest_id, 
					cc_contest_date_entry,
					cc_contest_user_form_data,
					cc_contest_station_optin 
					FROM $databaseEntriesTable
					WHERE cc_contest_id = '$cc_contest_id' AND cc_contest_field_checksum = '$checksum'";
					
					
$res1 = mysql_query($sql1) or die('sql1 query error:  '.mysql_error().' - debug: query: '.$sql1.' - please notify development');

/*
*/

$nrow = mysql_num_rows($res1);

if($nrow > 0){




while($row1 = mysql_fetch_array($res1)){

	$recordsTempArray1 = base64_decode($row1['cc_contest_user_form_data']);

	array_push($recordsArray,unserialize($recordsTempArray1));
} //end while		

//build CSV structure

//purge fields

$fields_to_ignore = array('fid');

	
	foreach($fields_to_ignore as $value){
		if(isset($recordsArray[$value])) {
			unset($_POST[$value]);
		} //end if
	} //end foreach



$data = '';

foreach(array_keys($recordsArray[0]) as $value){$data .= "$value,";}

// Remove trailing comma.

$data = rtrim($data,",");

$data .= "\r\n";

foreach($recordsArray as $i=>$value){

$data .= stripslashes(build_file_data($recordsArray[$i]));

$data .= "\r\n";

} //end foreach

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=".$cc_contest_id.".csv");
header("Pragma: no-cache");
header("Expires: 0");


echo $data;

//header();

} else {

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=".$cc_contest_id.".csv");
header("Pragma: no-cache");
header("Expires: 0");

print 'no entries';


} //end if


} else {
?>
<html>
<head>
<!--<meta http-equiv="refresh"  content="5; <?php print $admin_site_url;?>common/etw_forms/admin/index.php"/> -->

</head>

<body>

<h2>Please log in in order to download CSV files</h2>

<!-- cookie values:
<?php print_r($_COOKIE);?> 
-->

<body>
</html>
<?php
} //end if isset cookie




//function from formscript
function build_file_data($data_input) {

if(!isset($file_data)){$file_data = "";}if(!is_array($data_input)){if(stristr($data_input,'"')){$data_input = str_replace('"','""',$data_input);} if(stristr($data_input,'"') || stristr($data_input,",") || stristr($data_input,"\n") || stristr($data_input,"\r\n")){$file_data = "\"$data_input\"";}else{$file_data = $data_input;}}else{foreach($data_input as $key => $value){if(!is_numeric($key)){$file_data .= build_file_data($value).",";}else{$file_data .= build_file_data($value)." :: ";}}}return rtrim(rtrim($file_data,",")," :: ");

} //end function

?>