<?php

/**
 * Twitter Feed RSS parser and feeder for the Clear Channel Widget
 * Version with buffer - modifications for PHP 4
 * 2012-06-06 : added hashtag search
 * 2012-06-11 : for flash RMO added XML output
 * 2012-06-21 : for flash added the possibility to be fed by direct RSS or search API 
 *
 * @Date:	03-06-2012 10:50:00 -0800 
 * @author: Simone Bernacchia <simonebernacchia@gmail.com>
 * @version:0.0.4a 
 *
 */
 
 //This parser was built to parse from the old instagram API
 
 //prevent file from caching
 
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');


include('../includes/function_toolbox.php');

//base parameters for buffering

	$need_update = true;
	$delta = 300; //in seconds, how old is the filename if need to be updated
	
	$basepath = getcwd();
	$path = $basepath.'/temp/'; //absolute path for the buffer - REMEMBER TO ADAPT



//the channel or hashtag to parse
$myFeed = sanitize($_REQUEST['feed'],1);

//the type of source (from the RSS directly (1) or via search API (0)
$feedType = sanitize($_REQUEST['fty'],1);

//the type of parsing 'from:' = channel; '%23' = hashtag 
$frht = sanitize($_REQUEST['frht'],1);

$numTweets = sanitize($_REQUEST['num'],1);

$resultType = sanitize($_REQUEST['res'],1); //XML or json

$filterSource = sanitize($_REQUEST['from'],1); // TODO - might be needed if we want to filter results by a channel



//=============================================================================
//backward compatibility for other widgets around

if($frht =='' || $frht == null){
	$frht = 'from:';
} //end frht

if($resultType =='' || $resultType == null){
	$resultType = 'json';
} //end resultType

if($feedType =='' || $feedType == null){
	$feedType = '0';
} //end feedType




//=============================================================================


//-----------------------------------------------------------------------------
// params for feed, might become an include config file

if($myFeed !='' && $myFeed != null){

		$havefeed = true;
		

		$feedUrl='http://'.$myFeed;


		$iconUrl='http://a0.twimg.com/profile_images/1359908412/Screen_shot_2011-05-18_at_11.25.54_AM_normal.png';
		$twitterUrl = $myFeed;

		
			$myfilename = str_replace('/','_',$myFeed);

			$filename = $path.$myfilename;



} else {
		$errorUrl='https://www.laclearchannel.com/test/twitter_rss/twitter_rss_scroll/images/feed_error.png';
		$iconUrl='https://twimg0-a.akamaihd.net/profile_images/86399704/102.7_KIIS_logo_112707_normal.jpg';
		$havefeed = false;

} //end if
 




$myTimestamp = time();




if($havefeed == true) {

// check the creation date of the filename; if older then recreate it	

	if (file_exists($filename)) {

		if (filemtime($filename)+$delta < time() ) {
		
		
			//is old
//print 'getting updated';
			unlink($filename);		
		
		
		} else {
		
			//is good
//print 'still good';
			$need_update = false;
		
		} //end if filemtime
	} //end if file exists 


	if ($need_update) {

//print 'getting updated - need update: '.$need_update.'<br/>';

	$curl = curl_init();
    curl_setopt ($curl, CURLOPT_URL, $feedUrl);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec ($curl);
    curl_close ($curl);
//	file_put_contents($filename,$result);

$fp = fopen($filename, "w") or die ('cannot open file');
			fwrite($fp, $result."\r");			
	 		fclose($fp);


	} // end if	
	 
	
$arrFeeds = array();

$doc = parseRSS($filename);

$aRegexp = '#\((.*?)\)#';




if($resultType =='xml'){
//re-read the buffered file and provide it to the user
sleep(1); //one second pause		

	$contents = '';

	$fr = fopen($filename,'r') or die("can't open file");
	$contents = fread($fr, filesize($filename));
	fclose($fr);
	
	header ("content-type: text/xml");
	echo $contents;
	
} else {






	foreach($doc['RSS']['CHANNEL']['ITEM'] as $item) {


   $itemRSS = array ( 
	
	
      'title'	=>	$item['TITLE'],
      'desc' 	=> 	$item['DESCRIPTION'],
      'link' 	=> 	$item['LINK'],
      'date' 	=> 	$item['PUBDATE'],
	  'guid'	=>	$item['GUID'],
      );
    array_push($arrFeeds, $itemRSS);

	} //end foreach

	
	


//-----------------------------------------------------------------------------  
// the old php 5 parser kept for reference



//-----------------------------------------------------------------------------  
//create JSON to feed the scroller

//if there are less than 10 tweets get as much as available



if(count($doc['RSS']['CHANNEL']['ITEM']) < $numTweets){
	$ctTweets = count($doc['RSS']['CHANNEL']['ITEM']) -1;
} else {
	$ctTweets = $numTweets;
} //end if

//	$ctTweets = count($doc['RSS']['CHANNEL']['ITEM']) -1;
	
$html='';


$html.='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
$html.='<html lang="en">';
$html.='<head>';	
$html.='<style type="text/css">
* { margin:0!important; padding:0 !important; font-size:14px !important; color: #000 !important; line-height:18px!important; font-family: Arial,Helvetica,sans-serif !important;}
div#feedContainer {
	width:280px !important;
	padding:10px!important;
/*	border:1px red solid;*/
}

div.item {display:block; padding:4px !important;}
abbr {border: none !important;}
a:active, a:link, a:visited {text-decoration:none !important;}
h4, h4 a {font-size:18px !important;text-decoration:none !important;}
h4 a:hover, a:hover { color: #888 !important; text-decoration:underline !important;}
h2,p,a {font-size:14px !important;}
span.datetime {font-size:10px !important; font-style:italic !important;}

</style>
			';
$html.='</head>';	
$html.='<body>';	
$html.='<div id="feedContainer">';	


$json ='';






	for($i =0; $i<$ctTweets; $i++){
$html.='<div class="item">';


	//remove unwanted special chars
	$pretitle = strip_tags(preg_replace('/[^(\x20-\x7F)]*/','', $arrFeeds[$i]['title']));	
	$predesc = strip_tags(preg_replace('/[^(\x20-\x7F)]*/','', $arrFeeds[$i]['desc']));	

	$tweetTs = strtotime($arrFeeds[$i]['date']);
	$myDate = date('l, F j Y h:m:s A',$tweetTs - (3600*3));
	
	
	$pretitle2 =str_replace('&#039;','&rsquo;',htmlentities($pretitle, ENT_QUOTES));
	$predesc2 = str_replace('&#039;','&rsquo;',htmlentities($predesc, ENT_QUOTES));
		
$html.='<h4 class="title"><a href="'.$arrFeeds[$i]['link'].'" target="_blank">'.$pretitle.'</a></h4>';

$html.='<p><span class="datetime">'.$myDate.'</span><br/>';	

$html.=''.$arrFeeds[$i]['desc'].'</p>';
$html.='<!-- guid: '.$arrFeeds[$i]['guid'].' -->';
		
		
		
		
	// calculate the date	
		$tweetStyleTime = nicetime( ($tweetTs),2);
		
	
$html.='</div>';	
		
	} //end foreach


$html.='</div><!-- end feedContainer -->';	
$html.='</body>';	
$html.='</html>';	
	

  
//-----------------------------------------------------------------------------  
//output results


echo $html;

} //end if resultType
  
} else {

if($resultType =='xml'){

$contents = '';

$contents .= '<?xml version="1.0" encoding="UTF-8"?>';

$contents .='<rss version="2.0" xmlns:google="http://base.google.com/ns/1.0" xmlns:openSearch="http://a9.com/-/spec/opensearch/1.1/" xmlns:media="http://search.yahoo.com/mrss/" xmlns:twitter="http://api.twitter.com/">';

$contents .='<channel>';
$contents .='<title>No results</title>';
$contents .='<item><title>No Feed found or API overload</title>';
$contents.='<link></link>';
$contents.='<description>Either the Feed/hashtag was not found or the twitter API has been used over 1000 times/hour. Please try again later.</description>';
$contents.='<pubDate>'.date('D, d M Y H:i:s O',mktime()).'</pubDate>';
$contents.='<guid></guid>';
$contents .='<author></author>';
$contents .='<media:content type="image/jpg" height="48" width="48" url="'.$errorUrl.'"/>';
$contents .='</item></channel></rss>';


	header ("content-type: text/xml");


} else {


$json = '';
$json .= '{';
$json.=' "lastCalledDate": "feed last updated from twitter: '.date('Y-M-d h:i:s',time()).'",';
$json.='"results":[';
$json .= '{';
		$json.='"from_user":"0",';
		$json.='"from_url":"0",';
		$json.='"profile_image_url":'.'"'.$errorUrl.'",';
		$json.='"title":"Rate limit exceeded. Clients may not make more than 150 requests per hour.",';
		$json.='"text":"Rate limit exceeded. Clients may not make more than 150 requests per hour.",';
		$json.='"link":"0",';
		$json.='"date":""';

		$json.='},';
$json .= '{';
		$json.='"from_user":"0",';
		$json.='"from_url":"0",';
		$json.='"profile_image_url":'.'"'.$errorUrl.'",';
		$json.='"title":"Rate limit exceeded. Clients may not make more than 150 requests per hour.",';
		$json.='"text":"Rate limit exceeded. Clients may not make more than 150 requests per hour.",';
		$json.='"link":"0",';
		$json.='"date":""';

		$json.='}';
$json .= ']}';

echo $json;
//	print 'no feed selected'; //to be replaced with a decent output in the display

} //end if resultType


} //end if havefeed




//=============================================================================
// function for parsing the RSS feed on PHP 4
// origin: http://www.stemkoski.com/how-to-easily-parse-a-rss-feed-with-php-4-or-php-5/
//
// usage sample:
// PARSE THE RSS FEED INTO ARRAY
//	$xml = parseRSS("http://www.stemkoski.com/?feed=rss2");
// 
// SAMPLE USAGE OF 
//	foreach($xml['RSS']['CHANNEL']['ITEM'] as $item) {
//		echo("<p class=\"indexBoxNews\"><a href=\"{$item['LINK']}\" target=\"_blank\" class=\"indexBoxNews\">{$item['TITLE']}{$link}</a></p>");
//	} //end foreach
//=============================================================================

/**
 * function for parsing the RSS feed on PHP 4
 * origin: http://www.stemkoski.com/how-to-easily-parse-a-rss-feed-with-php-4-or-php-5/
 *
 * @param: url -> the URL of the RSS feed to parse
 *
 */

	function parseRSS($url) { 
 
	//parse rss feed
        $feedeed = implode('', file($url));
        $parser = xml_parser_create();
        xml_parse_into_struct($parser, $feedeed, $valueals, $index);
        xml_parser_free($parser);
 
	//construct array
        foreach($valueals as $keyey => $valueal){
            if($valueal['type'] != 'cdata') {
                $item[$keyey] = $valueal;
			} //end if
        } //end foreach
 
        $i = 0;
 
        foreach($item as $key => $value) {
 
            if($value['type'] == 'open') {
 
                $i++;
                $itemame[$i] = $value['tag'];
 
            } elseif($value['type'] == 'close') {
 
                $feed = $values[$i];
                $item = $itemame[$i];
                $i--;
 
                if(count($values[$i])>1){
                    $values[$i][$item][] = $feed;
                } else {
                    $values[$i][$item] = $feed;
                } //end if count
 
            } else {
                $values[$i][$value['tag']] = $value['value'];  
            } //end if
        }
 
	//return array values
        return $values[0];
	} //end function



?>