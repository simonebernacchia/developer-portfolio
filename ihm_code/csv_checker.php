<?php

/** 
 * Check CSV file for e-mail address
 *
 * 06-13-2012: added checkCSVforDate function
 *
 * @author: Simone Bernacchia <simonebernacchia@gmail.com>
 * @date: 2012-03-23 12:08:00 PM
 * @version: 0.0.1a
 *
 */



/**
 * check for Email in the given CSV file
 *
 * @param:		csvFile = filename of the CSV to be parsed
 * @param:		email = email address to look for
 * @returns:	1 if found, 0 if not found
 *
 */
 
// print 'CSV_Checker loaded<br/>';
 
function checkCSVforEmail($csvFile, $email) {

$found = 0;

$row = 1;
if (($handle = fopen($csvFile, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
   //     echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;
        for ($c=0; $c < $num; $c++) {
				if($data[$c] == $email){
			//	print 'bingo!<br/>';
					$found = 1;
					break;
					
				} //end if
				
				
		
    //        echo $data[$c] . "<br />\n";
        } //end for
    } //end while
    fclose($handle);
} //end if

return $found;


} //end function




/**
 * check for Email in a time interval in the given CSV file
 *
 * @param:		csvFile = filename of the CSV to be parsed
 * @param:		email = email address to look for
 * @param:		delta = interval (in seconds) between two entries
 * @returns:	1 if found, 0 if not found
 *
 */

function checkCSVforDate($csvFile, $email, $delta){

$myDelta = $delta;

$now = mktime();

$found = 0;

$row = 1;
if (($handle = fopen($csvFile, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
     //   echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;
       for ($c=0; $c < $num; $c++) {
				if($data[$c] == $email){ 
			//	print_r($data);
		//convert date in a more legit one
				$myDate = str_replace('.','',$data[3]);
			//	print 'date in seconds value: '.strtotime($myDate).'<br/>';
				if(strtotime($myDate) < $now+$myDelta){
			//	print 'bingo!<br/>';
					$found = 1;
					break;
				} //end if
					
				} //end if
				
				
		
       //     echo $data[$c] . "<br />\n";
        } //end for
    } //end while
    fclose($handle);
} //end if
return $found;

} //end function



?>