<!-- jQuery script goes here -->
<script type="text/javascript">
/**
 * Validation for Form
 *
 * new version
 * - improved parsing (switch instead of if) TODO
 * - added cases for checkbox, custom drop-dovn,file and autozip
 * - prepared the console
 *
 *
 *
 */
 
 
$(document).ready(function() {

<?php if($_GET['err']){ ?>
	var errorString = "<?php print $errorList; ?>"; 
	
	showErrors(errorString);
	
	
<?php } //end if ?>




<?php if($ajax_active == 1) { ?>
   //change the action of the form so that will work with ajax
   $('form#etw').get(0).setAttribute('action', ''); //this works
<?php } //end if ?>
	
	var inputValue;
	var defaultInputValue;
	
	var light = "#9A9A9A";
	var dark = "#666";
	
	<?php
	
		$jquery_submit_value_check = '';
		$fields_variableList = '';
		$fieldsCheckCode = '';
		
		$fieldsAjaxCollector = 'var dataString ="';
	
		foreach($required_fields_with_spaces as $i => $value)	{
			
			$additionalControls = '';
			$additionalControlsTwo = '';

			
			$tempFieldName0 = str_replace('_','',$required_fields[$i]);
			$tempFieldName = str_replace('-','',$tempFieldName0);

	
	
					//a dirty workaround to make email work with spanish title
				if($required_fields[$i]== 'Correo_electr_oacute_nico' || $required_fields[$i] == 'Direcci_oacute_n_de_correo_electr_oacute_nico'){
					
					$parsedFormfieldValue = 'E-Mail';
					
					

				} else {
				
					$parsedFormfieldValue = $required_fields[$i];
				
				} //end if
	
	

	
		if($required_fields_with_spaces[$i]['field_type'] != 'date_of_birth' && $required_fields_with_spaces[$i]['field_type'] != 'date_of_birth_es'  && $required_fields_with_spaces[$i]['field_type'] != 'phone_number' && $required_fields_with_spaces[$i]['field_type'] != 'custom' && $required_fields_with_spaces[$i]['field_type'] != 'radio' && $required_fields_with_spaces[$i]['field_type'] != 'checkbox' && $required_fields_with_spaces[$i]['field_type'] != 'dropdown' && $required_fields_with_spaces[$i]['field_type'] != 'file' && $required_fields_with_spaces[$i]['field_type'] != 'hidden' && $required_fields_with_spaces[$i]['field_type'] != 'autozip' && $required_fields_with_spaces[$i]['field_type'] != 'html'){
		

			$fields_variableList .= 'var '.$tempFieldName.'Field = $(\'#'.$parsedFormfieldValue.'\').val();'.chr(13);
			$fields_variableList .='var '.$tempFieldName.'defValue = "'.$required_fields_with_spaces[$i]['field_default_value'].'";'.chr(13);
			
			$fieldsAjaxCollector .= $required_fields[$i].'="+'.$tempFieldName.'Field+"&';

			} //end if
			
		//add regex control
			if($required_fields_with_spaces[$i]['field_has_regex'] =='1'){
				$fields_variableList .= 'var '.$tempFieldName.'Regexp = '.$required_fields_with_spaces[$i]['field_regex'].';'.chr(13);
			} //end if
		
		//add length contyrol
			if($required_fields_with_spaces[$i]['field_has_count'] =='1'){
				$fields_variableList .= 'var '.$tempFieldName.'Count = '.$required_fields_with_spaces[$i]['field_text_count'].';'.chr(13);
			} //end if		
		
//=============================================================================
//case for text
				if($required_fields_with_spaces[$i]['field_type'] == "text") {

		
				//a dirty workaround to make email work with spanishg title
				if($required_fields[$i]== 'Correo_electr_oacute_nico' || $required_fields[$i] == 'Direcci_oacute_n_de_correo_electr_oacute_nico'){
					
					$parsedFormfieldValue = 'E-Mail';
					
					

				} else {
				
					$parsedFormfieldValue = $required_fields[$i];
				
				} //end if
				
					if($required_fields_with_spaces[$i]['field_is_required'] == 1){
				

						
						
						if($required_fields_with_spaces[$i]['field_has_regex'] =='1'){
							$additionalControls ='|| '.$tempFieldName.'Regexp.test(inputValue) == false' ;
						} //end if 
						
						if($required_fields_with_spaces[$i]['field_has_count'] =='1'){
							$additionalControls.='|| '.$tempFieldName.'Field.length >= '.$tempFieldName."Count ";
						} //end if 
						
						

						

					$fieldsCheckCode .=' defaultInputValue = "'.$required_fields_with_spaces[$i]['field_default_value'] .'";
								$("#'. $parsedFormfieldValue.'").bind("keyup", function() {
									inputValue = $("#'.$parsedFormfieldValue.'").val();
									'.$tempFieldName.'Field = $(\'#'.$parsedFormfieldValue.'\').val();
							//		console.log("control on bind:"+inputValue);
									
									
									if(inputValue == defaultInputValue || inputValue == undefined || inputValue == "" '. $additionalControls.')	{
										$("#'. $parsedFormfieldValue.'").css("color",light);
									
									} else {
										//console.log("key pressed - dark");
										$("#'.$parsedFormfieldValue.'").css("color",dark);
						
									
									} //end if inputValue
								});'.chr(13);

				
								
					$fieldsCheckCode .='$("#'.  $parsedFormfieldValue.'").bind("focusin",function() {
									//console.log("FOCUS IN INPUT VALUE: " + inputValue);
									
									if(inputValue == "" || inputValue != defaultInputValue '. $additionalControls.') { 
										$("#'. $parsedFormfieldValue.'").css("color",dark);
									} else {
										$("#'. $parsedFormfieldValue.'").css("color",light);
									} //end if inputValue
									
								});'.chr(13);
											
								


					$fieldsCheckCode .='$("#'.$parsedFormfieldValue.'").bind("focusout keyup",function() { 
									inputValue = $("#'.$parsedFormfieldValue.'").val();
									//console.log("FOCUS OUT INPUT VALUE: " + inputValue);
									if(inputValue != "" && inputValue != defaultInputValue )
									{
									
										$("#'.$parsedFormfieldValue.'").css("color",dark);
														clearSingleError("'. $parsedFormfieldValue.'");
										$("#'. $parsedFormfieldValue.'_container").addClass("success");										
									} //end if
									
									if(inputValue == defaultInputValue || inputValue == undefined || inputValue == "" '. $additionalControls.')
									{
										clearSingleError("'. $parsedFormfieldValue.'");
										$("#'. $parsedFormfieldValue.'_container").addClass("error");										
										$("#'.$parsedFormfieldValue.'").css("color",light);
									} //end if
									
								});'.chr(13);
								

					// create the value check for submitMe
		
					$jquery_submit_value_check .="if(".$tempFieldName."Field == '' || ".$tempFieldName."Field == undefined || ".$tempFieldName."Field == ".$tempFieldName."defValue";
					
					//regex check
					if($required_fields_with_spaces[$i]['field_has_regex'] === 1){
						$jquery_submit_value_check .="|| ".$tempFieldName."Regexp.test(".$tempFieldName."Field) == false";
					} //end if regex check
					
					//length check
					if($required_fields_with_spaces[$i]['field_has_count'] === 1){
						$jquery_submit_value_check .="||  ".$tempFieldName."Field.length >= ".$tempFieldName."Count ";
					}	//end if length check				
					
					$jquery_submit_value_check .=	') {'.chr(13);
					
					

					
					$jquery_submit_value_check.= "valid = false;".chr(13)."//console.log('value is:'+".$tempFieldName."Field);".chr(13)."  errorLog +='".$parsedFormfieldValue.",';".chr(13);
						
					$jquery_submit_value_check .="} //end if ".chr(13);
					
					} //end if field_is_required
					
				
				} // end if
				
//=============================================================================				
//this should define for the textArea
				if($required_fields_with_spaces[$i]['field_type'] == "textarea"){
				
						if($required_fields_with_spaces[$i]['field_has_regex'] =='1'){
							$additionalControls ='|| '.$tempFieldName.'Regexp.test(inputValue) == false' ;
						} //end if 
						
						if($required_fields_with_spaces[$i]['field_has_count'] =='1'){
							$additionalControls.='|| '.$tempFieldName.'Field.length >= '.$tempFieldName."Count ";
						} //end if 
				
				
					//create check field for the area
						$fieldsCheckCode .='$("#'.$required_fields[$i].'").bind("focusin focusout keyup",function() { 
						inputValue = $("#'.$required_fields[$i].'").val();
						//console.log("FOCUS OUT INPUT VALUE: " + inputValue);

						
						if(inputValue == defaultInputValue || inputValue == undefined || inputValue == "" '. $additionalControls.')	{
							clearSingleError("'. $required_fields[$i].'");
							$("#'. $required_fields[$i].'_container").addClass("error");
							$("#'.$required_fields[$i].'").css("color",light);
						} //end if
						
						if(inputValue != "" && inputValue != defaultInputValue ){
						
							$("#'.$required_fields[$i].'").css("color",dark);
											clearSingleError("'. $required_fields[$i].'");
							$("#'. $required_fields[$i].'_container").addClass("success");
							
						} //end if
						
					});'.chr(13);

				
				// create the value check for submitMe
		
					$jquery_submit_value_check .="if(".$tempFieldName."Field == '' || ".$tempFieldName."Field == undefined || ".$tempFieldName."Field == ".$tempFieldName."defValue";
					
					//regex check
					if($required_fields_with_spaces[$i]['field_has_regex'] === 1){
						$jquery_submit_value_check .="|| ".$tempFieldName."Regexp.test(".$tempFieldName."Field) == false";
					} //end if regex check
					
					//length check
					if($required_fields_with_spaces[$i]['field_has_count'] === 1){
						$jquery_submit_value_check .="||  ".$tempFieldName."Field.length >= ".$tempFieldName."Count ";
					}	//end if length check				
					
					$jquery_submit_value_check .=	') {'.chr(13);
					$jquery_submit_value_check.= "valid = false;".chr(13)."//	console.log('value is:'+".$tempFieldName."Field);".chr(13)."  errorLog +='".$required_fields[$i].",';".chr(13);
					
					$jquery_submit_value_check.='	clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("error");
					} else {
					clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("success");
										';
					
					$jquery_submit_value_check .="} //end if ".chr(13);		

				
				} //end if required_fields_with_spaces textarea
				

				
//=============================================================================				
//this should define for the textArea with Counter

				if($required_fields_with_spaces[$i]['field_type'] == "textareacounter"){
				
				$fields_variableList .='var '.$tempFieldName.'MaxTxt = ('.$required_fields_with_spaces[$i]['field_text_count'].')-1;'.chr(13);
				
				
						if($required_fields_with_spaces[$i]['field_has_regex'] =='1'){
							$additionalControls ='|| '.$tempFieldName.'Regexp.test(inputValue) == false' ;
						} //end if 
						
						if($required_fields_with_spaces[$i]['field_has_count'] =='1'){
							$additionalControls.='|| '.$tempFieldName.'Field.length >= '.$tempFieldName."Count ";
						} //end if 
				
					//counter span enabled with the plug-in
					$fieldsCheckCode.=' //character counter
					$("#'.$required_fields[$i].'").charCount({
			allowed: ('.$required_fields_with_spaces[$i]['field_text_count'].')-1,		
			warning: 10
		}); //end character counter'.chr(13);
		
					//this snippet should stop the text after the max count
					
					$fieldsCheckCode.=" //this snippet should stop the text after the max count
	 $('#".$required_fields[$i]."').keypress(function(e) {
        if (e.which < 0x20) {
            // e.which < 0x20, then it's not a printable character, 0x20 is delete key
            // e.which === 0 - Not a character
            return;     // Do nothing
        }
        if (this.value.length == ".$tempFieldName."MaxTxt) {
            e.preventDefault();
        } else if (this.value.length > ".$tempFieldName."MaxTxt) {
            // Maximum exceeded
            this.value = this.value.substring(0, ".$tempFieldName."MaxTxt);
        }
    });
					".chr(13).chr(13);
					
					
				
					//create check field for the area
						$fieldsCheckCode .='$("#'.$required_fields[$i].'").bind("focusin focusout keyup",function() { 
						inputValue = $("#'.$required_fields[$i].'").val();
						//console.log("FOCUS OUT INPUT VALUE: " + inputValue);

						
						if(inputValue == defaultInputValue || inputValue == undefined || inputValue == "" '. $additionalControls.')	{
							clearSingleError("'. $required_fields[$i].'");
							$("#'. $required_fields[$i].'_container").addClass("error");
							$("#'.$required_fields[$i].'").css("color",light);
						} //end if
						
						if(inputValue != "" && inputValue != defaultInputValue ){
						
							$("#'.$required_fields[$i].'").css("color",dark);
											clearSingleError("'. $required_fields[$i].'");
							$("#'. $required_fields[$i].'_container").addClass("success");
							
						} //end if
						
					});'.chr(13);

				
				// create the value check for submitMe
		
					$jquery_submit_value_check .="if(".$tempFieldName."Field == '' || ".$tempFieldName."Field == undefined || ".$tempFieldName."Field == ".$tempFieldName."defValue";
					
					//regex check
					if($required_fields_with_spaces[$i]['field_has_regex'] === 1){
						$jquery_submit_value_check .="|| ".$tempFieldName."Regexp.test(".$tempFieldName."Field) == false";
					} //end if regex check
					
					//length check
					if($required_fields_with_spaces[$i]['field_has_count'] === 1){
						$jquery_submit_value_check .="||  ".$tempFieldName."Field.length >= ".$tempFieldName."Count ";
					}	//end if length check				
					
					$jquery_submit_value_check .=	') {'.chr(13);
					$jquery_submit_value_check.= "valid = false;".chr(13)."//	console.log('value is:'+".$tempFieldName."Field);".chr(13)."  errorLog +='".$required_fields[$i].",';".chr(13);
					
					$jquery_submit_value_check.='	clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("error");
					} else {
					clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("success");
										';
					
					$jquery_submit_value_check .="} //end if ".chr(13);		

				
				} //end if required_fields_with_spaces textarea				
				
//=============================================================================				
//this should define for the textArea with Word Counter
				if($required_fields_with_spaces[$i]['field_type'] == "textareawordcounter"){
	
				} //end if required_fields_with_spaces textarea		

				
//=========================================================================
//this should define for the DoB
				if($required_fields_with_spaces[$i]['field_type'] == "date_of_birth"){
				
					$fields_variableList .= 'var dateMonthField = $(\'#month_select\').val();'.chr(13);
					$fields_variableList .= 'var dateDayField = $(\'#day_select\').val();'.chr(13);
					$fields_variableList .= 'var dateYearField = $(\'#year_select\').val();'.chr(13);
					$fields_variableList .= 'var dateFullField = dateMonthField+" " +dateDayField+", "+dateYearField;'.chr(13);
					$fields_variableList .='var '.$tempFieldName.'defValue = "'.$required_fields_with_spaces[$i]['field_default_value'].'";'.chr(13);
					
							//add regex control
			if($required_fields_with_spaces[$i]['field_has_regex'] =='1'){
				$fields_variableList .= 'var '.$tempFieldName.'Regexp = '.$required_fields_with_spaces[$i]['field_regex'].';'.chr(13);
			} //end if
		
		
						if($required_fields_with_spaces[$i]['field_has_regex'] =='1'){
							$additionalControls ='|| '.$tempFieldName.'Regexp.test(inputValue) == false' ;
							$additionalControlsTwo ='|| '.$tempFieldName.'Regexp.test(dateFullField) == false' ;
						} //end if 
						
						if($required_fields_with_spaces[$i]['field_has_count'] =='1'){
							$additionalControls.='|| '.$tempFieldName.'Field.length >= '.$tempFieldName."Count ";
							$additionalControlsTwo.='|| dateFullField.length >= '.$tempFieldName."Count ";
						} //end if 
										
					$fieldsAjaxCollector .= 'date_month="+dateMonthField+"&date_day="+dateDayField+"&date_year="+dateYearField+"&';						
				
				$fieldsCheckCode .=' defaultInputValue = "'.$required_fields_with_spaces[$i]['field_default_value'] .'";'.chr(13);
					$fieldsCheckCode .='$("#month_select,#day_select,#year_select").bind("focusin focusout change",function() { 
					inputValue = $("#month_select").val()+" "+$("#day_select").val()+", "+$("#year_select").val();
				//	console.log("FOCUS OUT INPUT VALUE: [" + inputValue + "] - default value: ["+defaultInputValue+"]");
				//	console.log("result of regexp text: " + '.$tempFieldName.'Regexp.test(inputValue));

					if(inputValue != "" && inputValue != defaultInputValue) {
					
						$("#'.$required_fields[$i].'").css("color",dark);
										clearSingleError("'. $required_fields[$i].'");
						$("#'. $required_fields[$i].'_container").addClass("success");
						
					} //end if
					
					if(inputValue == defaultInputValue || inputValue == undefined || inputValue == "" '.$additionalControls.' )	{
						clearSingleError("'. $required_fields[$i].'");
						$("#'. $required_fields[$i].'_container").addClass("error");
						$("#'.$required_fields[$i].'").css("color",light);
					} //end if
						
						
					});'.chr(13);	
			
				
					// create the value check for submitMe
		
					$jquery_submit_value_check .="if(dateFullField == '' || dateFullField == undefined || dateFullField == ".$tempFieldName."defValue  ".$additionalControlsTwo."";
					
				

					$jquery_submit_value_check .=	') {'.chr(13);
					$jquery_submit_value_check.= "valid = false;".chr(13)."//	console.log('value is:'+dateFullField);".chr(13)."  errorLog +='Date_of_Birth,';".chr(13);
					
					$jquery_submit_value_check.='	clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("error");
					} else {
					clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("success");
										';
					
					$jquery_submit_value_check .="} //end if ".chr(13);						
				
				} //end if


//=========================================================================
//this should define for the DoB in Spanish
				if($required_fields_with_spaces[$i]['field_type'] == "date_of_birth_es"){
				
					$fields_variableList .= 'var dateMonthField = $(\'#month_select\').val();'.chr(13);
					$fields_variableList .= 'var dateDayField = $(\'#day_select\').val();'.chr(13);
					$fields_variableList .= 'var dateYearField = $(\'#year_select\').val();'.chr(13);
					$fields_variableList .= 'var dateFullField = dateMonthField+" " +dateDayField+", "+dateYearField;'.chr(13);
					$fields_variableList .='var '.$tempFieldName.'defValue = "'.$required_fields_with_spaces[$i]['field_default_value'].'";'.chr(13);
					
							//add regex control
			if($required_fields_with_spaces[$i]['field_has_regex'] =='1'){
				$fields_variableList .= 'var '.$tempFieldName.'Regexp = '.$required_fields_with_spaces[$i]['field_regex'].';'.chr(13);
			} //end if
		
		
						if($required_fields_with_spaces[$i]['field_has_regex'] =='1'){
							$additionalControls ='|| '.$tempFieldName.'Regexp.test(inputValue) == false' ;
							$additionalControlsTwo ='|| '.$tempFieldName.'Regexp.test(dateFullField) == false' ;
						} //end if 
						
						if($required_fields_with_spaces[$i]['field_has_count'] =='1'){
							$additionalControls.='|| '.$tempFieldName.'Field.length >= '.$tempFieldName."Count ";
							$additionalControlsTwo.='|| dateFullField.length >= '.$tempFieldName."Count ";
						} //end if 
										
					$fieldsAjaxCollector .= 'date_month="+dateMonthField+"&date_day="+dateDayField+"&date_year="+dateYearField+"&';						
				
				$fieldsCheckCode .=' defaultInputValue = "'.$required_fields_with_spaces[$i]['field_default_value'] .'";'.chr(13);
					$fieldsCheckCode .='$("#month_select,#day_select,#year_select").bind("focusin focusout change",function() { 
					inputValue = $("#month_select").val()+" "+$("#day_select").val()+", "+$("#year_select").val();
				//	console.log("FOCUS OUT INPUT VALUE: [" + inputValue + "] - default value: ["+defaultInputValue+"]");
				//	console.log("result of regexp text: " + '.$tempFieldName.'Regexp.test(inputValue));

					if(inputValue != "" && inputValue != defaultInputValue) {
					
						$("#'.$required_fields[$i].'").css("color",dark);
										clearSingleError("'. $required_fields[$i].'");
						$("#'. $required_fields[$i].'_container").addClass("success");
						
					} //end if
					
					if(inputValue == defaultInputValue || inputValue == undefined || inputValue == "" '.$additionalControls.' )	{
						clearSingleError("'. $required_fields[$i].'");
						$("#'. $required_fields[$i].'_container").addClass("error");
						$("#'.$required_fields[$i].'").css("color",light);
					} //end if
						
						
					});'.chr(13);	
			
				
					// create the value check for submitMe
		
					$jquery_submit_value_check .="if(dateFullField == '' || dateFullField == undefined || dateFullField == ".$tempFieldName."defValue  ".$additionalControlsTwo."";
					
				

					$jquery_submit_value_check .=	') {'.chr(13);
					$jquery_submit_value_check.= "valid = false;".chr(13)."//	console.log('value is:'+dateFullField);".chr(13)."  errorLog +='Date_of_Birth,';".chr(13);
					
					$jquery_submit_value_check.='	clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("error");
					} else {
					clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("success");
										';
					
					$jquery_submit_value_check .="} //end if ".chr(13);						
				
				} //end i
				
				
//=========================================================================
//this should define for the phone number
				if($required_fields_with_spaces[$i]['field_type'] == "phone_number" || $required_fields_with_spaces[$i]['field_type'] =='Phone_Number'){
				
				
						if($required_fields_with_spaces[$i]['field_has_regex'] =='1'){
							$additionalControls ='|| '.$tempFieldName.'Regexp.test(inputValue) == false' ;
						} //end if 
						
						if($required_fields_with_spaces[$i]['field_has_count'] =='1'){
							$additionalControls.='|| '.$tempFieldName.'Field.length >= '.$tempFieldName."Count ";
						} //end if 
				
					$fields_variableList .= 'var phone1Field = $(\'input[name="phone1"]\').val();'.chr(13);
					$fields_variableList .= 'var phone2Field = $(\'input[name="phone2"]\').val();'.chr(13);
					$fields_variableList .= 'var phone3Field = $(\'input[name="phone3"]\').val();'.chr(13);
					$fields_variableList .= 'var phoneFullField = phone1Field+"-"+phone2Field+"-"+phone3Field;'.chr(13);

					
					$fieldsAjaxCollector .= 'phone1="+phone1Field+"&phone2="+phone2Field+"&phone3="+phone3Field+"&';						
					$fieldsCheckCode .='$("#phone1,#phone2,#phone3").bind("change focusin focusout",function() { 
						inputValue = $("#phone1").val()+"-"+$("#phone2").val()+"-"+$("#phone3").val();
					//	console.log("FOCUS OUT INPUT VALUE: " + inputValue);
					//	console.log("result of regexp text: " + '.$tempFieldName.'Regexp.test(inputValue));

						
						if(inputValue == defaultInputValue || inputValue == undefined || inputValue == "" '. $additionalControls.')
						{
							clearSingleError("'. $required_fields[$i].'");
							$("#'. $required_fields[$i].'_container").addClass("error");
							$("#'.$required_fields[$i].'").css("color",light);
						} //end if
						
						if(inputValue != "" && '.$tempFieldName.'Regexp.test(inputValue) == true) {
						
							$("#'.$required_fields[$i].'").css("color",dark);
											clearSingleError("'. $required_fields[$i].'");
							$("#'. $required_fields[$i].'_container").addClass("success");
							
						} //end if
						
					});'.chr(13);	

			
					$jquery_submit_value_check .='// for this field the field_has_regex value is:"'.$required_fields_with_spaces[$i]['field_has_regex'].'" and the type of it is: '.gettype($required_fields_with_spaces[$i]['field_has_regex']).chr(13).chr(13);
				
					$jquery_submit_value_check .="if(phoneFullField  == '' || phoneFullField  == undefined";
					
										//debug
					
					
					//regex check
					if($required_fields_with_spaces[$i]['field_has_regex'] === 1 || $required_fields_with_spaces[$i]['field_has_regex'] === '1'){
						$jquery_submit_value_check .="|| ".$tempFieldName."Regexp.test(phoneFullField) == false";
					} //end if regex check

					$jquery_submit_value_check .=	') {'.chr(13);
					$jquery_submit_value_check.= "valid = false;".chr(13)."//	console.log('value is:'+phoneFullField );".chr(13)."  errorLog +='Phone_Number,';".chr(13);
					
					$jquery_submit_value_check.='	clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("error");
					} else {
					clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("success");
										';
					
					$jquery_submit_value_check .="} //end if ".chr(13);							
				
				
			
				} //end if
				
//=========================================================================
//this should define for the Additional phone number
				if($required_fields_with_spaces[$i]['field_type'] == "phone_number_add"){
				
				
						if($required_fields_with_spaces[$i]['field_has_regex'] =='1'){
							$additionalControls ='|| '.$tempFieldName.'Regexp.test(inputValue) == false' ;
						} //end if 
						
						if($required_fields_with_spaces[$i]['field_has_count'] =='1'){
							$additionalControls.='|| '.$tempFieldName.'Field.length >= '.$tempFieldName."Count ";
						} //end if 
				
					$fields_variableList .= 'var phone1_'.$tempFieldName.'Field = $(\'input[name="phone1_'.$required_fields[$i].'"]\').val();'.chr(13);
					$fields_variableList .= 'var phone2_'.$tempFieldName.'Field = $(\'input[name="phone2_'.$required_fields[$i].'"]\').val();'.chr(13);
					$fields_variableList .= 'var phone3_'.$tempFieldName.'Field = $(\'input[name="phone3_'.$required_fields[$i].'"]\').val();'.chr(13);
					$fields_variableList .= 'var phone_'.$tempFieldName.'FullField = phone1_'.$tempFieldName.'Field+"-"+phone2_'.$tempFieldName.'Field+"-"+phone3_'.$tempFieldName.'Field;'.chr(13);

					
					$fieldsAjaxCollector .= 'phone1_'.$tempFieldName.'="+phone1_'.$tempFieldName.'Field+"&phone2_'.$tempFieldName.'="+phone2_'.$tempFieldName.'Field+"&phone3_'.$tempFieldName.'="+phone3_'.$tempFieldName.'Field+"&';						
					$fieldsCheckCode .='$("#phone1_'.$tempFieldName.',#phone2_'.$tempFieldName.',#phone3_'.$tempFieldName.'").bind("change focusin focusout",function() { 
						inputValue = $("#phone1_'.$tempFieldName.'").val()+"-"+$("#phone2_'.$tempFieldName.'").val()+"-"+$("#phone3_'.$tempFieldName.'").val();
					//	console.log("FOCUS OUT INPUT VALUE: " + inputValue);
					//	console.log("result of regexp text: " + '.$tempFieldName.'Regexp.test(inputValue));

						
						if(inputValue == defaultInputValue || inputValue == undefined || inputValue == "" '. $additionalControls.')
						{
							clearSingleError("'. $required_fields[$i].'");
							$("#'. $required_fields[$i].'_container").addClass("error");
							$("#'.$required_fields[$i].'").css("color",light);
						} //end if
						
						if(inputValue != "" && '.$tempFieldName.'Regexp.test(inputValue) == true) {
						
							$("#'.$required_fields[$i].'").css("color",dark);
											clearSingleError("'. $required_fields[$i].'");
							$("#'. $required_fields[$i].'_container").addClass("success");
							
						} //end if
						
					});'.chr(13);	

			
				
					$jquery_submit_value_check .="if(phone_".$tempFieldName."FullField  == '' || phone_".$tempFieldName."FullField  == undefined";
					
									//regex check
					if($required_fields_with_spaces[$i]['field_has_regex'] === 1){
						$jquery_submit_value_check .="|| ".$tempFieldName."Regexp.test(phone_".$tempFieldName."FullField) == false";
					} //end if regex check

					$jquery_submit_value_check .=	') {'.chr(13);
					$jquery_submit_value_check.= "valid = false;".chr(13)."//	console.log('value is:'+phone_".$tempFieldName."FullField );".chr(13)."  errorLog +='". $required_fields[$i].",';".chr(13);
					
					$jquery_submit_value_check.='	clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("error");
					} else {
					clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("success");
										';
					
					$jquery_submit_value_check .="} //end if ".chr(13);							
				
	
					$fieldsExtraCode.="	
				//phone number manipulation
			//area code 3+ other focus - might need some modification though		
				$('#phone1_". $required_fields[$i]."').keyup(function(){
					var cnt = $('#phone1_". $required_fields[$i]."').val().length +1;
					if(cnt<=3){
					$('#phone1_". $required_fields[$i]."').focus();
					} else {
					$('#phone2_". $required_fields[$i]."').focus();
					}
				}); //end keyup
				
					$('#phone2_". $required_fields[$i]."').keyup(function(){
					var cnt = $('#phone2_". $required_fields[$i]."').val().length +1;
					if(cnt<=3){
					$('#phone2_". $required_fields[$i]."').focus();
					} else {
					$('#phone3_". $required_fields[$i]."').focus();
					}
				}); //end keyup';

			";

	
			
				} //end if		

				
//=============================================================================	
// State condition
		if($required_fields_with_spaces[$i]['field_type'] == "state"){
		
		
		
		$fieldsCheckCode .='$("#'.$required_fields[$i].'").bind("change focusin focusout",function() { 
						inputValue = $("#'.$required_fields[$i].'").val();
						defaultInputValue = "'.$required_fields_with_spaces[$i]['field_default_value'].'";
					//	console.log("FOCUS OUT INPUT VALUE: -" + inputValue+"-");
					//	console.log("DEFAULT VALUE: -" + defaultInputValue+"-");
					//	console.log("result of regexp text: " + '.$tempFieldName.'Regexp.test(inputValue));

						
						if(inputValue == defaultInputValue || inputValue == undefined || inputValue == "" ) {
							clearSingleError("'. $required_fields[$i].'");
							$("#'. $required_fields[$i].'_container").addClass("error");
							$("#'.$required_fields[$i].'").css("color",light);
						} //end if
						
						if(inputValue != "" && inputValue != defaultInputValue) {
						
							$("#'.$required_fields[$i].'").css("color",dark);
											clearSingleError("'. $required_fields[$i].'");
							$("#'. $required_fields[$i].'_container").addClass("success");
							
						} //end if
						
					});'.chr(13);	
		/*
		*/
			
				
					$jquery_submit_value_check .="if(".$tempFieldName."Field  == '' || ".$tempFieldName."Field  == undefined || ".$tempFieldName."Field == ".$tempFieldName."defValue";
					
									//regex check
					if($required_fields_with_spaces[$i]['field_has_regex'] === 1){
						$jquery_submit_value_check .="|| ".$tempFieldName."Regexp.test(".$tempFieldName."Field) == false";
					} //end if regex check

					$jquery_submit_value_check .=	') {'.chr(13);
					$jquery_submit_value_check.= "valid = false;".chr(13)."	//console.log('".$tempFieldName."Field value is:'+".$tempFieldName."Field );".chr(13)."  errorLog +='".$tempFieldName.",';".chr(13);
					
					$jquery_submit_value_check.='	clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("error");
					} else {
					clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("success");
										';
					
					$jquery_submit_value_check .="} //end if ".chr(13);			
		
		
		
		
		} // end if
				

//=========================================================================
// checkbox condition
		if($required_fields_with_spaces[$i]['field_type'] == "checkbox"){
			$checkboxValArr = explode('|',$required_fields_with_spaces[$i]['field_default_value']);
		
		$fields_variableList .='var '.$tempFieldName.'Field = $("input[name=\''.$required_fields[$i].'\']:checked").val();'.chr(13);		
		
		$fieldsAjaxCollector .=$required_fields[$i].'="+'.$tempFieldName.'Field+"&';
		
		if($required_fields_with_spaces[$i]['field_is_required'] == 1){

		
		$fieldsCheckCode .= '$("#'.$required_fields[$i].'").bind("focusin focusout change",function(){
							inputValue = $("input[name=\''.$required_fields[$i].'\']:checked").val();
							
							/* defaultInputValue = "'.$required_fields_with_spaces[$i]['field_default_value'].'";*/
							//	console.log("FOCUS OUT INPUT VALUE: -" + inputValue+"-");
							//	console.log("DEFAULT VALUE: -" + defaultInputValue+"-");					
							
							if(inputValue == undefined || inputValue == "" )	{
							//	console.log("not valid!");
									clearSingleError("'.$required_fields[$i].'");
									$("#'. $required_fields[$i].'_container").addClass("error");
						
			
								} //end if
								
								if(inputValue != "" && inputValue != undefined ){
								
						
									clearSingleError("'.$required_fields[$i].'");
									$("#'.$required_fields[$i].'_container").addClass("success");

								} //end if
								});'.chr(13);			
						
						
						
		$jquery_submit_value_check .="if(!".$tempFieldName."Field  ||".$tempFieldName."Field  == null || ".$tempFieldName."Field  == '' || ".$tempFieldName."Field  == undefined ";
				
								//regex check
				if($required_fields_with_spaces[$i]['field_has_regex'] === 1){
					$jquery_submit_value_check .="|| ".$tempFieldName."Regexp.test(".$tempFieldName."Field) == false";
				} //end if regex check

				$jquery_submit_value_check .=	') {'.chr(13);

				$jquery_submit_value_check.= "valid = false;".chr(13)."	//console.log('".$required_fields[$i]." value is:'+".$tempFieldName."Field );".chr(13)."  errorLog +='".$required_fields[$i].",';".chr(13);
				
				$jquery_submit_value_check.='	clearSingleError("'. $required_fields[$i].'");
				$("#'. $required_fields[$i].'_container").addClass("error");
				} else {
				clearSingleError("'. $required_fields[$i].'");
				$("#'. $required_fields[$i].'_container").addClass("success");
										';
				
				$jquery_submit_value_check .="} //end if ".chr(13);				
		
		
		} // end if field_is_required
		
		} //end if


//=========================================================================
// Radio condition
		if($required_fields_with_spaces[$i]['field_type'] == "radio"){
		$fields_variableList .='var '.$tempFieldName.'Field = $("input[name=\''.$required_fields[$i].'\']:checked").val();'.chr(13);	
		
		
		$fieldsAjaxCollector .=$required_fields[$i].'="+'.$tempFieldName.'Field+"&';

		
		$fieldsCheckCode .= '$("input[name=\''.$required_fields[$i].'\']").bind("focusin focusout change",function(){
					inputValue = $("input[name=\''.$required_fields[$i].'\']:checked").val();
					
					/* defaultInputValue = "'.$required_fields_with_spaces[$i]['field_default_value'].'";*/
					//	console.log("FOCUS OUT INPUT VALUE: -" + inputValue+"-");
					//	console.log("DEFAULT VALUE: -" + defaultInputValue+"-");					
					
					if(inputValue == undefined || inputValue == "" )	{
							clearSingleError("'.$required_fields[$i].'");
							$("#'. $required_fields[$i].'_container").addClass("error");
				
	
						} //end if
						
						if(inputValue != "" ){
						
				
							clearSingleError("'.$required_fields[$i].'");
							$("#'.$required_fields[$i].'_container").addClass("success");

						} //end if
						});'.chr(13);		
						
					$jquery_submit_value_check .="if(".$tempFieldName."Field  == '' || ".$tempFieldName."Field  == undefined ";
					
									//regex check
					if($required_fields_with_spaces[$i]['field_has_regex'] === 1){
						$jquery_submit_value_check .="|| ".$tempFieldName."Regexp.test(".$tempFieldName."Field) == false";
					} //end if regex check

					$jquery_submit_value_check .=	') {'.chr(13);
					$jquery_submit_value_check.= "valid = false;".chr(13)."//	console.log('value is:'+".$tempFieldName."Field );".chr(13)."  errorLog +='".$tempFieldName.",';".chr(13);
					
					$jquery_submit_value_check.='	clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("error");
										
					} else {
					clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("success");
										';
					
					$jquery_submit_value_check .="} //end if ".chr(13);						
						
		
		} // end if


//=========================================================================
// dropdown condition
		
		if($required_fields_with_spaces[$i]['field_type'] == "dropdown"){
		

		$fields_variableList .= 'var '.$tempFieldName.'Field = $(\'#'.$required_fields[$i].'\').val();'.chr(13);		
		
		$fieldsAjaxCollector .= $required_fields[$i].'="+'.$tempFieldName.'Field+"&';

		if($required_fields_with_spaces[$i]['field_is_required'] == 1){
		
		$fieldsCheckCode .='$("#'.$required_fields[$i].'").bind("change focusin focusout",function() { 
						inputValue = $("#'.$required_fields[$i].'").val();
					/*	defaultInputValue = "'.$required_fields_with_spaces[$i]['field_default_value'].'";*/
					//	console.log("FOCUS OUT INPUT VALUE: -" + inputValue+"-");
					//	console.log("DEFAULT VALUE: -" + defaultInputValue+"-");
					//	console.log("result of regexp text: " + '.$tempFieldName.'Regexp.test(inputValue));

						
					//	if(inputValue == defaultInputValue || inputValue == undefined || inputValue == "" ) {
						if(inputValue == undefined || inputValue == "" ) {
							clearSingleError("'. $required_fields[$i].'");
							$("#'. $required_fields[$i].'_container").addClass("error");
						//	$("#'.$required_fields[$i].'").css("color",light);
						} //end if
						
					//	if(inputValue != "" && inputValue != defaultInputValue) {
						if(inputValue != "") {
						
						//	$("#'.$required_fields[$i].'").css("color",dark);
											clearSingleError("'. $required_fields[$i].'");
							$("#'. $required_fields[$i].'_container").addClass("success");
							
						} //end if
						
					});'.chr(13);	
		/*
		*/
			
				

					$jquery_submit_value_check .="if(".$tempFieldName."Field  == null || ".$tempFieldName."Field  == '' || ".$tempFieldName."Field  == undefined ";
					
									//regex check
					if($required_fields_with_spaces[$i]['field_has_regex'] === 1){
						$jquery_submit_value_check .="|| ".$tempFieldName."Regexp.test(".$tempFieldName."Field) == false";
					} //end if regex check

					$jquery_submit_value_check .=	') {'.chr(13);

					$jquery_submit_value_check.= "valid = false;".chr(13)."//	console.log('".$tempFieldName."Field value is:['+".$tempFieldName."Field+']' );".chr(13)."  errorLog +='". $required_fields[$i].",';".chr(13);
					
					$jquery_submit_value_check.='	clearSingleError("'. $required_fields[$i].'");
				//	console.log("here");$("#'. $required_fields[$i].'_container").addClass("error");
										// console.log("and here");
					} else {
					clearSingleError("'. $required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("success");
					';
					
					$jquery_submit_value_check .="} //end if ".chr(13);			
		} //end if
		
		
		
		

		
		} //end if

		
//=========================================================================
// file condition		
		if($required_fields_with_spaces[$i]['field_type'] == "file"){

$fields_variableList .='var '.$required_fields[$i].'Field = $(\'input[name="'.$required_fields[$i].'"]\').val();'.chr(13);		
		
		
		$fieldsCheckCode .= '$(\'input[name="'.$required_fields[$i].'"]\').bind("focusin focusout blur keyup change",function(){
					inputValue = $(\'input[name="'.$required_fields[$i].'"]\').val();
					

					if(inputValue == undefined || inputValue == "" )	{
							clearSingleError("'.$required_fields[$i].'");
							$("#'. $required_fields[$i].'_container").addClass("error");
							$(\'input[name="'.$required_fields[$i].'"]\').css("color",light);
						} //end if
						
						if(inputValue != ""){
						
							$(\'input[name="'.$required_fields[$i].'"]\').css("color",dark);
							clearSingleError("'.$required_fields[$i].'");
							$("#'.$required_fields[$i].'_container").addClass("success");
							
						} //end if
						});'.chr(13);
		
$jquery_submit_value_check.=' if('.$required_fields[$i].'Field =="" || '.$required_fields[$i].'Field == undefined ){';
$jquery_submit_value_check.= "valid = false;".chr(13)."//	console.log('value is:'+".$required_fields[$i]."Field);".chr(13)."  errorLog +='".$required_fields[$i].",';".chr(13);
$jquery_submit_value_check.='	clearSingleError("'.$required_fields[$i].'");
					$("#'. $required_fields[$i].'_container").addClass("error");
					} else {
					clearSingleError("'.$required_fields[$i].'");
					$("#'.$required_fields[$i].'_container").addClass("success");';

$jquery_submit_value_check.='} //end if'.chr(13);						
								
		
		
		} //end if
		
//=========================================================================
// autozip condition		
		if($required_fields_with_spaces[$i]['field_type'] == "autozip"){
		
		//basic validation part
		
		$fields_variableList .= 'var '.$tempFieldName.'Field = $(\'#'.$required_fields[$i].'\').val();'.chr(13);
		$fields_variableList .='var '.$tempFieldName.'defValue = "'.$required_fields_with_spaces[$i]['field_default_value'].'";'.chr(13);

					$fieldsAjaxCollector .= $required_fields[$i].'="+'.$tempFieldName.'Field+"&';
						
						if($required_fields_with_spaces[$i]['field_has_regex'] =='1'){
							$additionalControls ='|| '.$tempFieldName.'Regexp.test(inputValue) == false' ;
						} //end if 
						
						if($required_fields_with_spaces[$i]['field_has_count'] =='1'){
							$additionalControls.='|| '.$tempFieldName.'Field.length >= '.$tempFieldName."Count ";
						} //end if 
						
						

						
						
					
					$fieldsCheckCode .=' defaultInputValue = "'.$required_fields_with_spaces[$i]['field_default_value'] .'";
								$("#'. $required_fields[$i].'").bind("keyup", function() {
									inputValue = $("#'.$required_fields[$i].'").val();
									'.$tempFieldName.'Field = $(\'#'.$required_fields[$i].'\').val();
							//		console.log("control on bind:"+inputValue);
									
									
									if(inputValue == defaultInputValue || inputValue == undefined || inputValue == "" '. $additionalControls.')	{
										$("#'. $required_fields[$i].'").css("color",light);
									
									} else {
										//console.log("key pressed - dark");
										$("#'.$required_fields[$i].'").css("color",dark);
						
									
									} //end if inputValue
								});'.chr(13);
								
					$fieldsCheckCode .='$("#'. $required_fields[$i].'").bind("focusin",function() {
									//console.log("FOCUS IN INPUT VALUE: " + inputValue);
									
									if(inputValue == "" || inputValue != defaultInputValue '. $additionalControls.') { 
										$("#'.$required_fields[$i].'").css("color",dark);
									} else {
										$("#'.$required_fields[$i].'").css("color",light);
									} //end if inputValue
									
								});'.chr(13);
								
					$fieldsCheckCode .='$("#'.$required_fields[$i].'").bind("keyup focusout",function() { 
									inputValue = $("#'.$required_fields[$i].'").val();
									//console.log("FOCUS OUT INPUT VALUE: " + inputValue);
									if(inputValue != "" && inputValue != defaultInputValue )
									{
									
										$("#'.$required_fields[$i].'").css("color",dark);
														clearSingleError("'. $required_fields[$i].'");
										$("#'. $required_fields[$i].'_container").addClass("success");
										
									} //end if
									
									if(inputValue == defaultInputValue || inputValue == undefined || inputValue == "" '. $additionalControls.')
									{
										clearSingleError("'. $required_fields[$i].'");
										$("#'. $required_fields[$i].'_container").addClass("error");
										$("#'.$required_fields[$i].'").css("color",light);
									} //end if
									
								});'.chr(13);

		
		//AJAX to interrogate the zip database and feed fields
		$fieldsCheckCode.="
		
							$('#".$required_fields[$i]."').bind('keyup focusin focusout',function(){
						//new	
						if(".$tempFieldName."Field !='' && ".$tempFieldName."Field !=undefined && ".$tempFieldName."Field != ".$tempFieldName."defValue && ".$tempFieldName."Regexp.test(".$tempFieldName."Field) != false){
			//			if(".$tempFieldName."Field !='' && ".$tempFieldName."Field !=undefined && ".$tempFieldName."Field != ".$tempFieldName."defValue){
							var ".$required_fields[$i]."Ajax = $('#".$required_fields[$i]."').val();
						//	console.log('keyin keyout working');
							var ajaxZipData = 'zip='+".$required_fields[$i]."Ajax;
							
							$.ajax({
							type: 	'POST',
						//	url:	'http://".$_SERVER['HTTP_HOST']."/common/common/ajax/ajax_zipcode_finder.php',
							url:	'ajax/ajax_zipcode_finder.php',
							data:	ajaxZipData,
							
							success: function(msg){
								var myMsg = msg;
								//	console.log('feedback and message received: -'+myMsg+'-');
								if(myMsg != '0'){
									debug.log('success and message received: -'+myMsg+'-');
									clearSingleError('". $required_fields[$i]."');
									$('#". $required_fields[$i]."_container').addClass('success');
									populateCityStateFields(myMsg);
									var cityStateZipArr = myMsg.split('!');
									$('#city_r').val(cityStateZipArr[1]);
									$('#state_r').val(cityStateZipArr[2]);
								//	showThankYou();
								
								} else {
								
							//	showErrors(myMsg);
									debug.log('feedback and message received: -'+myMsg+'-');
									clearSingleError('". $required_fields[$i]."');
									$('#". $required_fields[$i]."_container').addClass('error');
									
								} //end if
								
								
								return false;
								
							} //end success
					
					
					}); //end ajax
					//	} else {
						
						} //end if 
							return false;
						}); //end bind ".chr(13);
							
		
		
		
					// create the value check for submitMe
		
					$jquery_submit_value_check .="if(".$tempFieldName."Field == '' || ".$tempFieldName."Field == undefined || ".$tempFieldName."Field == ".$tempFieldName."defValue";
					

					
					//regex check
					if($required_fields_with_spaces[$i]['field_has_regex'] === 1){
						$jquery_submit_value_check .="|| ".$tempFieldName."Regexp.test(".$tempFieldName."Field) == false";
					} //end if regex check
					
					//length check
					if($required_fields_with_spaces[$i]['field_has_count'] === 1){
						$jquery_submit_value_check .="||  ".$tempFieldName."Field.length >= ".$tempFieldName."Count ";
					}	//end if length check				
					
					$jquery_submit_value_check .=	') {'.chr(13);
					$jquery_submit_value_check.= "valid = false;".chr(13)."//console.log('value is:'+".$tempFieldName."Field);".chr(13)."  errorLog +='".$required_fields[$i].",';".chr(13);
						
					$jquery_submit_value_check .="} //end if ".chr(13);		
		
		} //end if		

//=========================================================================
// instagram condition		
/**/
		if($required_fields_with_spaces[$i]['field_type'] == "instagram"){
		
$fields_variableList .='var submissionRadioContent = $(\'input[name="'.$required_fields[$i].'_submission_Type"]:checked\').val(); '.chr(13);
$fields_variableList .='var InstagramPicField = $(\'input[name="'.$required_fields[$i].'"]\').val();'.chr(13);
$fields_variableList .='var InstagramPicDefValue = "'.$instagramLabelArr[2].'";'.chr(13);

$fields_customList .='
	$("#inputFileContainer").hide();
	$("#inputURLContainer").hide();
'.chr(13);

$fieldsAjaxcollector .=''.$required_fields[$i].'_submission_Type="+submissionRadioContent+"&'.$required_fields[$i].'="+InstagramPicField+"&';

$fieldsCheckCode .='		$(\'input[name="'.$required_fields[$i].'_submission_Type"]\').click(function(e){
				var submissionRadiocontent = $(\'input[name="'.$required_fields[$i].'_submission_Type"]:checked\').val();
		//		console.log("you just pressed: "+submissionRadiocontent);
				switch(submissionRadiocontent){
					case "handle":
					$("#inputURLContainer").show();
					$("#instagramUrl").attr("value","'.$instagramLabelArr[2].'");


					$("#inputFileContainer").hide();
					break;
					
					case "file":
					$("#inputURLContainer").hide();
					$("#instagramUrl").attr("value","from file");

					$("#inputFileContainer").show();
					break;
				
				}
		});'.chr(13).chr(13);



$fieldsCheckCode .= '$(\'input[name="'.$required_fields[$i].'"]\').bind("focusout",function(){
					inputValue = $(\'input[name="'.$required_fields[$i].'"]\').val();
					
					defaultInputValue = "social handle";
					if(inputValue== defaultInputValue || inputValue == undefined || inputValue == "" )	{
							clearSingleError("'.$required_fields[$i].'");
							$("#'.$required_fields[$i].'Error").addClass("fieldNo");
							$(\'input[name="'.$required_fields[$i].'"]\').css("color",light);
						} //end if
						
						if(inputValue != "" && inputValue!= defaultInputValue){
						
							$(\'input[name="'.$required_fields[$i].'"]\').css("color",dark);
							clearSingleError("'.$required_fields[$i].'");
							$("#'.$required_fields[$i].'Error").addClass("fieldOk");
							
						} //end if
						});'.chr(13);

$jquery_submit_value_check.=' if(InstagramPicField =="" || InstagramPicField == undefined ||InstagramPicField == InstagramPicDefValue ){';
$jquery_submit_value_check.= "valid = false;".chr(13)."//	console.log('value is:'+InstagramPicField);".chr(13)."  errorLog +='Instagram_Pic,';".chr(13);
$jquery_submit_value_check.='	clearSingleError("'.$required_fields[$i].'");
					$("#'.$required_fields[$i].'Error").addClass("fieldNo");
					} else {
					clearSingleError("'.$required_fields[$i].'");
					$("#'.$required_fields[$i].'Error").addClass("fieldOk");';

$jquery_submit_value_check.='} //end if'.chr(13);								
		
		} //end if
		
				
			} //end foreach
			
			if($opt_in_enabled == 1) { 
				$fields_variableList .= 'var optinField = $(\'#checkbox\').val();'.chr(13);
				$fieldsAjaxCollector .= 'Opt-In="+optinField';
			}

			print $fields_variableList.chr(13);
			
			print $fields_customList.chr(13);			
	
			print $fieldsCheckCode.chr(13);
	
	?>
				//phone number manipulation
			//area code 3+ other focus - might need some modification though		
				$('#phone1').keyup(function(){
					var cnt = $('#phone1').val().length +1;
					if(cnt<=3){
					$('#phone1').focus();
					} else {
					$('#phone2').focus();
					}
				}); //end keyup
				
					$('#phone2').keyup(function(){
					var cnt = $('#phone2').val().length +1;
					if(cnt<=3){
					$('#phone2').focus();
					} else {
					$('#phone3').focus();
					}
				}); //end keyup
				

				
	/*
	*/			
				
<?php 

	print $fieldsExtraCode.chr(13);				

?>

				
			$('#ccla-submit-button').click(function(e){
				e.preventDefault();
				submitMe();
			});

		function submitMe(){
		//repeat variable definition to have it updated
		
<?php			print $fields_variableList.chr(13); ?>
		
		//normal variables and definitions
				var errorLog = '';
				//check all required fields to be filled up correctly
				
				var valid = true;
				
				<?php print $jquery_submit_value_check; ?>
		
				
			//	console.log('errorLog is:'+errorLog);
				
				//send the form via AJAX
				if(valid == true){
				
	<?php if($ajax_enabled == 1){			?>
	//collect all data
	<?php print $fieldsAjaxCollector?>;
	
	
		$.ajax({
				type: 	'POST',
				url:	'formscript.php?ajax=1',
				data:	dataString,
				
				success: function(msg){
					var myMsg = msg;
					if(myMsg == 'ok'){
						showThankYou();
					
					} else {
					
					showErrors(myMsg);
					
					
					} //end if
					
					
				
					
				} //end success
		
		
		}); //end ajax
/*
*/	
<?php } else { ?>
		$('#etw').submit();
<?php } //end if ?>	
				
				
				
				} else {
				
				showErrors(errorLog);
				
				} //end if
				
		} //end function submitMe


	function showErrors(errorList){
	
	<?php if($debug_mode == 1){ ?>
		console.log('showErrors invoked');
		console.log('Errors are:'+ errorList);
	<?php } //end if ?>
	
		errorListArr = errorList.split(',');
		
		//remove the probable last empty element
		if(errorListArr[errorListArr.length -1] == ''){
			errorListArr.pop();
		}
		
		

		
			clearErrors();
		var CCErrorTextList = '';	
	
;

	
		for(var j = 0; j < errorListArr.length; j++){
			
			if(errorListArr[j] == 'duplicate' || errorListArr[j] == 'pippo'){
				CCErrorTextList += '<li><?php print $csv_check_single_vote_error;?></li>';
			
			} //end if
			
			$('#'+errorListArr[j]+'_container').addClass('error');

		} //end for
		
		<?php if($written_errors_notify	== 1) {  ?>
	//added again for mail quantity check
			$('#errorsTextList ul.errorList').html(CCErrorTextList);
			$('#errorsTextList').fadeIn(100);
	//end added		

		<?php } // end if 	?>
		
		
	} //end function
	
	/**
	 * Mass-Clear form elements errors
	 *
	 */
	
	function clearErrors(){

<?php $elementsList = implode($required_fields,',');?>
	var elementsList = "<?php print $elementsList; ?>";
	var elementsListArr = elementsList.split(',');
	
	$('#errorsTextList ul.errorList').html('');
	
	

	
	
		for(var i=0; i < elementsListArr.length; i++){
			if($('#'+elementsListArr[i]+'_container').is('.error')){
				$('#'+elementsListArr[i]+'_container').removeClass('error');
			
			} //end if
			
			
			if ($('#'+elementsListArr[i]+'_container').is('.success')){
				$('#'+elementsListArr[i]+'_container').removeClass('success');
			} //end if
		} //end for
	
		<?php if($written_errors_notify	== 1) {  ?>
			$('#errorsTextList').hide();
			$('#errorsTextList').fadeOut(100);
			<?php } // end if 	?>
	} //end function

	
	/**
	 * Clear single form element error
	 *
	 */
	
	function clearSingleError(element){

		if($('#'+element+'_container').is('.error')){
				$('#'+element+'_container').removeClass('error');
			
			} //end if
			
			
		if ($('#'+element+'_container').is('.success')){
			$('#'+element+'_container').removeClass('success');
		} //end if
	
	} //end function
	
	function showThankYou(){

		$('#etw').fadeOut(100);
		$('#thankyou-container').fadeIn(200);

	
	} //end function
	

	
});//end doc ready





</script>
